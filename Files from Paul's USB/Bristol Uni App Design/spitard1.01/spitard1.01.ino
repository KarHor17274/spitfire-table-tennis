

/*
   from arduino Tutorials ReadASCIIString
   can receive an array - 'pattern' - of values.
   works to 'play' values through servos as a programmable ball sequence.
   works with values from the Flash app spitfire 1.01.
    THIS: tested timing and this ard app works eg 1000 delay signal = balls 1 second apart
   working power supply to test 'brake' properly. is ok. must try without it next
   setup routine ends with blinkLED(5) 
   check pins are allocated to servos correctly before using
   check feed sweep angle is correct
*/
#include <Servo.h>

int buff[100];//an array to hold patterns received over RX serial
int j = 0; //used to increment serial integer receipt in serialEvent
int patternLength;//to hold # of ball types delivered

#define LED_PIN 13

Servo top; //name the top wheel servo control for the esc for the brushless motor
Servo bot; //bottom wheel ditto
Servo dir; //creates the 'direction servo', etc
Servo dis; //distance servo
Servo feed;//feeder servo

void setup()
{
  //says the servo called 'top' is on pin 0 etc
  top.attach(3);//40 max reverse, 90 zero, 160 max forwards, with calibrated car esc
  bot.attach(4);//40-90-160
  dir.attach(9);//change to make 80-120
  dis.attach(10);//change to make 100 level, 90 high
  feed.attach(8);//50 bottom, 95 top, delay 300 to sweep NOW 80B, 128T

  top.write(90); //esc initialization: 'min' speed for over 3 seconds?
  bot.write(90);
  dir.write(80); // to your left
  dis.write(90); // up
  feed.write(120); // near top
  delay(1000);
  dir.write(100);// to centre
  dis.write(110); //down
  feed.write(110); // down a bit
  delay(1000);
  dir.write(100);// to your right NB THIS SHOULD READ 120 NOT 100
  dis.write(90);// up
  delay(1000);
  dir.write(100);// to centre
  dis.write(100);// to level
  feed.write(120); // near top NB MAKE THIS 80 BOTTOM so the rest position is down
  delay(2000);

  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  randomSeed(analogRead(5));//I think this creates a different random number each time.
  Serial.println("ready");
  blinkLED(5);//blinks to indicate ready - and it works
}

void loop()
{
  while (Serial.available() > 0)
  {
    buff[j] = Serial.parseInt();
    j++;
    patternLength = j;
  }
  j = 0;//change the pattern in 'buff' immediately on receipt of serial data

  for (int i = 0; i <= patternLength - 6; i = i + 6)
  {
    //Serial.println(buff[i]);
    int t = buff[i];
    top.write(t); //motor speed 40-90-160 with car ESCs
    //Serial.println(t);
    
    int b = buff[i + 1];
    bot.write(b);
    
    int randNumber = random(-2*buff[i+4],2*buff[i+4]);//buff=0-5 from as3, so randNumber=0 to +/-10
    int r = buff[i + 2] + randNumber; //plus the random function so every loop is different;
    dir.write(r); //75-125 +/- adjustments
    
    int s = buff[i + 3];
    dis.write(s); //115-100-85 approx but max range 90 degrees so say 145-100-55
    
    if (buff[i + 5] == 30000)//if the pause signal has been received
      {
          feed.write(80);//puts feeder at bottom position
          blinkLED(2);//produces a constant blinking LED whilst robot is paused
      }
      //completes the pause pattern in 500ms (2 blinks), returns to look for serial info,
      //or else if this is a non-pause pattern, do below, ie the delay:
    else
      {        
      delay(buff[i + 5]-300); // as3 sends (i+5) = ballTime = total delay in ms
      feed.write(128);  //sweep up to feed the ball (previous version had this at 50 and was wrong)         
      delay(300);      //allow 300ms for the servo to do that. maybe longer?
      feed.write(80);  //returns for the next ball (previous version had this at 95 and was wrong)     
        
        if(buff[i]-buff[i+6] >50 && patternLength > i+6 && buff[i+6] <90 && buff[i+6] >0)
          {
            //Serial.println("panic mode top");
            //Serial.println(t);
            //Serial.println(t-buff[i+6]);
            top.write(90);
            delay(300);
          }
        if(buff[i+1]-buff[i+7] >50 && patternLength > i+6 && buff[i+7] <90 && buff[i+7] >0)
          {
            //Serial.println("panic mode bottom");
            //Serial.println(b);
            //Serial.println(b-buff[i+7]);
            bot.write(90);
            delay(300);          
          }      
      }
      //if a pause pattern is sent, break out of the 'for' loop and return to the start
      if(Serial.available() > 0)
      {
        break;
      }
  }
}

//function to blink the LED
void blinkLED(int count)
{
  for (int k = 0; k < count; k++)
  {
    digitalWrite(LED_PIN, HIGH);
    delay(250);
    digitalWrite(LED_PIN, LOW);
    delay(250);
  }
}
