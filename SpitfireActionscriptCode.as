// this is the first 'new' version after spitfire 1.0.3 worked (then bluetooth broke ... maybe)
// thereafter last known good version numbered sequentially with these header notes explaining what I'm working on
//THIS IS THE TOP WHEEL/BOTTOM WHEEL VERSION, WITH JUST TOP/BOTTOM/DISTANCE MATHS, NO SPIN/SPEED TO TOP/BOTTOM MATHS
//still can't get touch tap right so tapping actually works for incrementing spin or speed by 1 correctly
//now trying to get a 'callibrate' feature with a variable to alter 'distance' (saved to an XML file eventually). works!
// need to design a 'save' function so routines can be saved
// as3 receives from user values for speed(top), spin(bottom), direction, distance, direction, randomizer, delay
// as3 sends arduino values for:
// top wheel, bottom wheel, direction, distance, delay - of each ball in a pattern.

//REMEMBER TO ALTER LINE 343 ABOUT NEWLINE CHARACTER FOR BLUETOOTH AS NECESSARY: LAST WAS "," NOT "\n"

import flash.events.TouchEvent;
import flash.utils.Timer;
import flash.display.MovieClip;
//all next to get sending to arduino working:
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.utils.Endian;
import flash.utils.ByteArray;
//to use the vibration ANE: (not used during pc trials 'cos pc has no vibrate and it crashes if the function is included)
//import com.adobe.nativeExtensions.Vibration;
//to use the bluetooth ANE:
//import com.doitflash.air.extensions.bluetooth.Bluetooth;
//import com.doitflash.air.extensions.bluetooth.BluetoothEvent;
//to get an 'exit' programme button working, using the 'dispose' method of the blootooth ane to empty memory 
import flash.desktop.NativeApplication;
import flash.geom.Rectangle;
import flash.display.Shape;

//so movie clips work with mobile touch and gesture events
Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
Multitouch.inputMode = MultitouchInputMode.GESTURE;

//SOUNDS
var buttonPress:Sound = new mp3water//for arrows and buttons
var ballBounce:Sound = new mp3ball//for balls

//VIBRATION: unloaded this ane whilst doing bluetooth, so load back later
/*var vibe:Vibration;

function vibeR():void
{
if(Vibration.isSupported)
{
     vibe = new Vibration();
     vibe.vibrate(100);
}
}*/

//RESET BUTTON
mcReset.addEventListener(TouchEvent.TOUCH_TAP, onReset);
function onReset(event:TouchEvent):void
{
	if(mcReset.currentFrame == 2)
	{
		buttonPress.play();//play sound
		mcLeft.gotoAndStop(1);//make the adjust arrows grey
		mcRight.gotoAndStop(1);
		mcShort.gotoAndStop(1);
		mcLong.gotoAndStop(1);
		mcRobot.gotoAndStop(1);//PUT ROBOT ON 'GREY'
		mcReset.gotoAndStop(1);//grey out reset button again
		adj = [[0,0],[0,0],[0,0],[0,0]];//empty out the adjustment values
		myTextBox.text = ("move or place ball on table");
	}
	//or do nothing
}

//TEXT
var myFormat:TextFormat = new TextFormat();
myFormat.color = 0xFFFFFF;
myFormat.size = 18;    
myFormat.font = "Myriad Pro";
myFormat.align = TextFormatAlign.CENTER;

var myTextBox:TextField = new TextField();
myTextBox.backgroundColor = 0x336666;
myTextBox.defaultTextFormat = myFormat;
myTextBox.text = " ";
addChild(myTextBox);

myTextBox.visible = true;
myTextBox.wordWrap = true;
myTextBox.background = true;
myTextBox.width = 290;    
myTextBox.height = 50;    
myTextBox.x = 95;    
myTextBox.y = 60;

//INFO BUTTON BEHAVIOUR
//first make the buttons it regulates invisible
mcHelp.visible = false;
mcExit.visible = false;
mcBluetooth.visible = false;
tgButton.visible = false;

mcInfo.addEventListener(TouchEvent.TOUCH_BEGIN, showhideInfo);
function showhideInfo(event:TouchEvent):void
{
	buttonPress.play();//play sound
	//vibeR();//calls the vibrate function
	
	if(mcHelp.visible == false)
	{
		mcHelp.visible = true;
		mcExit.visible = true;
		mcBluetooth.visible = true;
		tgButton.visible = true;
	}
	else
	{
		mcHelp.visible = false;
		mcExit.visible = false;
		mcBluetooth.visible = false;
		tgButton.visible = false;
	}
}

//SHOW/HIDE HELP TEXT
mcHelp.addEventListener(TouchEvent.TOUCH_BEGIN, showhideHelp);
function showhideHelp(event:TouchEvent):void
{
	buttonPress.play();//play sound
	//vibeR();//calls the vibrate function	
	
	if(mcHelp.currentFrame == 1)
 	{
	 	myTextBox.visible = true;
		mcHelp.gotoAndStop(2);
 	}
	else
	{
		myTextBox.visible = false;
		mcHelp.gotoAndStop(1);
	}
}

//SHOW/HIDE TARGET
tgButton.addEventListener(TouchEvent.TOUCH_BEGIN, showhideTarget);
function showhideTarget(event:TouchEvent):void
{
	buttonPress.play();//play sound
	//vibeR();//calls the vibrate function	
	
	if(tgButton.currentFrame == 1)
 	{
		addChild(mcTarget);
		this.addChild(mcTleft);
		this.addChild(mcTright);
		this.addChild(mcTshort);
		this.addChild(mcTlong);		
		tgButton.gotoAndStop(2);
		myTextBox.visible = true;
		myTextBox.text = "tap the target to fire a ball\ntap arrows until ball hits target";
 	}
	else if(tgButton.currentFrame == 2)
	{
		removeChild(mcTarget);
		removeChild(mcTleft);
		removeChild(mcTright);
		removeChild(mcTshort);
		removeChild(mcTlong);
		tgButton.gotoAndStop(1);
		myTextBox.text = "move or place ball on table";
	}
}

//TARGET FOR INITIAL ROBOT CALLIBRATION
//start with the target removed from the stage
		this.removeChild(mcTarget);
		this.removeChild(mcTleft);
		this.removeChild(mcTright);
		this.removeChild(mcTshort);
		this.removeChild(mcTlong);
//make a callibration pattern
var callibPattern:Array =[131,90,100,88,0,2000];//= a ball at '67' top wheel (67 app = 131 ard) speed landing on target
callibPattern.push(" ");//just to match line about 1022

//mcTarget.addEventListener(TouchEvent.TOUCH_BEGIN, onTargetTap);
mcTarget.addEventListener(TouchEvent.TOUCH_END, onTargetRelease);

/*function onTargetTap(event:TouchEvent):void
{
	if(event.target.currentFrame == 1)
	{
		_ex.sendMessage(callibPattern.join(","));//_ex.sendMessage(pattern.join(",")); //write the pattern array as a string to arduino
	}
	else
	{
		_ex.sendMessage(pausePattern);// send 'PAUSE' pattern to arduino
	}
}*/
function onTargetRelease(event:TouchEvent):void
{
	if(event.target.currentFrame == 1)
	{
		event.target.gotoAndStop(2);
		myTextBox.text = "when ball hits target\ntap target to exit setup";
	}
	else
	{
		event.target.gotoAndStop(1);
		myTextBox.text = ("move or place ball on table");
		this.removeChild(mcTarget);
		this.removeChild(mcTleft);
		this.removeChild(mcTright);
		this.removeChild(mcTshort);
		this.removeChild(mcTlong);
		tgButton.gotoAndStop(1);
		
	}
}

//set up arrows to adjust the callibratePattern to hit the target
var Tadj:Array = [0,0];//to hold the callibration adjustment values [dir,dis]
//var leftRight:Boolean;
var TarrIndex:int;
//add event listeners to 'add' instances 
var Tarrows:Array = [mcTleft,mcTright,mcTshort,mcTlong];
Tarrows.forEach(setupTars);
function setupTars(targetarrow:MovieClip, index:int, array:Array)
{
	targetarrow.addEventListener(TouchEvent.TOUCH_BEGIN, TarrowNudge);
	targetarrow.addEventListener(TouchEvent.TOUCH_END, TarrowPlay);
}
//call functions to change callibration numbers with arrows

function TarrowNudge(event:TouchEvent):void
{
			TarrIndex = Tarrows.indexOf(event.currentTarget);//so you know which Tarrow has been clicked on release
			buttonPress.play();//play sound			
			//vibeR();//calls the vibrate function
			trace("just touched a Tarrow");
			
			if(TarrIndex ==0)
			{
				Tadj[0] = Tadj[0]-2;//adjust direction to the left
			}
			else if(TarrIndex ==1)
			{
				Tadj[0] = Tadj[0]+2;//to the right
			}
			else if(TarrIndex ==2)
			{
				Tadj[1] = Tadj[1]+1;//shorter
			}
			else if(TarrIndex ==3)
			{
				Tadj[1] = Tadj[1]-1;//longer
			}
			trace("new callibration values are: " + Tadj);
}

function TarrowPlay(event:TouchEvent):void
{
		
		trace("adjusted value and re-sent to robot");
		callibPattern[2] = 100 + Tadj[0];
		callibPattern[3] = 88 + Tadj[1];
		trace(callibPattern);
//		_ex.sendMessage(callibPattern.join(","));
}

//SPEED AND SPIN VALUE TEXT BOXES
var ssFont = new numbersfont();
var ssFormat:TextFormat = new TextFormat();
ssFormat.color = 0xFFFFFF;    
ssFormat.size = 40;    
ssFormat.font = ssFont.fontName;
ssFormat.align = TextFormatAlign.CENTER;

var sFont = new numbersfont();
var sFormat:TextFormat = new TextFormat();
sFormat.color = 0xFFFFFF;    
sFormat.size = 30;    
sFormat.font = sFont.fontName;
sFormat.align = TextFormatAlign.CENTER;

var speedTextBox:TextField = new TextField();
var speedText:int;//see below in SPEED
speedTextBox.defaultTextFormat = ssFormat;
speedTextBox.embedFonts = true;
//speedTextBox.AntiAliasType = AntiAliasType.ADVANCED;
speedTextBox.text = speedText.toString();
//addChildAt(speedTextBox,0);//not yet!
speedTextBox.width = 100;    
speedTextBox.height = 100;    
speedTextBox.x = 99.5;    
speedTextBox.y = 129;

var spinTextBox:TextField = new TextField();
var spinText:int;//see below in SPEED
spinTextBox.defaultTextFormat = sFormat;
spinTextBox.embedFonts = true;
spinTextBox.text = spinText.toString();
//addChildAt(spinTextBox,0);//not yet!
spinTextBox.width = 100;    
spinTextBox.height = 100;    
spinTextBox.x = 99.5;    
spinTextBox.y = 255.5;

//GRAPHICS FOR THOSE BOXES HIDDEN TOO
this.removeChild(topCounter);
this.removeChild(botCounter);

//SPEED (actually top wheel in this version)
//create a hit area for dragging
var speedBox:MovieClip = new MovieClip();
speedBox.graphics.beginFill(0xFF0000,0);//0 = hit area is invisible
speedBox.graphics.drawRect(0,120,380,90);
speedBox.graphics.endFill();
//this.addChild(speedBox);//not yet!
speedBox.addEventListener(TouchEvent.TOUCH_BEGIN, moveSpeed);//allows dragging the speed bar
speedBox.addEventListener(TouchEvent.TOUCH_TAP, tapSpeed);//for incremental tapping
stage.addEventListener(TouchEvent.TOUCH_END, dropSpeed);

function tapSpeed(event:TouchEvent):void
{
	buttonPress.play();//play sound
	if(event.stageX > speed.x)
	{
		speed.x = speed.x +(350/125);//occasionally
 jumps 0 or 2 because of fractions in mapping I think.
	}
	else
	{
		speed.x = speed.x -(350/125);
	}
}

var DragBounds_1:Rectangle = new Rectangle(50, 160, stage.stageWidth-100, 0);

function moveSpeed(event:TouchEvent):void
{
	speed.startTouchDrag(event.touchPointID, false, DragBounds_1);
}
function dropSpeed(event:TouchEvent):void
{
	speed.stopTouchDrag(event.touchPointID);
	if(speed.x >112 && speed.x <186)//if in neutral zone force to 90
	{
		speed.x = 149;
	}
	//TOP LIMITS: if Top >= Bot and lock is on, force bot to sensible maximum
	else if(speed.x >= spin.x && spin.x > 225 && mclocK.currentFrame == 1)
	{
		spin.x = 225;
		spinText = map(spin.x, 50,400,-25,100);
	}
	//+ force top to a min. could get down to 240 if bot >=105, but dunno if I need bother coding that
	else if(speed.x >= spin.x && speed.x < 258 && spin.x >= 149 && mclocK.currentFrame == 1)
	{
		speed.x = 258;
	}
	//LOOP LIMITS: if BOT-VE and the lock is ON, force bottom to sensible minima
	else if(spin.x < 149 && speed.x < ((112-spin.x)*5/4)+325 && mclocK.currentFrame == 1)
	{
		speed.x = 325+(112-spin.x)*5/4;//nearly works. check it out carefully for strange traces
	}		
	//CHOP LIMITS: if BOT > TOP and lock is on, limit bot a sensible min
	else if(spin.x > speed.x && spin.x <258 && mclocK.currentFrame == 1)
	{
		spin.x = 258;
		spinText = map(spin.x, 50,400,-25,100);
	}
	//+ constrain bot to a max
	else if(spin.x > speed.x  && speed.x == 149 && spin.x >330 && mclocK.currentFrame == 1)
	{
		spin.x = 330;
		spinText = map(spin.x, 50,400,-25,100);
	}
	//+ constrain top to zero max
	else if(spin.x > speed.x && speed.x >149 && mclocK.currentFrame == 1)
	{
		speed.x = 149;
	}
	//DEEP CHOP LIMITS: if TOP -ve limit to a min
	else if(speed.x <90 && mclocK.currentFrame == 1)
	{
		speed.x = 90;
	}
	// if top -ve limit bot to a min
	else if(speed.x <149 && spin.x <330 && mclocK.currentFrame == 1)
	{
		spin.x = 330;
		spinText = map(spin.x, 50,400,-25,100);
	}
	//speedText control:
	if(speed.x != 149)
	{
		speedText = map(speed.x, 50,400,-25,100);
	}
	else
	{
		speedText = 0;
	}
	speedTextBox.text = speedText.toString();
	spinTextBox.text = spinText.toString();
}

//SPIN (actually bottom wheel in this version)
//create a zone to react to dragging
var spinBox:MovieClip = new MovieClip();
spinBox.graphics.beginFill(0xFF0000,0);
spinBox.graphics.drawRect(0,240,380,90);
spinBox.graphics.endFill();
//this.addChild(spinBox); not yet
spinBox.addEventListener(TouchEvent.TOUCH_BEGIN, moveSpin);//allows dragging the speed bar
spinBox.addEventListener(TouchEvent.TOUCH_TAP, tapSpin);//for incremental tapping
stage.addEventListener(TouchEvent.TOUCH_END, dropSpin);

function tapSpin(event:TouchEvent):void
{
	buttonPress.play();//play sound
	if(event.stageX > spin.x)
	{
		spin.x = spin.x +(350/125);//occasionally jumps 0 or 2 because of fractions in mapping I think.

	}
	else
	{
		spin.x = spin.x -(350/125);
	}
}

var DragBounds_2:Rectangle = new Rectangle(50, 280 , stage.stageWidth-100, 0);

function moveSpin(event:TouchEvent):void
{
	spin.startTouchDrag(event.touchPointID, false, DragBounds_2);
}
function dropSpin(event:TouchEvent): void
{
	spin.stopTouchDrag(event.touchPointID);//
	//if bot in neutral zone force to 90
	if(spin.x >112 && spin.x <186)
	{
		spin.x = 149;
	}
	//TOP LIMITS: if TOP >= BOT and lock is on, constrain bot to a max
	else if(speed.x >= spin.x && spin.x > 225 && mclocK.currentFrame == 1)
	{
		spin.x = 225;
	}
	//+ force top to a min. could get down to 240 if bot >=105, but dunno if I need bother coding that
	else if(speed.x >= spin.x && speed.x < 258 && spin.x >= 149 && mclocK.currentFrame == 1)
	{
		speed.x = 258;
		speedText = map(speed.x, 50,400,-25,100);
	}
	//LOOP LIMITS: if BOT-VE and the lock is ON, force top to sensible minimum
	else if(spin.x < 149 && speed.x < ((112-spin.x)*5/4)+325 && mclocK.currentFrame == 1)//was 300+(112-spin.x)*5/4
	{
		speed.x = 325+(112-spin.x)*5/4;//nearly works. check it out carefully for strange traces
		speedText = map(speed.x, 50,400,-25,100);
	}	
	//CHOP LIMITS: if BOT > TOP and lock is on, limit bot a sensible min
	else if(spin.x > speed.x && spin.x <258 && mclocK.currentFrame == 1)
	{
		spin.x = 258;
	}
	//+ constrain bot to a max if top is zero
	else if(spin.x > speed.x && speed.x == 149 && spin.x >330 && mclocK.currentFrame == 1)
	{
		spin.x = 330;
	}
	//+ constrain top to a maximum of zero
	else if(spin.x > speed.x && speed.x >149 && mclocK.currentFrame == 1)
	{
		speed.x = 149;
		speedText = 0;
	}
	//DEEP CHOP LIMITS: if TOP -ve limit to a min
	else if(speed.x <90 && mclocK.currentFrame == 1)
	{
		speed.x = 90;
		speedText = map(speed.x, 50,400,-25,100);
	}
	// if top -ve limit bot to a min
	else if(speed.x <149 && spin.x <330 && mclocK.currentFrame == 1)
	{
		spin.x = 330;
	}
	//spinText control:
	if(spin.x != 149)
	{
		spinText = map(spin.x, 50,400,-25,100);
	}
	else
	{
		spinText = 0;
	}
	spinTextBox.text = spinText.toString();
	speedTextBox.text = speedText.toString();
}

//LOCK to enable distance slider (starts out removed)
mclocK.visible = false;
mclocK.addEventListener(TouchEvent.TOUCH_TAP, doLock);

function doLock(event:TouchEvent):void
{
	buttonPress.play();//play sound
	//vibeR();//calls the vibrate function
	if(event.target.currentFrame == 1)//if the lock is ON
	{
		event.target.gotoAndStop(2);//turn lock OFF
		disT.gotoAndStop(2);//and the disT slider yellow
		disT.y = 220;//and make sure it's on the stage
	}
	else//if the lock is OFF
	{
		event.target.gotoAndStop(1);
		disT.gotoAndStop(1);
	}
}

//DISTANCE SLIDER (starts out removed)
this.removeChild(mcDist);//the hit zone
var DragBounds_3:Rectangle = new Rectangle(420,80,0,280);//change to a circumference if possible one day
mcDist.addEventListener(TouchEvent.TOUCH_BEGIN, movedisT);//allows dragging the distance bar
stage.addEventListener(TouchEvent.TOUCH_END, dropdisT);

		function movedisT(event:TouchEvent):void
		{
			disT.startTouchDrag(event.touchPointID, false, DragBounds_3);//disT is the indicator button only
		}
		function dropdisT(event:TouchEvent):void
		{
			disT.stopTouchDrag(event.touchPointID);
		}

//this alters direction and length - nearly works - gotta test for degree and usefulness
//LEFT/RIGHT/LONG/SHORT ARROW ADJUSTERS

var adj:Array = [[0,0],[0,0],[0,0],[0,0]];//to hold the adjustment numbers [dir,dis]
var leftRight:Boolean;
var arrIndex:int;
//add event listeners to 'add' instances 
var arrows:Array = [mcLeft,mcShort,mcRight,mcLong];
arrows.forEach(setupArrow);
function setupArrow(addArrow:MovieClip, index:int, array:Array)
{
	addArrow.addEventListener(TouchEvent.TOUCH_BEGIN, arrowFlash);
	addArrow.addEventListener(TouchEvent.TOUCH_END, arrowNudge);
}
//call functions to change pattern numbers with arrows
function arrowFlash(event:TouchEvent):void
{
	if(event.currentTarget.currentFrame == 2 && balls[index].currentFrame == 2)//if arrow is visible ...
	{
		buttonPress.play();//play sound			
		//vibeR();//calls the vibrate function
		trace("just touched an arrow");
	}
}
function arrowNudge(event:TouchEvent):void
{
	arrIndex = arrows.indexOf(event.currentTarget);//so you know which arrow has been clicked on release
	if (balls[index].currentFrame==2 && event.currentTarget.currentFrame == 2)//if a ball is hilited and arrows are
	{
			if(arrIndex ==0)
			{
				adj[index][0] = adj[index][0]-2;//adjust direction value for that ball to the left
			}
			else if(arrIndex ==1)
			{
				adj[index][1] = adj[index][1]+1;//shorter
			}
			else if(arrIndex ==2)
			{
				adj[index][0] = adj[index][0]+2;//to the right
			}
			else if(arrIndex ==3)
			{
				adj[index][1] = adj[index][1]-1;//longer
			}
	}
	else
	{
		trace ("no ball is selected");
	}
}

//define the PATTERN arrays we'll be using
var pausePattern:String = "90,90,100,100,0,30000,";//to deliver a 'pause' command - see line ~ 716
//note those values need to be directly fed to the arduino, unmapped. 5000 delay = 5000*20ms = 100 seconds
var pattern:Array = new Array();//current pattern of balls
var testPattern:Array = new Array();
var ballTimer: Timer = new Timer (50);//50 means 50/1000 = .05 second intervals: 1/20 of a second, 20 per second
// so if the timer records 0.5 of a second, it records the number 10; 0.05 of a second it records the number 1.
// so 50*(ballTimer) = actual time between balls
var testTimer: Timer = new Timer (50);
var bytes:ByteArray = new ByteArray();//to do with commmunicating with arduino
var table:Rectangle = new Rectangle(70, 0, stage.stageWidth-140, stage.stageHeight-100);//seems ok for different screens

//set up RANDOMIZING OF DIRECTION capability
var dirTimer:Timer = new Timer(500);//a timer in 1/2 second increments for adding a direction randomizer (318ish)
var priorGI = 0;//set up this variable to compare to the current grid[index][4] value, below
dirTimer.addEventListener(TimerEvent.TIMER, dirRandomize);//this fires every second

				function dirRandomize(event:TimerEvent):void
				{ 
					if(grid[index][4] <5 && grid[index][4] >= priorGI)//approaching 5
					{
						priorGI = grid[index][4];
						trace("priorGI = " + priorGI);
						grid[index][4] = ++grid[index][4];//make the value 1 larger (each 1/2 second)
						trace("randomise direction (+) to: " + grid[index][4]);
						balls[index].dirL.gotoAndStop(grid[index][4]);
					}
					else if(grid[index][4] == 5)
					{
						priorGI = grid[index][4];
						trace("priorGI = " + priorGI);
						grid[index][4] = 4;
						trace("randomise direction (--) to: " + grid[index][4]);
						balls[index].dirL.gotoAndStop(grid[index][4]);
					}
					else if(grid[index][4] >0 && grid[index][4] < priorGI)//approaching 1
					{
						priorGI = grid[index][4];
						trace("priorGI = " + priorGI);
						grid[index][4] = --grid[index][4];//make the value 1 smaller each 1/2 second
						trace("randomise direction (-) to: " + grid[index][4]);
						balls[index].dirL.gotoAndStop(grid[index][4]);
					}
					else
 if(grid[index][4] == 0)
					{
						priorGI = grid[index][4];
						trace("priorGI = " + priorGI);
						grid[index][4] = 1;
						trace("randomise direction (++) to: " + grid[index][4]);
						balls[index].dirL.gotoAndStop(grid[index][4]);
					}
				}

//MAPPING FUNCTION FOR SPIN/SPEED to SERVO COMMANDS:
//from http://lab.joelgillman.com/archives/87_map-function

function map(v:int, a:int, b:int, x:int = 0, y:int = 1):int
	{
    return (v == a) ? x : (v - a) * (y - x) / (b - a) + x;
	}

//used http:code.tutsplus.com/tutorials/as3-101-arrays-basix--active-1703 to streamline these ball behaviours
//BALLS
var balls:Array = [ball0, ball1, ball2, ball3];//create an array to hold refs to the balls
var grid:Array = [				//and their pattern values (no need for the delay value)
				  [149,149,0,0,0],  //top/bottom/direction/distance/DIR RANDOMIZER - delays are pushed in during recording
				  [149,149,0,0,0],
				  [149,149,0,0,0],
				  [149,149,0,0,0]
				  ];
var gridOld:Array = [0,0,0,0];//for splicing adjustments by comparing to dir values in pattern							    
var index:int; //to hold indexOf values below to identify which ball was clicked
var ballsLock:Array = [0,0,0,0];//an array to hold the state of the lock wrt each ball's last change

balls.forEach(setupBall);//add event handlers to the  balls
function setupBall(ballmaster:MovieClip, index:int, array:Array)
{
	ballmaster.addEventListener(TouchEvent.TOUCH_BEGIN, touchBall);
	//ballmaster.addEventListener(TouchEvent.TOUCH_MOVE, moveLine);
	ballmaster.addEventListener(TouchEvent.TOUCH_END, dropBall);
	ballmaster.addEventListener(TransformGestureEvent.GESTURE_SWIPE, swipeBall);
}

//LINES
//create an array to hold refs to the lines - not working. a bit stuck. forget for a bit.
/*var lineS:Array = [];
//make four lines and add them to the array
for (var i:int = 0; i < 4; i++)
{
	var ballLine:Shape = new Shape();
	ballLine.graphics.lineStyle(1,0xFFFFFF, 0.5);
	ballLine.graphics.moveTo(225,10);
	ballLine.graphics.lineTo(235, 20);
	ballLine.name = "line" + i;
	trace(ballLine.name);
	this.addChild((ballLine));
	lineS.push(ballLine);
	trace(lineS);
}*/

function swipeBall(event:TransformGestureEvent):void//doesn't seem to work at all
{
	if(event.currentTarget.y < 450) 
		{
		event.currentTarget.x = 149;
		event.currentTarget.y = 30;
		event.currentTarget.gotoAndStop(1);
		adj[index][0] = 0;//and return the ball to a zero state of adjustment index numbers
		adj[index][1] = 0;
		}
}
//a function (used BELOW) to turn all the balls grey when required
function turnOff(ballmaster:MovieClip, index:int, array:Array):void
{
	ballmaster.gotoAndStop(1);
}

function touchBall(event:TouchEvent):void
{
	index = balls.indexOf(event.currentTarget);//save the ball's index position (0,1,2 or 3) in the balls array to 'index'
	ballBounce.play();//sound
	//vibeR();//calls the vibrate function

	if(event.currentTarget.currentFrame == 1)//if ball is GREY and...
	{
			balls.forEach(turnOff);//turn 'em all grey
			event.currentTarget.gotoAndStop(2);//turn THIS ball yellow (so it's values can be altered)
			
			if(mcLong.currentFrame == 2)//if grey and LONG ARROW is visible
			{
				mcLeft.gotoAndStop(2);//make the adjust arrows visible, ready to adjust
				mcRight.gotoAndStop(2);
				mcShort.gotoAndStop(2);
				mcLong.gotoAndStop(2);
				mcRobot.gotoAndStop(1);//keep ROBOT ON 'PLAY'
			}
			else if(mcRec.currentFrame !==3)//otherwise if grey, unless you're recording so mcRec is on(3)
			{			
				event.currentTarget.startTouchDrag(event.touchPointID, false, table);//constrain dragging to the table dims
				if(ballsLock[index] == 1)//if the ball was UNLOCKED last time
				{
					mclocK.gotoAndStop(2);//show unlocked on activating the ball this time
					disT.gotoAndStop(2);//make button visible
				}
				else
				{
					mclocK.gotoAndStop(1);//show locked on activating the ball this time
					disT.gotoAndStop(1);//keep button invisible
				}
				mcRobot.gotoAndStop(1);//hide the 'send to robot' button
				mcRec.gotoAndStop(1);//hide the 'record' button
				this.addChildAt(speedTextBox,0);//add all the behaviours for robot setting
				this.addChildAt(spinTextBox,1);
				this.addChildAt(topCounter,2);
				this.addChildAt(botCounter,3);
				mclocK.visible = true;
				this.addChildAt(speedBox,4);
				this.addChildAt(spinBox,5);
				this.addChildAt(mcDist,6);
				spin.x = grid[index][1];//the bars should move to the ball's previously-set spin and speed values
				speed.x = grid[index][0];//all spin and speed values are between 50 and 400
				disT.y = map(grid[index][3], 70, 130, 80, 360);//NEW: robot disT blob moves to previously set value too				
				trace("disT.y = " + disT.y);
				trace("grid[index][3] = " + grid[index][3]);
				spin.gotoAndStop(2);//and light them up
				speed.gotoAndStop(2);
			}
			   //otherwise do nothing
	}
	else if(event.currentTarget.currentFrame==2 && mcLong.currentFrame !== 2)//if ball is YELLOW and not in adjust mode
	{
			event.currentTarget.gotoAndStop(1);//turn it grey
			dirTimer.start();//activating direction randomizer functions @ approx 210				
			spin.gotoAndStop(1);//grey out spin speed and disT buttons
			speed.gotoAndStop(1);
			disT.gotoAndStop(1);
			mcRec.gotoAndStop(2);//light up the 'record' button
			grid[index][1] = spin.x;//ball's S&S values re-set to where they have just been moved to
			grid[index][0] = speed.x;
			trace("grid[index][3] = " + grid[index][3]);//to check the above step does as predicted			
			this.removeChild(speedTextBox);
			this.removeChild(spinTextBox);
			this.removeChild(topCounter);
			this.removeChild(botCounter);
			mclocK.visible = false; //leave visible whilst experimenting
			this.removeChild(speedBox);
			this.removeChild(spinBox);
			this.removeChild(mcDist);
			
	//map DIRECTION directly from screen position to an arduino servo position instruction
	var tanangle:Number;//create a variable to hold the tangent of the angle the current ball makes with the robot
	tanangle = (event.currentTarget.x - 225)/(event.currentTarget.y + 170)
	var angle:int;//this is the actual angle
	angle = Math.atan(tanangle) * 180/Math.PI;//that last bit to turn atan from radians to degrees
	trace("direction angle = " + angle);//works
	//grid[index][2] = map(angle,-12,12,70,130) + adj[index][0];//-12,12 is range of app angles; 75,125 is robot range.
	grid[index][2] = map(angle,-12,12,70,130) + Tadj[0];//-12,12 is range of app angles; 75,125 is robot range.

	trace("on dropping ball " + grid[index] + " adjusted direction = " + adj[index][0] + ", adjusted distance = " + adj[index][1]);
	trace("pre-sent pattern = " + event.currentTarget.name, grid[index]);//trace the ball name and it's SSDD values as adjusted: works	
			
	if(mclocK.currentFrame == 2)//if UNLOCKED
	{
		ballsLock[index] = 1;//record it as unlocked in the ballLock array, for next time
		grid[index][3] = map(disT.y, 80, 360, 70, 130);//sets the distance in pattern to the disT blob as just positioned
		trace("disTance = " + disTance);
	}
	else //if LOCKED
	{
	ballsLock[index] = 0;//record it as locked			
	var disTance:int = 0;//to force this value into being a whole number, below
	
	if(spin.x >148 && speed.x >= spin.x)//if TOPSPIN or NOSPIN - good TOP mid-table.
		{
		disTance = (map(speed.x,149,400,0,70)/(map(spin.x,149,400,8,78)) + (map(spin.x,149,400,0,30)));
		trace("top to distance = " + map(speed.x,149,400,0,70)/map(spin.x,149,400,8,78));
		trace("distance = " + disTance);
		//grid[index][3] = map(event.currentTarget.y, 550,700,85,81)+ disTance - adj[index][1];//DIS to empty out adjustment arrows prior to recording see 289
		grid[index][3] = map(event.currentTarget.y, 550,700,85,81)+ disTance + Tadj[1];//DIS to empty out adjustment arrows prior to recording see 289
		trace("mapped ball length = " + map(event.currentTarget.y, 550,700,85,81));//
		}
	else if(speed.x >148 && spin.x > speed.x)//if CHOP - very good mid-table
		{
		disTance = (1/2)*Math.pow(map(spin.x,258,400,95,1000),1/2);//was 100,1000
		//grid[index][3] = map(event.currentTarget.y, 550,700,85,79)+ disTance - adj[index][1];//DIS to empty out adjustment arrows prior to recording see 289
		grid[index][3] = map(event.currentTarget.y, 550,700,85,79)+ disTance + Tadj[1];//DIS to empty out adjustment arrows prior to recording see 289
		trace("mapped ball length = " + map(event.currentTarget.y, 550,700,85,79));
		trace("distance with 1/4 power function in = " + disTance);
		}
	else if(speed.x <149)//if HEAVY CHOP
		{
		disTance = map(speed.x,50,112,38,44) + map(spin.x,186,400,19,32);//
		//grid[index][3] = map(event.currentTarget.y, 550,700,19,14)+ disTance - adj[index][1];//DIS to empty out adjustment arrows prior to recording see 289
		grid[index][3] = map(event.currentTarget.y, 550,700,19,14)+ disTance + Tadj[1];//DIS to empty out adjustment arrows prior to recording see 289
		trace("mapped ball length = " + map(event.currentTarget.y, 550,700,19,14));	
		}
	else if(spin.x <149)//if LOOP
		{
		disTance = map(speed.x,149,400,0,14) + (map(spin.x,50,112,0,7));//spin range was 0-10
		//grid[index][3] = map(event.currentTarget.y, 550,700,70,66)+ disTance - adj[index][1];//DIS to empty out adjustment arrows prior to recording see 289
		grid[index][3] = map(event.currentTarget.y, 550,700,70,66)+ disTance + Tadj[1];//DIS to empty out adjustment arrows prior to recording see 289
		trace("mapped ball length = " + map(event.currentTarget.y, 550,700,70,66));		
		}
		trace("disTance = " + disTance);//correct. distance has been callibrated to wheel speeds, not their tablet co-ordinates.
	}
	}
else if(event.currentTarget.currentFrame==2 && mcLong.currentFrame == 2)//if ball is YELLOW and in ADJUST MODE
	{
				event.currentTarget.gotoAndStop(1);//turn it grey
				mcRobot.gotoAndStop(2);//turn the 'send to' mcRobot on (changing it from 'reset' OR 'PAUSE')
				mcReset.gotoAndStop(2);//activate the 'RESET' button
				mcLeft.gotoAndStop(1);//make
 the adjust arrows invisible
				mcRight.gotoAndStop(1);
				mcShort.gotoAndStop(1);
				mcLong.gotoAndStop(2);//but keep this one visible to stay in potential adjust mode	
				
				myTextBox.text = ("restart : select ball : or reset");
				// splice the new position values (as adjusted by the arrows) into pattern
				for (var p:int = 2; p<pattern.length-2; p+=6)//for p=2,8,14 etc ie 3rd in each pattern block of 6 values
					{
						trace ("whole last recorded pattern was " + pattern);
						trace ("p = " + p);
						trace ("pattern[p] = " + pattern[p]);
						trace ("gridOld[index] = " + gridOld[index]);

					if (pattern[p] == gridOld[index])//ie the pattern's direction value = this ball's direction value
						{
						pattern.splice(p, 1, grid[index][2]+adj[index][0]);//alter the direction to the adjusted value of that ball
						pattern.splice(p+1, 1, grid[index][3]+adj[index][1]);//alter the distance to the adjusted value of that ball
						//then save this ball's new dir value to gridOld so can compare it when next adjusting a ball's dir and dis
						gridOld[index] = grid[index][2]+adj[index][0];
						trace("new direction should be " + pattern[p]);
						trace("new distance should be " + pattern[p+1]);
						}
					}
	}
}

//and on ball release
function dropBall(event:TouchEvent):void //WORK OUT THE =/- ON THESE ADJUSTMENTS: +/-5 CURRENTLY NOT TESTED.
{
	event.currentTarget.stopTouchDrag(event.touchPointID);//drop it
	dirTimer.reset();//stop the dirTimer and reset currentCount to zero.
	trace("onDrop, ball y-coordinate = " + event.currentTarget.y);
	
	//RETURN BALLS TO BASKET if they're dragged to the top of the screen - and hide the S&S counters
	if(event.currentTarget.y <= 400) 
		{
		event.currentTarget.x = 149;
		event.currentTarget.y = 30;
		event.currentTarget.gotoAndStop(1);
		adj[index][0] = 0;//and return the ball to a zero state of adjustment index numbers
		adj[index][1] = 0;
		spin.gotoAndStop(1);//grey out spin speed and disT buttons
		speed.gotoAndStop(1);
		disT.gotoAndStop(1);
		this.removeChild(speedTextBox);
		this.removeChild(spinTextBox);
		this.removeChild(topCounter);
		this.removeChild(botCounter);
		mclocK.visible = false; //leave visible whilst experimenting
		this.removeChild(speedBox);
		this.removeChild(spinBox);
		this.removeChild(mcDist);
		//would be nice to turn randomiser to zero too
		}
	//snap balls to useful area of the table if they're dropped too short
	if (event.currentTarget.y > 400 && event.currentTarget.y < 550)//short but too near the net
		{
		event.currentTarget.y = 550;
		}

	if(event.currentTarget.currentFrame==1 && mcLeft.currentFrame ==1)//if ball went GREY on clicking, not in adjust mode:
		{
					if(mcRec.currentFrame ==3)//and if you've pressed 'record' so you're recording a drill now
					{
						myTextBox.text = (" ");
					}
					else
					{
						myTextBox.text = ("add ball        or    record drill");
					}
		}
	if(event.currentTarget.currentFrame==2)//if ball went YELLOW on clicking, when dropped:
		{
			if(mcLeft.currentFrame ==2)//and if arrows are visible (so robot is paused), then on release
			{
				myTextBox.text = ("tap arrows to adjust, then tap ball to save");
			}
			if(mcLeft.currentFrame ==1)//but if arrows are invisible, then on release
			{
				myTextBox.text = ("set wheelspeeds by dragging buttons then tap ball to save");
			}				
		}
	//X this could be all in on tap instead of on drop, but it does work and allows grid values mapped to ard wheels and back
	if(mcRec.currentFrame ==3)//SPECIAL: if you've pressed 'RECORD' (so it's now on 'STOP') before you click this ball
	{
			spin.gotoAndStop(1);//get these grey
			speed.gotoAndStop(1);//
			spin.x = grid[index][1];//bars  move - maybe not needed cos the correct grid values are already set
			speed.x = grid[index][0];
			event.currentTarget.play();//flash the ball and return to frame 1, grey
			
	//ADD IN HERE THE SPIN AND SPEED MATHS TO CALIBRATE grid[index][0,1] TO ARDUINO VALUES JUST BEFORE SENDING THEM TO THE ROBOT
			var Top:int;//create Top variable and map it from the speed.x value
			var Bot:int;//create Bot variable and map it from the spin.x value
			if(spin.x >182)//if bottom wheel is spinning forwards use this mapping:
			{
				Bot = map(spin.x,186,400,96,160);//map to forwards arduino values
			}
			else if(spin.x == 149)//if bottom wheel is stationary
			{
				Bot = 90;
			}
			else //if spinning backwards use this mapping:
			{
				Bot = map(spin.x,112,50,84,70);//map to backwards arduino values
			}
			grid[index][1] = Bot;
			trace ("bot = " + Bot + ", grid[1] = " + grid[index][1]);//	
			
			if(speed.x >182)//if top wheel spinning forwards use this mapping:
			{
				Top = map(speed.x,186,400,96,160);//map to forwards arduino values
			}
			else if(speed.x == 149)//in the central 'zero' zone
			{
				Top = 90;
			}
			else //if backwards use this mapping:
			{
				Top = map(speed.x,112,50,84,78);//map to backwards arduino values
			}
			grid[index][0] = Top;
			trace ("top = " + Top + ", grid[0] = " + grid[index][0]);//	
			
			for (var k:int = 0; k<grid[index].length; k++)
				{			
					pattern.push(grid[index][k]);//push that ball's grid values into 'pattern'
				}
			var ballTime:int;
			if(ballTimer.currentCount < 12)
			{
				ballTime = 600;//so set minimum time between balls at 600ms (ballTimer clocks every 50ms)
			}
			else
			{
				ballTime = (ballTimer.currentCount)*50;//= total delay between balls in milliseconds
			}
			trace("delay = " + ballTime);
			pattern.push(ballTime);//push in time from last ball tapped into the pattern array
			testPattern.push(ballTime);//push time from last ball into the testPattern array
			testPattern.push(index);//identifies this as the ball just clicked
			trace("on dropping ball whilst recording: " + adj[index][0] + ", " + adj[index][1]);
			trace("pattern:" + pattern);
			trace("test pattern:" + testPattern);
			//then save this ball's dir value to gridOld so can compare it when adjusting a ball's dir and dis
			gridOld[index] = pattern[pattern.length - 4];
			trace("ball" + [index] + " direction = " + gridOld[index]);
			//then re-set the grid[index] values to pre-maths so the buttons stay in the right places on screen
			grid[index][1] = spin.x;
			grid[index][0] = speed.x;
			ballTimer.reset();
			ballTimer.start();
	}
}

//RECORD/PAUSE/TEST/BIN/SAVE FUNCTIONS
//create variables for the buttons I'm gonna call from the mcRec button below
var mcLoser:mcLose
var mcSaver:mcSave
//set up the mcRec button
mcRec.addEventListener(TouchEvent.TOUCH_TAP, changeState);

function changeState(event:TouchEvent):void //moves the mcRec clip forward a frame
{
	balls.forEach(turnOff);//turn all balls off (ie grey: on to frame 1)
	//vibeR();//calls the vibrate function
	
	if (mcRec.currentFrame ==2)//ie if you press 'RECORD'
		{
		buttonPress.play();	
		pattern.splice(0,pattern.length);//empties the pattern for a fresh start.
		testPattern.splice(0,testPattern.length);
		trace("emptied out the pattern:" + pattern);
		trace("emptied out the testPattern:" + testPattern);
		trace("start recording");//all ok
		ballTimer.start();
		mcRec.gotoAndStop(3);//so after this point mcRec !=1, so this block of code isn't repeated
		myTextBox.text = ("tap balls             then tap stop");
		}
	else if(mcRec.currentFrame ==3)//ie if you press 'STOP'
		{
		buttonPress.play();
			var ballTime:int;
			if(ballTimer.currentCount < 12)
			{
				ballTime = 600;
			}
			else
			{
				ballTime = (ballTimer.currentCount)*50;//= total delay between balls in milliseconds
			}
			trace("delay on stopping recording" + ballTime);
		pattern.splice(5, 1, ballTime);//send this value to the first ball's correct pattern index
		testPattern.splice(0, 1, ballTime);//and also into the first spot of the testPattern array
		pattern.push(" ");//add a 'newline' character or similar as required by bluetooth ane to the end of the pattern?
		//ard sends two commas at the end of a pattern, so see if removing the above line solves that.
		ballTimer.reset();
		mcRec.nextFrame();
		trace("stop recording and shunt in 1st ball's time delay: "+ pattern);
		myTextBox.text = ("                           playback drill");
		}
	else if(mcRec.currentFrame ==4)//ie if you press 'TEST'
		{
		buttonPress.play();
		mcRec.nextFrame();//(at first empty of bin + save buttons) go to the play+bin+save screen
		//add bin and save buttons to the stage
		mcLoser=new mcLose();
		addChild(mcLoser);
		mcLoser.x=stage.stageWidth-125
		mcLoser.y=35
		
		mcSaver = new mcSave();
		addChild(mcSaver);
		mcSaver.x=stage.stageWidth-50
		mcSaver.y=35
		
		myTextBox.text = ("");

		//make them capable of something when tapped
		mcSaver.addEventListener(TouchEvent.TOUCH_TAP, patternSave);
		mcLoser.addEventListener(TouchEvent.TOUCH_TAP, patternBin);
		
		function patternSave(event:TouchEvent):void//to go back to 'record' and lose the bin+save buttons
				{
				//go to a 'save' screen to enter a name and then save it somehow - still to write
				testTimer.stop();//this stops the testpattern flashing 
				testTimer.reset();
				buttonPress.play();//play sound
				//vibeR();//calls the vibrate function
				removeChild(mcSaver);
				removeChild(mcLoser);
				mcRec.gotoAndStop(1);//is enough, cos the first action in that frame is losing the pattern
				trace ("saved" + pattern);
				myTextBox.text = (pattern + "send");
				mcRobot.gotoAndStop(2);//light up the 'send to robot' button
				}
		function patternBin(event:TouchEvent):void//to go back to 'record' and lose the bin+save buttons
				{
				testTimer.stop();//this stops the testpattern flashing
				testTimer.reset();
				buttonPress.play();//play sound
				//vibeR();//calls the vibrate function
				removeChild(mcSaver);
				removeChild(mcLoser);
				mcRec.gotoAndStop(2);
				trace("last pattern binned " + pattern);
				myTextBox.text = ("alter balls or record again");//all
 ok
				}
		
		//play and loop a movie of the pattern just recorded: ok except tracing ballDelay repeats integers each test
		testTimer.start();//testTimer starts when the 'test' orange arrow is pressed
		trace("just pressed 'test' for this: " + testPattern);//ok as time followed by ball number
		var t:int = 0;//is what?
		testTimer.addEventListener(TimerEvent.TIMER, testbyFlashing); //testbyFlashing runs every .05 of a second until stopped
		
		function testbyFlashing(event:TimerEvent):void
				{
				var ballDelay = testTimer.currentCount*50;//ballDelay is time elapsed from last ball flash
				if (t>(testPattern.length-2))//once you've reached the last delay number and t has got 0.05s beyond it
					{
					t=0;//loop the flashing pattern test.reset t
					}
				else if (ballDelay == testPattern[t])//advances until, when you reach the first ball's delay time at t=0:
					{
						var xxx:int = testPattern[t+1];
						var thisOne:MovieClip = balls[xxx] as MovieClip;
						thisOne.play();
						ballBounce.play();
						testTimer.reset();
						testTimer.start();
						t=t+2;//now run it for the next ball*/
					}
				}
		}
}
//PLAY ROBOT FUNCTION

//BLUETOOTH - ANDROID + ARDUINO = from Android to Arduino!!!!!

// initialize the extension
/*var _ex:Bluetooth = new Bluetooth();
     
// add the required listeners
_ex.addEventListener(BluetoothEvent.BLUETOOTH_STATE , onBluetoothState); // dispatches the state of bluetooth whenever it changes
_ex.addEventListener(BluetoothEvent.COMMUNICATION_STATUS , onCommunicationState); // dispatches the communication state of two devices
_ex.addEventListener(BluetoothEvent.CONNECTION , onConnectionState); // dispatches the connection state of two devices
_ex.addEventListener(BluetoothEvent.DIALOG_STATUS , onDialogState); // dispatches the 'enable' and 'visibility' dialog states
_ex.addEventListener(BluetoothEvent.DISCOVERING_STATUS , onDiscoveringState); // dispatches the device discovering state
_ex.addEventListener(BluetoothEvent.NEW_DISCOVERD , onNewDiscoverd); // dispatches when new devices are discovered
_ex.addEventListener(BluetoothEvent.READ_MESSAGE , onMessageReceived); // dispatches whenever a new String message is received from the second device
_ex.addEventListener(BluetoothEvent.SCAN_MODE , onScanMode); // dispatches to notify you about the scan mode of the device
_ex.addEventListener(BluetoothEvent.SEND_MESSAGE , onSendMessage); // dispatches when a String message is sent to the second device

		
function onBluetoothState(e:BluetoothEvent):void//why this as well as 'make sure bt is on' above?
	{
		myTextBox.text = ("state: " + e.param);
	}
     
function onCommunicationState(e:BluetoothEvent):void
     {
         myTextBox.text = ("communication: " + e.param);
     }
     
function onConnectionState(e:BluetoothEvent):void
	{
     if(e.param == "connected")
	 {
		 myTextBox.text = "tap the target to fire a ball\nadjust aim then tap target to exit"
	 }
	 else
	 {
         myTextBox.text = ("connection: " + e.param);
     }
	}
     
function onDialogState(e:BluetoothEvent):void
     {
         myTextBox.text = ("dialogStatus: " + e.param);
     }
     
function onDiscoveringState(e:BluetoothEvent):void
     {
         myTextBox.text = ("discovering: " + e.param);
     }
	 
function onNewDiscoverd(e:BluetoothEvent):void
	{
		var obj:Object = e.param;
		trace("new Discovered: " + "device = " + obj.device + " mac :" + obj.mac);
	}

//set up bluetooth button:;
mcBluetooth.addEventListener(TouchEvent.TOUCH_TAP, startstopScan);

function startstopScan(event:TouchEvent):void
{
	myTextBox.visible = true;
	mcHelp.gotoAndStop(2);
	ballBounce.play();//sound
	//vibeR();//calls the vibrate function
	if (mcBluetooth.currentFrame ==1)//if click on blue, discover arduino and say hello?
	{
		_ex.startDiscovery();
		// make sure bluetooth is on		
		if (_ex.isEnable)
		{
			_ex.visible(0);//makes android visible to all devices at all times
			_ex.initCommunicationService("00001101-0000-1000-8000-00805F9B34FB");//think this is standard for serial port comms
			mcBluetooth.gotoAndStop(2);//go grey
		}
		else
		{
			_ex.enable();
		}
	}
	else if (mcBluetooth.currentFrame ==2)//if click on blue surround, get a list of paired devices
	{
			//_ex.pairWith("20:13:05:06:31:37");//dont need if already paired using android settings
			var arr:Array = _ex.pairedList;
     		var obj:Object;
     		var i:int;
    		for (i = 0; i < arr.length; i++) 
     		{
         		obj = arr[i];
         		myTextBox.text = ("paired:" + obj.device + " : " + obj.mac);
     		}
			mcBluetooth.gotoAndStop(3);
	}
	else if (mcBluetooth.currentFrame ==3)//if click on grey surround, try to connect to arduino
	{
			myTextBox.text = ("trying to connect");
			mcBluetooth.gotoAndStop(1)
			_ex.connectTo("20:13:05:06:31:37");
	}		
}
     
function onMessageReceived(e:BluetoothEvent):void
     {
         myTextBox.text = ("readMessage: " + e.param);
     }
     
function onScanMode(e:BluetoothEvent):void
     {
         myTextBox.text = ("scanMode: " + e.param);
     }
     
function onSendMessage(e:BluetoothEvent):void
     {
         // when sending a message out to the other device, this listener will be called and on its param will return the same string message you had sent
         // but if for any reason, the message is not sent, the e.param will be "unableToSend"! this way, you will know if the message was not sent successfully
         myTextBox.text = ("sent: " + e.param);
     }
*/	 
//send pattern to robot so it starts playing
mcRobot.addEventListener(TouchEvent.TOUCH_TAP, onTap);
function onTap(event:TouchEvent):void
{
	ballBounce.play();//sound
	//vibeR();//calls the vibrate function
	if (mcRobot.currentFrame == 2)// if 'send' picture is tapped
	{
		trace("just started robot");
		mcRobot.gotoAndPlay(3);//changes to 'pause' picture by playing 3 to 10
		mcReset.gotoAndStop(1);//de-activate the reset button
		//DE-ACTIVATE THE BALLS SO THEY WONT WORK IF PRESSED?
		mcLeft.gotoAndStop(1);//make all of the adjust arrows invisible
		mcRight.gotoAndStop(1);
		mcShort.gotoAndStop(1);
		mcLong.gotoAndStop(1);
		myTextBox.text = ("pause robot");
		trace(pattern.join(","));
//		_ex.sendMessage(pattern.join(",")); //write the pattern array as a string to arduino
	}
	else if(mcRobot.currentFrame == 10)// if 'pause' picture is tapped
	{
		trace("just paused robot");
		trace(pausePattern);
		mcRobot.gotoAndStop(2);//change to 'SEND' picture
		mcReset.gotoAndStop(2);//activate the reset button
		//DE-ACTIVATE THE BALLS SO THEY WONT WORK IF PRESSED?
		mcLong.gotoAndStop(2);//make one of the arrows go visible so you know it's an option to adjust a ball
		myTextBox.text = ("restart : tap ball : or reset");
//		_ex.sendMessage(pausePattern);// send 'PAUSE' pattern to arduino see line 206ish
	}
	else if(mcRobot.currentFrame == 11)// if 'reset' picture is tapped
	{
		trace("just reset");
		buttonPress.play();//play sound
		mcRobot.gotoAndStop(1);// changes to 'grey' picture
		mcLeft.gotoAndStop(1);//make all of the adjust arrows grey
		mcRight.gotoAndStop(1);
		mcShort.gotoAndStop(1);
		mcLong.gotoAndStop(1);
		myTextBox.text = ("move or place ball on table");
	}
}

//stop the android 'back' button from closing the app: works!
NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true)
 
function onKeyDown(event:KeyboardEvent):void
{
    if( event.keyCode == Keyboard.BACK )
    {
        event.preventDefault();
        event.stopImmediatePropagation();
        //handle the button press here, if I want a behaviour of some other kind to happen 
    }
}

//EXIT the app: first add the event handler to the exit button;
mcExit.addEventListener(TouchEvent.TOUCH_TAP, onExittap);
function onExittap(event:TouchEvent):void
{
	 ballBounce.play();//sound
	 //vibeR();//calls the vibrate function
	 if(mcExit.currentFrame == 1)
	 {
		mcExit.gotoAndStop(2);
	 }
	 else
	 {
		mcExit.gotoAndStop(1);
	 }
}
//Long Press. Pressing the object for one second executes a function containing your custom code.

var exitTimer:Timer = new Timer(500);
exitTimer.addEventListener(TimerEvent.TIMER, closeApp);

function closeApp(event:TimerEvent):void
{
//	 	_ex.disable();//turn off bluetooth
//		_ex.dispose();
	 	NativeApplication.nativeApplication.exit();//seems to minimise but not close in android
}

Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;

mcExit.addEventListener(TouchEvent.TOUCH_BEGIN, startTimer);
mcExit.addEventListener(TouchEvent.TOUCH_END, stopTimer);

function startTimer(event:TouchEvent):void
{
	exitTimer.start();
}

function stopTimer(event:TouchEvent):void
{
	exitTimer.stop();

}