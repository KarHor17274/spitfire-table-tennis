package com.spe.bristol.spitfire.Pages;


import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.spe.bristol.spitfire.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class HelpActivityTest {

    @Rule
    public ActivityTestRule<ConnectActivity> mActivityTestRule = new ActivityTestRule<>(ConnectActivity.class);

    @Test
    public void helpActivityTest() {
        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.root),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction navigationMenuItemView = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0)),
                        5),
                        isDisplayed()));
        navigationMenuItemView.perform(click());

        ViewInteraction viewGroup = onView(
                allOf(withId(R.id.action_bar),
                        childAtPosition(
                                allOf(withId(R.id.action_bar_container),
                                        childAtPosition(
                                                withId(R.id.decor_content_parent),
                                                1)),
                                0),
                        isDisplayed()));
        viewGroup.check(matches(isDisplayed()));

        ViewInteraction imageView = onView(
                allOf(withId(R.id.connect_page_screenshot), withContentDescription("Connect"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                1),
                        isDisplayed()));
        imageView.check(matches(isDisplayed()));

        ViewInteraction textView = onView(
                allOf(withId(R.id.help_connect), withText("1a. To start, press the blue button at the bottom of the screen to connect to the robot via Bluetooth.\n\n1b. Once the connection is made, you will be taken to the main screen where you can start to create, record and play your routines.\n\n1c. Alternatively, if you want to simply create and record routines or access other pages, you can do so via the menu button on the top left of the screen. However, be aware you cannot play a routine if you are not connected to the robot."),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                2),
                        isDisplayed()));
        textView.check(matches(withText("1a. To start, press the blue button at the bottom of the screen to connect to the robot via Bluetooth.  1b. Once the connection is made, you will be taken to the main screen where you can start to create, record and play your routines.  1c. Alternatively, if you want to simply create and record routines or access other pages, you can do so via the menu button on the top left of the screen. However, be aware you cannot play a routine if you are not connected to the robot.")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.help_connect), withText("1a. To start, press the blue button at the bottom of the screen to connect to the robot via Bluetooth.\n\n1b. Once the connection is made, you will be taken to the main screen where you can start to create, record and play your routines.\n\n1c. Alternatively, if you want to simply create and record routines or access other pages, you can do so via the menu button on the top left of the screen. However, be aware you cannot play a routine if you are not connected to the robot."),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                2),
                        isDisplayed()));
        textView2.check(matches(withText("1a. To start, press the blue button at the bottom of the screen to connect to the robot via Bluetooth.  1b. Once the connection is made, you will be taken to the main screen where you can start to create, record and play your routines.  1c. Alternatively, if you want to simply create and record routines or access other pages, you can do so via the menu button on the top left of the screen. However, be aware you cannot play a routine if you are not connected to the robot.")));

        ViewInteraction imageView2 = onView(
                allOf(withId(R.id.main_page_screenshot), withContentDescription("Main"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                3),
                        isDisplayed()));
        imageView2.check(matches(isDisplayed()));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.help_main), withText("2a. If you are using the robot for the first time, or if you have moved the robot, it would be a good idea to calibrate it. Select the Calibrate option on the menu to enter calibration mode, and follow the instructions that show up.\n\n2b. Now that the Spitfire is calibrated, you can start making a routine! Drag a ball down from the top and drop it anywhere on the lower half of the table where you want it to be fired. You can change the speed (top wheel) and spin (bottom wheel) of any ball you place by selecting it and changing the spinners at the top. To delete a selected ball, press the bin icon that appears on the top left.\n\n2c. Once you are happy with all of the ball settings, tap the record button on the top right to start recording the routine. You can select the balls in any order including repeating the same ball. Press the stop button at the very top right to stop the recording."),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                4),
                        isDisplayed()));
        textView3.check(matches(withText("2a. If you are using the robot for the first time, or if you have moved the robot, it would be a good idea to calibrate it. Select the Calibrate option on the menu to enter calibration mode, and follow the instructions that show up.  2b. Now that the Spitfire is calibrated, you can start making a routine! Drag a ball down from the top and drop it anywhere on the lower half of the table where you want it to be fired. You can change the speed (top wheel) and spin (bottom wheel) of any ball you place by selecting it and changing the spinners at the top. To delete a selected ball, press the bin icon that appears on the top left.  2c. Once you are happy with all of the ball settings, tap the record button on the top right to start recording the routine. You can select the balls in any order including repeating the same ball. Press the stop button at the very top right to stop the recording.")));

        ViewInteraction textView4 = onView(
                allOf(withId(R.id.help_main), withText("2a. If you are using the robot for the first time, or if you have moved the robot, it would be a good idea to calibrate it. Select the Calibrate option on the menu to enter calibration mode, and follow the instructions that show up.\n\n2b. Now that the Spitfire is calibrated, you can start making a routine! Drag a ball down from the top and drop it anywhere on the lower half of the table where you want it to be fired. You can change the speed (top wheel) and spin (bottom wheel) of any ball you place by selecting it and changing the spinners at the top. To delete a selected ball, press the bin icon that appears on the top left.\n\n2c. Once you are happy with all of the ball settings, tap the record button on the top right to start recording the routine. You can select the balls in any order including repeating the same ball. Press the stop button at the very top right to stop the recording."),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                4),
                        isDisplayed()));
        textView4.check(matches(withText("2a. If you are using the robot for the first time, or if you have moved the robot, it would be a good idea to calibrate it. Select the Calibrate option on the menu to enter calibration mode, and follow the instructions that show up.  2b. Now that the Spitfire is calibrated, you can start making a routine! Drag a ball down from the top and drop it anywhere on the lower half of the table where you want it to be fired. You can change the speed (top wheel) and spin (bottom wheel) of any ball you place by selecting it and changing the spinners at the top. To delete a selected ball, press the bin icon that appears on the top left.  2c. Once you are happy with all of the ball settings, tap the record button on the top right to start recording the routine. You can select the balls in any order including repeating the same ball. Press the stop button at the very top right to stop the recording.")));

        ViewInteraction imageView3 = onView(
                allOf(withId(R.id.main_page_screenshot_2), withContentDescription("Main"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                5),
                        isDisplayed()));
        imageView3.check(matches(isDisplayed()));

        ViewInteraction textView5 = onView(
                allOf(withId(R.id.help_main_2), withText("3a. When the recording is stopped, three buttons will appear. Press the big play button to start playing your routine in real time! Again, please be aware you must be successfully connected to your Spitfire to play.\n\n3b. If you are not happy with your routine and want to change something, press the brown X button to go back to editing your routine or creating a new one entirely.\n\n3c To save your routine, press the green tick button. This should take you to the routines page where you can give it a name for you to go back to later."),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                6),
                        isDisplayed()));
        textView5.check(matches(withText("3a. When the recording is stopped, three buttons will appear. Press the big play button to start playing your routine in real time! Again, please be aware you must be successfully connected to your Spitfire to play.  3b. If you are not happy with your routine and want to change something, press the brown X button to go back to editing your routine or creating a new one entirely.  3c To save your routine, press the green tick button. This should take you to the routines page where you can give it a name for you to go back to later.")));

        ViewInteraction textView6 = onView(
                allOf(withId(R.id.help_main_2), withText("3a. When the recording is stopped, three buttons will appear. Press the big play button to start playing your routine in real time! Again, please be aware you must be successfully connected to your Spitfire to play.\n\n3b. If you are not happy with your routine and want to change something, press the brown X button to go back to editing your routine or creating a new one entirely.\n\n3c To save your routine, press the green tick button. This should take you to the routines page where you can give it a name for you to go back to later."),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                                        0),
                                6),
                        isDisplayed()));
        textView6.check(matches(withText("3a. When the recording is stopped, three buttons will appear. Press the big play button to start playing your routine in real time! Again, please be aware you must be successfully connected to your Spitfire to play.  3b. If you are not happy with your routine and want to change something, press the brown X button to go back to editing your routine or creating a new one entirely.  3c To save your routine, press the green tick button. This should take you to the routines page where you can give it a name for you to go back to later.")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
