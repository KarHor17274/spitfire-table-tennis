package com.spe.bristol.spitfire.Pages;


import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.spe.bristol.spitfire.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityRoutineTest2 {

    @Rule
    public ActivityTestRule<ConnectActivity> mActivityTestRule = new ActivityTestRule<>(ConnectActivity.class);

    @Test
    public void mainActivityTest2() {
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.connect_button), withText("Connect to Spitfire"),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.root),
                                                6)),
                                0),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction navigationMenuItemView = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0)),
                        3),
                        isDisplayed()));
        navigationMenuItemView.perform(click());

        ViewInteraction imageView = onView(
                allOf(withId(R.id.ball),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                15),
                        isDisplayed()));
        imageView.check(matches(isDisplayed()));

        ViewInteraction imageView2 = onView(
                allOf(withId(R.id.ballSelectedCircle),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                9),
                        isDisplayed()));
        imageView2.check(matches(isDisplayed()));

        ViewInteraction imageView3 = onView(
                allOf(withId(R.id.circleBehindSpinner),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                3),
                        isDisplayed()));
        imageView3.check(matches(isDisplayed()));

        ViewInteraction view = onView(
                allOf(withId(R.id.progClockwiseTop),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                19),
                        isDisplayed()));
        view.check(matches(isDisplayed()));

        ViewInteraction view2 = onView(
                allOf(withId(R.id.progClockwiseTop),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                19),
                        isDisplayed()));
        view2.check(matches(isDisplayed()));

        ViewInteraction imageView4 = onView(
                allOf(withId(R.id.spinnerController),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                13),
                        isDisplayed()));
        imageView4.check(matches(isDisplayed()));

        ViewInteraction view3 = onView(
                allOf(withId(R.id.progAnticlockwiseBtm),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                17),
                        isDisplayed()));
        view3.check(matches(isDisplayed()));

        ViewInteraction view4 = onView(
                allOf(withId(R.id.progAnticlockwiseBtm),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                17),
                        isDisplayed()));
        view4.check(matches(isDisplayed()));

        ViewInteraction imageView5 = onView(
                allOf(withId(R.id.spinnerController),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                12),
                        isDisplayed()));
        imageView5.check(matches(isDisplayed()));

        ViewInteraction view5 = onView(
                allOf(withId(R.id.TableView),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                0),
                        isDisplayed()));
        view5.check(matches(isDisplayed()));

        ViewInteraction imageView6 = onView(
                allOf(withId(R.id.recordButton),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                21),
                        isDisplayed()));
        imageView6.check(matches(isDisplayed()));

        ViewInteraction imageView7 = onView(
                allOf(withId(R.id.ring_rec),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                7),
                        isDisplayed()));
        imageView7.check(matches(isDisplayed()));

        ViewInteraction imageView8 = onView(
                allOf(withId(R.id.stopButton),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                22),
                        isDisplayed()));
        imageView8.check(matches(isDisplayed()));

        ViewInteraction imageView9 = onView(
                allOf(withId(R.id.ring_stop),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                6),
                        isDisplayed()));
        imageView9.check(matches(isDisplayed()));

        ViewInteraction imageView10 = onView(
                allOf(withId(R.id.binButton),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                16),
                        isDisplayed()));
        imageView10.check(matches(isDisplayed()));

        ViewInteraction imageView11 = onView(
                allOf(withId(R.id.ring_bin),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                8),
                        isDisplayed()));
        imageView11.check(matches(isDisplayed()));

        ViewInteraction imageView12 = onView(
                allOf(withId(R.id.ball),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                14),
                        isDisplayed()));
        imageView12.check(matches(isDisplayed()));

        ViewInteraction imageView13 = onView(
                allOf(withId(R.id.balls_icon),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                4),
                        isDisplayed()));
        imageView13.check(matches(isDisplayed()));

        ViewInteraction view6 = onView(
                allOf(withId(R.id.VerticalLineView),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                24),
                        isDisplayed()));
        view6.check(matches(isDisplayed()));

        ViewInteraction imageView14 = onView(
                allOf(withId(R.id.HorizontalLineView),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                23),
                        isDisplayed()));
        imageView14.check(matches(isDisplayed()));

        ViewInteraction imageView15 = onView(
                allOf(withId(R.id.TableActiveAreaView),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                1),
                        isDisplayed()));
        imageView15.check(matches(isDisplayed()));

        ViewInteraction viewGroup = onView(
                allOf(withId(R.id.toolbar),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                0)),
                                5),
                        isDisplayed()));
        viewGroup.check(matches(isDisplayed()));

        ViewInteraction imageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.root),
                                                5)),
                                0),
                        isDisplayed()));
        imageButton.check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
