package com.spe.bristol.spitfire.SpitfireCommunication.Bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class BluetoothService {

    private static final UUID MY_UUID_INSECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String TAG = "BluetoothService";
    private final BluetoothAdapter mBluetoothAdapter;
    private static ConnectThread connectThread;
    private static ConnectedThread connectedThread;

    public BluetoothService(){
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public void initConnection(BluetoothDevice targetDevice){
        Log.i(TAG,"started initialisation of ConnectThread in Bluetooth Service");
        connectThread = new ConnectThread(targetDevice);
        connectThread.start();
    }

    public void terminateConnection(){
        connectThread.cancel();
        connectedThread.cancel();
    }

    private void manageMyConnectedSocket(BluetoothSocket socket){
        connectedThread = new ConnectedThread(socket);
        Log.i(TAG,"manageMyConnectedSocket initialised");
        connectedThread.start();
        Log.i(TAG,"manageMyConnectedSocket start");
    }

//    public boolean btIsConnected(){
//        Log.i(TAG,"connectThread is " + connectThread );
//        if (connectThread == null) {
//            return false;
//        }
//        else if (connectThread.connectSocket == null) {
//            Log.i(TAG,"connectThread.connectSocket is ");
//            return false;
//        }
//        else {
//            Log.i(TAG,"connectThread.connectSocket.isConnected() is " + connectThread.connectSocket.isConnected());
//            return connectThread.connectSocket.isConnected();
//        }
//    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
        // Synchronize a copy of the ConnectedThread
        Log.i(TAG, "public write function : Write Called in .");
        //perform the write
        // TODO: make sure this doesn't write when null
        connectedThread.write(out);
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket connectSocket;
        private final BluetoothDevice mmDevice;

        private ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to connectSocket
            // because connectSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;
            Log.i(TAG,"check ConnectThread 1");

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID_INSECURE);
                Log.i(TAG,"check ConnectThread 2");
            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            connectSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                Log.i(TAG,"check ConnectThread 3");
                connectSocket.connect();
                if(connectSocket.isConnected()){
                    Log.i(TAG,"active connection with remote device");
                }
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    connectSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            Log.i(TAG,"manageMyConnectedSocket's call in ConnectThread");
            manageMyConnectedSocket(connectSocket);
        }

        // Closes the client socket and causes the thread to finish.
        private void cancel() {
            try {
                connectSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }


    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream

        private ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;

            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // Read from the InputStream.
                    numBytes = mmInStream.read(mmBuffer);
                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    break;
                }
            }
        }

        // Call this to send data to the remote device.
        private void write(byte[] bytes) {
            try {
                Log.i(TAG,"write() function in ConnectedThread");
                mmOutStream.write(bytes);
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);
            }
        }

        // Call this method from the main activity to shut down the connection.
        private void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }

}


//private class AcceptThread extends Thread {
//
//
//    private final BluetoothServerSocket mmServerSocket;
//
//    public AcceptThread() {
//        // Use a temporary object that is later assigned to mmServerSocket
//        // because mmServerSocket is final.
//        BluetoothServerSocket tmp = null;
//        try {
//            // MY_UUID is the app's UUID string, also used by the client code.
//            tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord("SpitfireTableTennis", MY_UUID_INSECURE);
//        } catch (IOException e) {
//            Log.e(TAG, "Socket's listen() method failed", e);
//        }
//        mmServerSocket = tmp;
//    }
//
//    public void run() {
//        BluetoothSocket acceptSocket = null;
//        // Keep listening until exception occurs or a socket is returned.
//        while (true) {
//            try {
//                acceptSocket = mmServerSocket.accept();
//            } catch (IOException e) {
//                Log.e(TAG, "Socket's accept() method failed", e);
//                break;
//            }
//
//            if (acceptSocket != null) {
//                // A connection was accepted. Perform work associated with
//                // the connection in a separate thread.
//                manageMyConnectedSocket(acceptSocket);
//                try {
//                    mmServerSocket.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                break;
//            }
//        }
//    }
//
//    // Closes the accept socket and causes the thread to finish.
//    public void cancel() {
//        try {
//            mmServerSocket.close();
//        } catch (IOException e) {
//            Log.e(TAG, "Could not close the accept socket", e);
//        }
//    }
//}