package com.spe.bristol.spitfire.Pages;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.spe.bristol.spitfire.R;

import java.util.Objects;

public class HelpActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private android.support.v7.widget.Toolbar toolbar;
    public static final String MY_TAG = "helpActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        setupDrawerLayout();
        makeActionBarToolbar();

        setContentView(mDrawerLayout);
    }

    private void setupDrawerLayout() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        Log.i(MY_TAG, ""+mDrawerLayout);
        mDrawerLayout.setBackgroundColor(Color.argb(255,50,50,50));

        //Nav View (side menu)
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(false);
                    // close drawer when item is tapped
                    mDrawerLayout.closeDrawers();

                    // Add code here to update the UI based on the item selected
                    // For example, swap UI fragments here

                    int id = menuItem.getItemId();
                    switch (id) {
                        case R.id.connect_page:
                            Intent r = new Intent(HelpActivity.this,ConnectActivity.class);
                            startActivity(r);
                            break;
                        case R.id.main_page:
                            Intent i = new Intent(HelpActivity.this, MainActivity.class);
                            startActivity(i);
                            break;
                        case R.id.about_page:
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setData(Uri.parse("http://www.spitfiretabletennis.com"));
                            startActivity(intent);
                            break;
                        case R.id.routines_page:
                            Intent f = new Intent(HelpActivity.this, MyRoutineActivity.class);
                            startActivity(f);
                            break;
                        case R.id.setting_page:
                            Intent s = new Intent(HelpActivity.this, SettingsActivity.class);
                            startActivity(s);
                            break;
                        case R.id.help_page:
                            break;
                    }
                    return true;
                });

        ///Setup DrawerLayout
        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Respond when the drawer is opened
                    }
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Respond when the drawer is closed
                    }
                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void makeActionBarToolbar() {
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        Log.i(MY_TAG,"toolBar value is " + toolbar);


        //Action Bar
        ActionBar actionbar = getSupportActionBar();
        try{
            Objects.requireNonNull(actionbar).setDisplayHomeAsUpEnabled(true);
        }catch (NullPointerException e){
            Log.e(MY_TAG,e.getMessage());
        }
        Objects.requireNonNull(actionbar).setHomeAsUpIndicator(R.drawable.ic_menu_main);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setDisplayShowHomeEnabled(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
