package com.spe.bristol.spitfire.TouchHelpers;

import android.content.ClipData;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.spe.bristol.spitfire.HelperFunctions;
import com.spe.bristol.spitfire.Pages.MainActivity;
import com.spe.bristol.spitfire.R;


public class MyTouchListener implements View.OnTouchListener {
    private static final String TAG = "touch:";
    private TouchInterface touchInterface = null;


    private boolean draggable;
    public MyTouchListener(boolean draggable, TouchInterface touchInterface) {
        this.draggable = draggable;
        this.touchInterface = touchInterface;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        //Only trigger for down click
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            if (draggable) {
                boolean allowDrag = touchInterface.allowDrag(view);

                if (allowDrag) {
                    // Get view object tag value.
                    String tag = (String) view.getTag();
                    // Create clip data.
                    ClipData clipData = ClipData.newPlainText("", tag);
                    // Create drag shadow builder object.
                    View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(view);
                /*
                 view object's startDrag method to start the drag action.
                 clipData : to be dragged data.
                 dragShadowBuilder : the shadow of the dragged view.

                */
                    HelperFunctions.assertion(view != null, "View cannot be null");
                    view.setVisibility(View.INVISIBLE);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Log.i(TAG, "API 23 and above");
                        view.startDragAndDrop(clipData, dragShadowBuilder, view, 0);
                    } else {
                        Log.i(TAG, "API 22 and below");
                        view.startDrag(clipData, dragShadowBuilder, view, 0);
                    }

                }
                else {
                    Log.i(TAG, "Draggable but not allowing drag");
                    return true;
                }

                return true;
            }
            else {
                Log.i(TAG, "onTouch --> not draggable");
                Log.i(TAG, "parent = " + view.getParent());

                touchInterface.touchesBegan(view);
                return true;
            }
        }
        else if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
            if (draggable) {
                HelperFunctions.assertion(view != null, "View cannot be null");
                view.setVisibility(View.VISIBLE);
                touchInterface.touchesBegan(view);

                // Show the view object because we are just touching it not dragging.
                return true;
            }
            else {
                Log.i(TAG, "Ignoring  MotionEvent." + MotionEvent.actionToString(motionEvent.getAction()) + "for non ball");
                touchInterface.touchesEnded(view,motionEvent.getRawX(),motionEvent.getRawY());
                return true;
            }
        }
        else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
            Log.i(TAG, "MotionEvent." + MotionEvent.actionToString(motionEvent.getAction()) + "for spinenr handler!?");
            touchInterface.touchesMoved(view,motionEvent.getRawX(),motionEvent.getRawY());
            return true;
        }
        else {
            //MotionEvent.ACTION_UP or MOVE) {
            Log.i(TAG, "Ignoring  MotionEvent." + MotionEvent.actionToString(motionEvent.getAction()));
            HelperFunctions.assertion(view != null, "View cannot be null");
            view.setVisibility(View.VISIBLE);
            return true;
        }
    }



}