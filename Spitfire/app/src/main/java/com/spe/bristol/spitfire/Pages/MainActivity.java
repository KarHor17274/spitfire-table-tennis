package com.spe.bristol.spitfire.Pages;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.TextView;

import com.spe.bristol.spitfire.RoutineHelper.AddRoutineActivity;
import com.spe.bristol.spitfire.Ball;
import com.spe.bristol.spitfire.CircleProgressBar;
import com.spe.bristol.spitfire.DoubleSpinner;
import com.spe.bristol.spitfire.HelperFunctions;
import com.spe.bristol.spitfire.PointXY;
import com.spe.bristol.spitfire.R;
import com.spe.bristol.spitfire.RoutineHelper.Routine;
import com.spe.bristol.spitfire.SpitfireCommunication.PhysicsCalculator;
import com.spe.bristol.spitfire.SpitfireCommunication.SpitfireController;
import com.spe.bristol.spitfire.State;
import com.spe.bristol.spitfire.TouchHelpers.MyDragListener;
import com.spe.bristol.spitfire.TouchHelpers.MyTouchListener;
import com.spe.bristol.spitfire.TouchHelpers.TouchInterface;
import com.spe.bristol.spitfire.ZPositions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity implements TouchInterface {
    public static final String MY_TAG = "MainActivity:";
    public static final String MY_EMPTY_TAG = "empty";
    public static final String TAG_FIRE = "FireMode";
    public static final String ROUT_TAG = "RoutineTesting:";

    private ImageView stopButton, stopRing, recordButton, recordRing, saveButton, binButton, binRing, sendToRobotButton, confirmButton, returnButton, pauseButton;
    private ImageView calibrationUpArrow, calibrationLeftArrow, calibrationDownArrow, calibrationRightArrow, calibrationCircle, calibrationConfirm;
    private android.support.v7.widget.Toolbar toolbar;


    private DoubleSpinner topSpinner, btmSpinner;

    private ArrayList<Ball> balls = new ArrayList<>();
    private View table = null;
    private View tableActiveArea = null;

    private Context context = null;
    private ConstraintLayout constraintLayout = null;
    private DrawerLayout mDrawerLayout;

    public Integer K_WINDOW_HEIGHT;
    public Integer K_WINDOW_WIDTH;
    private MyTimer timer = new MyTimer(true);

    private State currentState = State.Creating;
    private State prevState = null;
    private SpitfireController spitfireController;

    private void CHANGE_STATE(State nextState) {
        if (currentState == nextState) {
            return;
        }
        if(nextState == State.Calibrating) {
            calibration.set(0,0);
            calibration.set(1,0);
        }
        prevState = currentState;
        currentState = nextState;
        Log.i(MY_TAG, "-------------------- CHANGE_STATE [" + prevState.toString() + "] -> [" + currentState.toString() + "]--------------------");
    }

    private float getTableMargin() {
        return getResources().getDimension(R.dimen.table_margin);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(MY_TAG, "onCreateCalled");
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();

        if (context == null) {
            context = getApplicationContext();
        }
        if (constraintLayout == null) {
            constraintLayout = findViewById(R.id.root);
        }
        setScreenConstants();
        setupDrawerLayout();

        //Instantiate class to pass routines on to save in MyRoutineActivity
        spitfireController = (SpitfireController) getApplication();


        boolean testToolbar = false;
        makeActionBarToolbar();

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException err) {
            //TODO: Handle this
        }

        ////////////////////////////////////////////////////////////Supported formats are: #RRGGBB #AARRGGBB
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.primaryTable_COL)));

        if (testToolbar) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.toolbarColor)));
            toolbar.setBackgroundColor(Color.YELLOW);
//            toolbar.setY(toolbar.getMeasuredHeight() + 900);
            Log.i(MY_TAG, "toolbar XY= (" + toolbar.getX() + "," + toolbar.getY() + ")");
            toolbar.setX(0);
            toolbar.setY(0);
            Log.i(MY_TAG, "toolbar XY= (" + toolbar.getX() + "," + toolbar.getY() + ")");
        }


        //Balls TODO: Make this own class
        makeNewBall();
        makeBallsIcon();

        makeTable();

        //Toolbar Buttons
        makeRecordButton();
        makeButtonRing(recordButton);
        makeStopButton();
        makeButtonRing(stopButton);
        //makeSaveButton();
        //makeButtonRing(saveButton);
        makeBinButton();
        makeButtonRing(binButton);

        //Spitfire Buttons
        makeSendToRobotButton();
        makeReturnButton();
        makeConfirmButton();
        makePauseButton();

        makeCalibrationArrowsAndButtons();

        makeDoubleSpinner(true);
        makeDoubleSpinner(false);

        constraintLayout.post(new Runnable() {
            @Override
            public void run() {
                addCircleBehindSpinner(topSpinner);
                addCircleBehindSpinner(btmSpinner);

                constraintLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        addSpinnerController(topSpinner);
                        addSpinnerController(btmSpinner);
                        hideSpinners();
                    }
                });
            }
        });

        //Loads parent node and all children
        setContentView(mDrawerLayout);


        //Set constants after loading widths/heights etc
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {

                tableActiveArea.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(MY_TAG,"setting spitfire screen constants");
                        setSpitfireControllerScreenConstants();
                    }
                });

            }
        });

        currentRoutine = (Routine) intent.getSerializableExtra("routine");
        if(currentRoutine != null) {
            displayRoutine(currentRoutine);
        }
    }

    private void setSpitfireControllerScreenConstants() {
        Log.i(TAG_FIRE,"SCREEN CONSTANTS W: " + K_WINDOW_WIDTH + " H: " + K_WINDOW_HEIGHT);

        Integer k_ACTIVE_TABLE_AREA_Y = (int) tableActiveArea.getY();
        Integer k_ACTIVE_TABLE_AREA_X = (int) tableActiveArea.getX();
        Integer k_ACTIVE_TABLE_AREA_WIDTH = (int) tableActiveArea.getWidth();
        Integer k_ACTIVE_TABLE_AREA_HEIGHT = (int) tableActiveArea.getWidth();

        Integer k_SPITFIRE_X = (int) tableActiveArea.getX() + tableActiveArea.getWidth()/2;
        Integer k_SPITFIRE_Y = (int) tableActiveArea.getY() - 1; //slightly higher to avoid /0


        spitfireController.SET_SCREEN_CONSTS(k_ACTIVE_TABLE_AREA_HEIGHT, k_ACTIVE_TABLE_AREA_WIDTH, k_ACTIVE_TABLE_AREA_X, k_ACTIVE_TABLE_AREA_Y, k_SPITFIRE_X, k_SPITFIRE_Y);
    }

    private void setPhysicsCalcScreenConstants(PhysicsCalculator physicsCalculator) {
        Integer k_ACTIVE_TABLE_AREA_Y = (int) tableActiveArea.getY();
        Integer k_ACTIVE_TABLE_AREA_X = (int) tableActiveArea.getX();
        Integer k_ACTIVE_TABLE_AREA_WIDTH = (int) tableActiveArea.getWidth();
        Integer k_ACTIVE_TABLE_AREA_HEIGHT = (int) tableActiveArea.getWidth();

        Integer k_SPITFIRE_X = (int) tableActiveArea.getX() + tableActiveArea.getWidth()/2;
        Integer k_SPITFIRE_Y = (int) tableActiveArea.getY() - 1; //slightly higher to avoid /0

        physicsCalculator.SET_SCREEN_CONSTS(k_ACTIVE_TABLE_AREA_HEIGHT, k_ACTIVE_TABLE_AREA_WIDTH, k_ACTIVE_TABLE_AREA_X, k_ACTIVE_TABLE_AREA_Y, k_SPITFIRE_X, k_SPITFIRE_Y);
    }



    private void setScreenConstants() {
        ///Setup display constants
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        K_WINDOW_HEIGHT = displayMetrics.heightPixels;
        K_WINDOW_WIDTH = displayMetrics.widthPixels;
        Log.i(MY_TAG, "DISPLAY SIZE (" + K_WINDOW_WIDTH + ", " + K_WINDOW_HEIGHT + ")");

    }

    private void setupDrawerLayout() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerLayout.setBackgroundColor(Color.argb(255, 50, 50, 50));

        //Nav View (side menu)
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        int id = menuItem.getItemId();
                        switch (id) {
                            case R.id.connect_page:
                                goToPageOfClass(ConnectActivity.class);
                                break;
                            case R.id.main_page:
                                if(currentState==State.Calibrating){
                                    CHANGE_STATE(State.Creating);

                                    hideCalibrationAll();
                                    showAllBalls();
                                    showRecordButton();
                                    showStopButton();
                                    testBallImageView.setVisibility(View.INVISIBLE);
                                    testBallImageView=null;
                                }
                                break;
                            case R.id.calibrate:
                                if (currentState != State.Creating) {
                                    CharSequence text = "Cannot currently calibrate.";
                                    showToastShort(text);
                                    break;
                                }
                                else {
                                    CHANGE_STATE(State.Calibrating);

                                    hideAllBalls();
                                    removeCurrentSelectedCircle();
                                    if (binButton.getVisibility() == View.VISIBLE) {
                                        hideBinIcon();
                                    }
                                    hideSpinners();
                                    hideRecordButton();
                                    hideStopButton();
                                    showCalibrationAll();
                                    showCalibrationCircle();

                                    if (testBallImageView == null) {
                                        testBallImageView = makeTestBallImageView(new PointXY(calibrationCircle.getX() + calibrationCircle.getMeasuredWidth() * 0.5f, calibrationCircle.getY() + calibrationCircle.getMeasuredHeight() * 0.5f));
                                    }

                                    CharSequence text = "Tap the target to fire a ball. \nTap arrows until ball hits target.";
                                    showToastLong(text);
                                    break;
                                }
                            case R.id.about_page:
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("http://www.spitfiretabletennis.com"));
                                startActivity(intent);
                                break;
                            case R.id.routines_page:
                                goToPageOfClass(MyRoutineActivity.class);
                                break;
                            case R.id.setting_page:
                                goToPageOfClass(SettingsActivity.class);
                                break;
                            case R.id.help_page:
                                goToPageOfClass(HelpActivity.class);
                        }
                        return true;
                    }
                });


        ///Setup DrawerLayout
        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }






    @Override
    protected void onStart() {
        super.onStart();
        Log.i(MY_TAG, "onStartCalled");
    }

    //Activity is priority again (user navigated return_button)
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(MY_TAG, "onResumeCalled");
    }

    //User is leaving app (e.g. save routines now => ContentProvider class)
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(MY_TAG, "onPauseCalled");
        spitfireController.stop();

    }

    //Activity is no longer visible
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(MY_TAG, "onStopCalled");
        spitfireController.stop();

    }

    //App killed/activity killed
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(MY_TAG, "onDestroyCalled");
        spitfireController.stop();

    }

    //Go return_button to connect activity
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void goToPageOfClass(Class cls) {
        Intent r = new Intent(MainActivity.this, cls);
        //spitfireController.stop();
        startActivity(r);
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //
    ///////////////////  BALL LOGIC  ///////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private boolean checkIfAllBallPositionsAreValid(Ball newBall, float newX, float newY) {
        boolean newBallOnExistingBall = false;

        for (Ball ball : balls) {
            ImageView ballImgV = ball.getBallImageView();
            //skip if same ball
            if (ball.getBallImageTag() == newBall.getBallImageTag()) {
                continue;
            }
            //check if valid position
            if (checkIfBothBallPositionsAreValid(newX, newY, ballImgV.getX(), ballImgV.getY()) == false) {
                //Not valid
                newBallOnExistingBall = true;
                break;
            }
        }

        return newBallOnExistingBall;
    }

    private boolean checkIfBothBallPositionsAreValid(float x1, float y1, float x2, float y2) {
        //Amount disallowed each side - 0 means only balls exactly on top of one another are disallowed
        float xBoundAllowed = 2; //could change to half width of ball
        float yBoundAllowed = 2;
        boolean treatFloatsAsInts = false;

        if (treatFloatsAsInts) {
//            Log.i(MY_TAG, "Diff btwn balls: (" + Math.abs((int)x1-(int)x2) + ","  + Math.abs((int)y1-(int)y2) + ")");

            if (Math.abs((int) x1 - (int) x2) < (int) xBoundAllowed) {
                if (Math.abs((int) y1 - (int) y2) < (int) yBoundAllowed) {
                    return false;
                }
            }
        } else {
//            Log.i(MY_TAG, "Diff btwn balls: (" + Math.abs(x1-x2) + ","  + Math.abs(y1-y2) + ")");

            if (Math.abs(x1 - x2) < xBoundAllowed) {
                if (Math.abs(y1 - y2) < yBoundAllowed) {
                    return false;
                }
            }
        }
        //Valid if not overlapping
        return true;
    }


    private Ball getBallFromTag(Object tag) throws NullPointerException {
        for (Ball ball : balls) {
            if (ball.getBallImageTag() == tag) {
//                Log.i(MY_TAG,"Found ball by tag: " + ball.getBallImageTag());
                return ball;
            }
        }

        HelperFunctions.assertion(false, "No Ball Found by tag: " + tag);
        throw new NullPointerException();
    }

    private void removeBallFromBallArray(Object tag) {
        Ball rmBall = null;
        for (Ball ball : balls) {
            if (ball.getBallImageTag() == tag) {
//                Log.i(MY_TAG,"Found ball by tag: " + ball.getBallImageTag());
                rmBall = ball;
            }
        }
        if (rmBall == null) {
            return;
        }
        balls.remove(rmBall);
        Log.i(MY_TAG, "Removed ball from array by tag: " + rmBall.getBallImageTag());
    }

    private void deleteBall(Object tag) {
        try {
            Ball ball = getBallFromTag(tag);
            ball.setBallInactive();
            totalBallsActive--;
            removeBallFromBallArray(tag);
        } catch (NullPointerException err) {
            Log.i(MY_TAG, "No ball with tag: " + tag);
        }
    }

    ///Removes ball assets and calls other deleteBall
    private void deleteBall(Ball rmBall) {
        constraintLayout.removeView(rmBall.getBallImageView());
        Log.i(MY_TAG, "Removed ball from constraintLayout by tag: " + rmBall.getBallImageTag());
        deleteBall(rmBall.getBallImageTag());

        try {
            currentRoutine.removeBall(rmBall);
        }
        catch (NullPointerException err) {

        }

        try {
            currentRoutine.removeBall(rmBall.getId());
        }
        catch (NullPointerException err) {

        }
    }

    //Ball has been dropped
    public void ballDropWasValid(Ball ballDropped) {
        Log.i(MY_TAG, "ballDropped \n" + " ");

        //Ball dragged was the new ball (top left) so need to make another one
        if (ballDropped.isActive() == false) {
            ballDropped.setBallActive();
            totalBallsActive++;
            makeNewBall();
        } else { //was already active, so just moving ball

        }

        //Set ball dropped to selected ball
        deselectAllBalls();
        selectBall(ballDropped);
    }








    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //
    ///////////////////  MAKING ASSETS ///////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void makeActionBarToolbar() {
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbar.setZ(ZPositions.Toolbar.ordinal());

        toolbar.setVisibility(View.INVISIBLE);
        toolbar.setY(0);
        toolbar.setX(0);

//        float YScalar = K_WINDOW_HEIGHT * 0.5f / getResources().getDimension(R.dimen.toolbarHeight);
//        toolbar.setScaleY(YScalar);
        setSupportActionBar(toolbar);

        //Action Bar
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_main);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setDisplayShowHomeEnabled(false);

        toolbar.setVisibility(View.VISIBLE);
    }

    private void makeButtonRing(ImageView buttonToRing) {
        ImageView buttonRing = new ImageView(constraintLayout.getContext());
        buttonRing.setImageResource(R.drawable.ring_button);

        try {
            buttonRing.setId(getRingButtonIDFromButton(buttonToRing));
            buttonRing.setTag(getRingButtonTagFromButton(buttonToRing));
        } catch (NullPointerException err) {
            Log.i(MY_TAG, "Can't find suitable RING ID or TAG from button");
            //TODO: remove before release
            HelperFunctions.assertion(false, "Can't find suitable RING ID or TAG from button");
        }

        if (buttonToRing.getId() == R.id.ball) { //if ball, set ball ring below TODO: Old code, only need else
            buttonRing.setZ(ZPositions.BehindBall.ordinal());
        }
        else { //If not ball, set ring above and add touch listener
            if (buttonRing.getId() == R.id.ring_bin) {
                binRing = buttonRing;
            } //keep reference to binRing so can hide it later
            if (buttonRing.getId() == R.id.ring_rec) {
                recordRing = buttonRing;
            }

            if (buttonRing.getId() == R.id.ring_stop) {
                stopRing = buttonRing;
            }
            buttonRing.setZ(ZPositions.ButtonRing.ordinal());
            buttonRing.setOnTouchListener(new MyTouchListener(false, this));
        }

        //Hide until position
        buttonRing.setVisibility(View.INVISIBLE);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                //Log.i(MY_TAG,"toolbar.getMeasuredHeight() " + toolbar.getMeasuredHeight());

                int K_buttonRingSize = (int) (toolbar.getMeasuredHeight() * 0.8f);
                buttonRing.setLayoutParams(new ConstraintLayout.LayoutParams(K_buttonRingSize, K_buttonRingSize));

                float yPos = buttonToRing.getY() + buttonToRing.getMeasuredHeight() / 2;
                float xPos = buttonToRing.getX() + buttonToRing.getMeasuredWidth() / 2;

                buttonRing.setY(yPos);
                buttonRing.setX(xPos);
                constraintLayout.addView(buttonRing);
            }
        });

        buttonRing.post(new Runnable() {
            @Override
            public void run() {
                buttonRing.setX(buttonRing.getX() - buttonRing.getMeasuredWidth() / 2);
                buttonRing.setY(buttonRing.getY() - buttonRing.getMeasuredHeight() / 2);

                if (buttonRing.getId() != R.id.ring_bin) {
                    buttonRing.setVisibility(View.VISIBLE);
                } //show if it's not binRing
            }
        });
    }

    private void makeBallsIcon() {
        ImageView ballsIcon = new ImageView(constraintLayout.getContext());
        ballsIcon.setImageResource(R.drawable.balls_icon);
        //set tag to UNIQUE tag
        ballsIcon.setTag("balls_icon");
        //Set Z index to above tables
        ballsIcon.setZ(ZPositions.BehindBall.ordinal());
        //non unique ID
        ballsIcon.setId(R.id.balls_icon);

        ballsIcon.setLayoutParams(new ConstraintLayout.LayoutParams(K_ballSize, K_ballIconHeight));
        //Hide until positioned correctly
        ballsIcon.setVisibility(View.INVISIBLE);
        constraintLayout.addView(ballsIcon);

        //Position ball
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                float xPos = toolbar.getX() + toolbar.getMeasuredWidth() * BallXWidthScalar - K_ballSize / 2f;
                float yPos = toolbar.getY() + toolbar.getMeasuredHeight() / 2f - ballsIcon.getMeasuredHeight() / 2f;
                ///Set position of newBall on toolbar
                ballsIcon.setX(xPos);
                ballsIcon.setY(yPos);

                //Positioned correctly so show ball
                ballsIcon.setVisibility(View.VISIBLE);
            }
        });
    }


    private Integer K_ballSize = 60;
    private Integer K_ballHitBoxSize = 120;
    private Integer K_ballIconHeight = K_ballSize * 3;
    private Integer totalBallsActive = 0;
    private Integer totalBallsMadeEver = 0;

    private void makeNewBall() {
        totalBallsMadeEver++;

        ImageView ballHitBoxImgView = new ImageView(constraintLayout.getContext());
        ballHitBoxImgView.setImageResource(R.drawable.ball);
        ballHitBoxImgView.setTag("ball_" + totalBallsMadeEver);
        ballHitBoxImgView.setZ(ZPositions.AboveBall.ordinal());
        ballHitBoxImgView.setId(R.id.ball);
        ballHitBoxImgView.setLayoutParams(new ConstraintLayout.LayoutParams(K_ballHitBoxSize, K_ballHitBoxSize));
        ballHitBoxImgView.setVisibility(View.INVISIBLE);
        ballHitBoxImgView.setAlpha(0f);

        ImageView ballImgView = new ImageView(constraintLayout.getContext());
        ballImgView.setImageResource(R.drawable.ball);
        //set tag to UNIQUE tag
        //ballImgView.setTag("ball_" + totalBallsMadeEver);
        //Set Z index to above tables
        ballImgView.setZ(ZPositions.Ball.ordinal());
        //non unique ID
        ballImgView.setId(R.id.ball);

        ballImgView.setLayoutParams(new ConstraintLayout.LayoutParams(K_ballSize, K_ballSize));
        //Hide until positioned correctly
        ballImgView.setVisibility(View.INVISIBLE);
        //


        //create Ball (backend) representation
        Ball newBall = new Ball(ballImgView,ballHitBoxImgView, totalBallsMadeEver);
        newBall.setBallInactive();
        //Add ball to array
        balls.add(newBall);

        //Position ball
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                float xPos = toolbar.getX() + toolbar.getMeasuredWidth() * BallXWidthScalar - K_ballSize / 2f;

                //top of 3 balls (y pos)
                float yPos = toolbar.getY() + toolbar.getMeasuredHeight() / 2f - K_ballIconHeight / 2f + K_ballSize / 2f;

                //add one margins
                yPos += getResources().getDimension(R.dimen.ballIconOverlap);
//                //add on half height of one ball in the 3 balls icon
                yPos += getResources().getDimension(R.dimen.ballIconBallSize) / 2f;
//                //centre ball
//                yPos -= K_ballSize/2f;


                ///Set position of newBall on toolbar
                newBall.setPosition(xPos, yPos);
                //Positioned correctly so show ball
                ballImgView.setVisibility(View.VISIBLE);
                ballHitBoxImgView.setVisibility(View.VISIBLE);

//
//                ///First time  ring around ball
//                if (totalBallsMadeEver == 1) { //only does first time
//                    newBall.getBallImageView().post(new Runnable() {
//                        @Override
//                        public void run() {
//                            makeBoxForBall(newBall.getBallImageView());
//                        }
//                    });
//                }
            }
        });

        //Add ball to layoutView
        constraintLayout.addView(ballImgView);
        constraintLayout.addView(ballHitBoxImgView);
        Log.i(MY_TAG, "Made ball at (" + ballImgView.getX() + "," + ballImgView.getY() + ") ID: " + ballImgView.getId() + " TAG:" + ballImgView.getTag());

        //set TouchListener
        //ballHitBoxImgView.setOnTouchListener(new MyTouchListener(true, this));
        ballHitBoxImgView.setOnTouchListener(new MyTouchListener(true, this));

    }


    private Ball makeBallWithXY(float x, float y, int id) {
        ImageView ballHitBoxImgView = new ImageView(constraintLayout.getContext());
        ballHitBoxImgView.setImageResource(R.drawable.ball);
        ballHitBoxImgView.setTag("ball_" + id);
        ballHitBoxImgView.setZ(ZPositions.AboveBall.ordinal());
        ballHitBoxImgView.setId(R.id.ball);
        ballHitBoxImgView.setLayoutParams(new ConstraintLayout.LayoutParams(K_ballHitBoxSize, K_ballHitBoxSize));
        ballHitBoxImgView.setVisibility(View.INVISIBLE);
        ballHitBoxImgView.setAlpha(0f);

        ImageView ballImgView = new ImageView(constraintLayout.getContext());
        ballImgView.setImageResource(R.drawable.ball);
        ballImgView.setZ(ZPositions.Ball.ordinal());
        ballImgView.setId(R.id.ball);
        ballImgView.setLayoutParams(new ConstraintLayout.LayoutParams(K_ballSize, K_ballSize));
        ballImgView.setVisibility(View.INVISIBLE);

        //create Ball (backend) representation
        Ball newBall = new Ball(ballImgView,ballHitBoxImgView, totalBallsMadeEver);
        newBall.setBallActive();
        newBall.setPosition(x, y);

        ballImgView.setVisibility(View.VISIBLE);
        ballHitBoxImgView.setVisibility(View.VISIBLE);

        //Add ball to layoutView
        constraintLayout.addView(ballImgView);
        constraintLayout.addView(ballHitBoxImgView);
        Log.i(MY_TAG, "Made ball at (" + ballImgView.getX() + "," + ballImgView.getY() + ") ID: " + ballImgView.getId() + " TAG:" + ballImgView.getTag());

        balls.add(newBall);
        //set TouchListener
        //ballHitBoxImgView.setOnTouchListener(new MyTouchListener(true, this));
        ballHitBoxImgView.setOnTouchListener(new MyTouchListener(true, this));

        return newBall;
    }

    private void printBall(Ball b, boolean withVarNames, boolean withXY) {
        //Fake routine made for ease of printing & Fake physCalculator for ease of printing
        Routine r = new Routine(-99);
        PhysicsCalculator fakePhys = new PhysicsCalculator();

        setPhysicsCalcScreenConstants(fakePhys);
        r.setPhysicsCalculator(fakePhys);

        r.addBall(b);
        String printString = r.toString(withVarNames,withXY);

        //Print to both log tags
        Log.i(MY_TAG, " ");
        Log.i(MY_TAG, printString);

        Log.i(TAG_FIRE, " ");
        Log.i(TAG_FIRE, printString);
    }


    private ImageView testBallImageView;
    private ImageView makeTestBallImageView(PointXY pointXY) {
        ImageView ballImgView = new ImageView(constraintLayout.getContext());
        ballImgView.setImageResource(R.drawable.calibration_ball);
        ballImgView.setTag("ball_" + totalBallsMadeEver);
        ballImgView.setZ(ZPositions.Ball.ordinal());
        ballImgView.setId(R.id.ball);
        ballImgView.setLayoutParams(new ConstraintLayout.LayoutParams(K_ballSize, K_ballSize));


        float xPos = pointXY.getX() - K_ballSize / 2f;
        float yPos = pointXY.getY() - K_ballSize / 2f;
        ballImgView.setX(xPos);
        ballImgView.setY(yPos);


        ballImgView.setVisibility(View.VISIBLE);
        //Add ball to layoutView

        constraintLayout.addView(ballImgView);
        Log.i(MY_TAG, "Made ball at (" + ballImgView.getX() + "," + ballImgView.getY() + ") ID: " + ballImgView.getId() + " TAG:" + ballImgView.getTag());
        ballImgView.setOnTouchListener(new MyTouchListener(true, this));

        return ballImgView;
    }



    private ImageView selectedBallCircle;

    private void makeCircleBehindSelectedBall(Ball ball) {
        if (selectedBallCircle == null) {
            //Set tag
            //add golden circle behind it
            selectedBallCircle = new ImageView(constraintLayout.getContext());
            selectedBallCircle.setImageResource(R.drawable.ball);
            selectedBallCircle.setColorFilter(getResources().getColor(R.color.ballSelected_COL));

            selectedBallCircle.setTag("ball_" + totalBallsMadeEver + "_selectedCircle");
            selectedBallCircle.setZ(ZPositions.BehindBall.ordinal());
            //non unique ID
            selectedBallCircle.setId(R.id.ballSelectedCircle);

            int selectedBallCircleSize = K_ballSize * 2;
            selectedBallCircle.setLayoutParams(new ConstraintLayout.LayoutParams(selectedBallCircleSize, selectedBallCircleSize));
            //Hide until positioned correctly
            selectedBallCircle.setVisibility(View.INVISIBLE);
            //Add ball to layoutView
            constraintLayout.addView(selectedBallCircle);

            float xPos = ball.getX() - K_ballSize / 2f;
            float yPos = ball.getY() - K_ballSize / 2f;
            //Set position of newBall on toolbar
            selectedBallCircle.setX(xPos);
            selectedBallCircle.setY(yPos);

            //Positioned correctly so show ball
            selectedBallCircle.setVisibility(View.VISIBLE);
        } else {
            //Old circle, so delete and call itself
            removeCurrentSelectedCircle();
            makeCircleBehindSelectedBall(ball);
        }
    }

    private void removeCurrentSelectedCircle() {
        constraintLayout.removeView(selectedBallCircle);
        selectedBallCircle = null;
        tagOfSelectedBall = null;
    }

    ///This is the background (large table)
    private void makeTable() {
        //don't want to make multiple tables
        if (table == null) {
            table = findViewById(R.id.TableView);
            table.setTag("table");
            //            table.setOnDragListener(new MyDragListener(context, this));
            table.setZ(ZPositions.Table.ordinal());
            table.setBackgroundColor(getResources().getColor(R.color.primaryTable_COL));

            table.setOnTouchListener(new MyTouchListener(false, this));
            View lineV = findViewById(R.id.VerticalLineView);
            View lineH = findViewById(R.id.HorizontalLineView);
            lineV.setZ(ZPositions.TableLine.ordinal());
            lineH.setZ(ZPositions.TableLine.ordinal());

            table.setX(0);
            table.setY(0);

            makeActiveTableArea();
        }
    }

    ///This is the table area that balls can be dragged into
    private void makeActiveTableArea() {
        if (tableActiveArea == null) {
            //Make tableActiveArea
            View tableActiveAreaImageView = new ImageView(constraintLayout.getContext());
            tableActiveAreaImageView.setBackgroundColor(Color.parseColor("#00992061"));

            tableActiveAreaImageView.setOnTouchListener(new MyTouchListener(false, this));
//            tableActiveAreaImageView.setImageResource(R.drawable.table_active_area);
            //set tag to UNIQUE tag
            tableActiveAreaImageView.setTag("tableActiveArea");
            //Set Z index to above tables
            tableActiveAreaImageView.setZ(ZPositions.TableActiveArea.ordinal());
            //non unique ID
            tableActiveAreaImageView.setId(R.id.TableActiveAreaView);
            tableActiveAreaImageView.setVisibility(View.INVISIBLE);

            //Add drag listener
            tableActiveArea = tableActiveAreaImageView;

            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    //SIZE
                    int width = (int) (table.getMeasuredWidth() * 0.93f);//- 20;
                    int height = (int) ((table.getMeasuredHeight() / 2f) * 0.9f); // - 20;

                    tableActiveArea.setLayoutParams(new ConstraintLayout.LayoutParams(width, height));
                    //POSITION
                    tableActiveArea.setX(table.getX() + table.getMeasuredWidth()/2f  - width/2f);
                    tableActiveArea.setY(table.getY() + 3f * table.getMeasuredHeight()/4f - height/2f);

                    tableActiveArea.setVisibility(View.VISIBLE);
                    constraintLayout.addView(tableActiveArea);
                    tableActiveArea.setOnDragListener(new MyDragListener(context, MainActivity.this));
                }
            });

            //tableActiveArea.setBackgroundColor(getResources().getColor(R.color.my_green_light));
        }
    }


    private float StopPlayXWidthScalar = 11f / 12f;
    private float RecordXWidthScalar = 9f / 12f;
    private float BinXWidthScalar = 3f / 12f;
    private float SaveXWidthScalar = 3f / 12f;
    private float BallXWidthScalar = 6f / 12f;

    private void makeRecordButton() {
        //don't want to make multiple record buttons
        if (recordButton == null) {
            recordButton = findViewById(R.id.recordButton);
            recordButton.setTag("recordButton");

            recordButton.setOnTouchListener(new MyTouchListener(false, this));
            recordButton.setZ(ZPositions.Button.ordinal());
            recordButton.setVisibility(View.INVISIBLE);

            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float yPos = toolbar.getY() + toolbar.getMeasuredHeight() / 2f - recordButton.getMeasuredHeight() / 2f;
                    float xPos = toolbar.getX() + toolbar.getMeasuredWidth() * RecordXWidthScalar - recordButton.getMeasuredWidth() / 2f;
                    recordButton.setY(yPos);
                    recordButton.setX(xPos);
                    recordButton.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void makeBinButton() {
        //don't want to make multiple binButtons
        if (binButton == null) {
            binButton = findViewById(R.id.binButton);
            binButton.setTag("binButton");

            binButton.setOnTouchListener(new MyTouchListener(false, this));
            binButton.setZ(ZPositions.Button.ordinal());

            float binButtonScalar = 0.3f;
            binButton.setScaleX(binButtonScalar);
            binButton.setScaleY(binButtonScalar);
            //Hide until ball active
            binButton.setVisibility(View.INVISIBLE);

            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float yPos = toolbar.getY() + toolbar.getMeasuredHeight() / 2f - binButton.getMeasuredHeight() / 2f;
                    float xPos = toolbar.getX() + toolbar.getMeasuredWidth() * BinXWidthScalar - binButton.getMeasuredWidth() / 2f;
                    binButton.setY(yPos);
                    binButton.setX(xPos);
                }
            });
        }
    }


    private void makeStopButton() {
        //don't want to make multiple stopButtons
        if (stopButton == null) {
            stopButton = findViewById(R.id.stopButton);
            stopButton.setTag("stopButton");
            stopButton.setOnTouchListener(new MyTouchListener(false, this));
            stopButton.setZ(ZPositions.Button.ordinal());

            stopButton.setVisibility(View.INVISIBLE);

            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float yPos = toolbar.getY() + toolbar.getMeasuredHeight() / 2f - stopButton.getMeasuredHeight() / 2f;
                    float xPos = toolbar.getX() + toolbar.getMeasuredWidth() * StopPlayXWidthScalar - stopButton.getMeasuredWidth() / 2f;
                    stopButton.setY(yPos);
                    stopButton.setX(xPos);
                    stopButton.setVisibility(View.VISIBLE);
                }
            });
        }
    }

//    private void makeSaveButton() {
//        //don't want to make multiple saveButtons
//        if (saveButton == null) {
//            saveButton = findViewById(R.id.saveButton);
//            saveButton.setTag("saveButton");
//
//            saveButton.setScaleX(0.25f);
//            saveButton.setScaleY(0.25f);
////            saveButton.setOnTouchListener(new MyTouchListener(false, this));
//            saveButton.setZ(ZPositions.Button.ordinal());
//
//            mDrawerLayout.post(new Runnable() {
//                @Override
//                public void run() {
//                    float yPos = toolbar.getY() + toolbar.getMeasuredHeight() / 2f - saveButton.getMeasuredHeight() / 2f;
//                    float xPos = toolbar.getX() + toolbar.getMeasuredWidth() * SaveXWidthScalar - saveButton.getMeasuredWidth() / 2f;
//                    saveButton.setY(yPos);
//                    saveButton.setX(xPos);
//                    saveButton.setVisibility(View.VISIBLE);
//                }
//            });
//
//            saveButton.setVisibility(View.INVISIBLE);
//        }
//    }

    private Integer K_sendToRobotButtonSize = 500;

    private void makeSendToRobotButton() {
        if (sendToRobotButton == null) {
            sendToRobotButton = new ImageView(constraintLayout.getContext());
            sendToRobotButton.setId(R.id.sendToRobotButton);
            sendToRobotButton.setZ(ZPositions.Button.ordinal());

            sendToRobotButton.setImageResource(R.drawable.send_to_robot_button_in_circle);
            sendToRobotButton.setLayoutParams(new ConstraintLayout.LayoutParams(K_sendToRobotButtonSize, K_sendToRobotButtonSize));
            //hide until stop button pressed
            sendToRobotButton.setVisibility(View.INVISIBLE);


            //
            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth() / 1.9f - K_sendToRobotButtonSize / 2f;
                    float yPos = table.getY() + table.getMeasuredHeight() / 3.5f - K_sendToRobotButtonSize / 2f;

                    sendToRobotButton.setX(xPos);
                    sendToRobotButton.setY(yPos);
                }
            });

            //add sTRButton to layoutView
            constraintLayout.addView(sendToRobotButton);
            Log.i(MY_TAG, "Made button at (" + sendToRobotButton.getX() + "," + sendToRobotButton.getY() + ")");
            //set TouchListener
            sendToRobotButton.setOnTouchListener(new MyTouchListener(false, this));
        }
    }

    private Integer K_confirmButtonSize = 200;

    private void makeConfirmButton() {
        if (confirmButton == null) {
            confirmButton = new ImageView(constraintLayout.getContext());
            confirmButton.setId(R.id.confirmButton);
            confirmButton.setZ(ZPositions.Button.ordinal());


            confirmButton.setImageResource(R.drawable.confirm_button_in_circle);
            confirmButton.setLayoutParams(new ConstraintLayout.LayoutParams(K_confirmButtonSize, K_confirmButtonSize));
            //hide until stop button pressed
            confirmButton.setVisibility(View.INVISIBLE);


            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth() / 1.15f - K_confirmButtonSize / 2f;
                    float yPos = table.getY() + table.getMeasuredHeight() / 2.8f - K_confirmButtonSize / 2f;

                    confirmButton.setX(xPos);
                    confirmButton.setY(yPos);
                }
            });

            //add confirmButton to layoutView
            constraintLayout.addView(confirmButton);
            Log.i(MY_TAG, "Made button at (" + confirmButton.getX() + "," + confirmButton.getY() + ")");
            //set TouchListener
            confirmButton.setOnTouchListener(new MyTouchListener(false, this));
        }
    }

    private Integer K_returnButtonSize = 200;

    private void makeReturnButton() {
        if (returnButton == null) {
            returnButton = new ImageView(constraintLayout.getContext());
            returnButton.setId(R.id.returnButton);
            returnButton.setZ(ZPositions.Button.ordinal());


            returnButton.setImageResource(R.drawable.return_button_in_circle);
            returnButton.setLayoutParams(new ConstraintLayout.LayoutParams(K_returnButtonSize, K_returnButtonSize));
            //hide until stop button pressed
            returnButton.setVisibility(View.INVISIBLE);


            //
            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth() / 1.15f - K_returnButtonSize / 2f;
                    float yPos = table.getY() + table.getMeasuredHeight() / 4.1f - K_returnButtonSize / 2f;

                    returnButton.setX(xPos);
                    returnButton.setY(yPos);
                }
            });

            //add returnButton to layoutView
            constraintLayout.addView(returnButton);
            Log.i(MY_TAG, "Made button at (" + returnButton.getX() + "," + returnButton.getY() + ")");
            //set TouchListener
            returnButton.setOnTouchListener(new MyTouchListener(false, this));
        }
    }


    private Integer K_pauseButtonSize = 900;

    private void makePauseButton() {
        if (pauseButton == null) {
            pauseButton = new ImageView(constraintLayout.getContext());
            pauseButton.setId(R.id.pauseButton);
            pauseButton.setZ(ZPositions.Button.ordinal());

            pauseButton.setImageResource(R.drawable.pause_button_in_circle);
            pauseButton.setLayoutParams(new ConstraintLayout.LayoutParams(K_pauseButtonSize, K_pauseButtonSize));
            //hide until play button pressed
            pauseButton.setVisibility(View.INVISIBLE);


            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth() / 2f - K_pauseButtonSize / 2f;
                    float yPos = table.getY() + table.getMeasuredHeight() / 2f - K_pauseButtonSize / 2f;

                    pauseButton.setX(xPos);
                    pauseButton.setY(yPos);
                }
            });

            //add pauseButton to layoutView
            constraintLayout.addView(pauseButton);
            Log.i(MY_TAG, "Made button at (" + pauseButton.getX() + "," + pauseButton.getY() + ")");
            //set TouchListener
            pauseButton.setOnTouchListener(new MyTouchListener(false, this));
        }
    }

    private Integer K_calibrationArrowSize = 200;

    private void makeCalibrationArrowsAndButtons() {
        makeCalibrationUpArrow();
        makeCalibrationLeftArrow();
        makeCalibrationDownArrow();
        makeCalibrationRightArrow();
        makeCalibrationCircle();
        makeCalibrationConfirm();
    }

    private void makeCalibrationUpArrow() {
        if (calibrationUpArrow == null) {
            calibrationUpArrow = new ImageView(constraintLayout.getContext());
            calibrationUpArrow.setId(R.id.calibrationUpArrow);
            calibrationUpArrow.setZ(ZPositions.Button.ordinal());

            calibrationUpArrow.setImageResource(R.drawable.calibration_arrow_up);
            calibrationUpArrow.setLayoutParams(new ConstraintLayout.LayoutParams(K_calibrationArrowSize, K_calibrationArrowSize));
            //hide until calibrate button pressed
            calibrationUpArrow.setVisibility(View.INVISIBLE);




            //
            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth()/2f  - K_calibrationArrowSize/2f;
                    float yPos = table.getY() + table.getMeasuredHeight()*0.64f - K_calibrationArrowSize/2f;

                    calibrationUpArrow.setX(xPos);
                    calibrationUpArrow.setY(yPos);
                }
            });

            //add pauseButton to layoutView
            constraintLayout.addView(calibrationUpArrow);
            Log.i(MY_TAG, "Made button at (" + calibrationUpArrow.getX() + "," + calibrationUpArrow.getY() + ")");
            //set TouchListener
            calibrationUpArrow.setOnTouchListener(new MyTouchListener(false, this));
        }
    }

    private void makeCalibrationLeftArrow() {
        if (calibrationLeftArrow == null) {
            calibrationLeftArrow = new ImageView(constraintLayout.getContext());
            calibrationLeftArrow.setId(R.id.calibrationLeftArrow);
            calibrationLeftArrow.setZ(ZPositions.Button.ordinal());

            calibrationLeftArrow.setImageResource(R.drawable.calibration_arrow_left);
            calibrationLeftArrow.setLayoutParams(new ConstraintLayout.LayoutParams(K_calibrationArrowSize, K_calibrationArrowSize));
            //hide until calibrate button pressed
            calibrationLeftArrow.setVisibility(View.INVISIBLE);

            //
            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth()*0.37f  - K_calibrationArrowSize/2f;
                    float yPos = table.getY() + table.getMeasuredHeight()/1.35f - K_calibrationArrowSize/2f;

                    calibrationLeftArrow.setX(xPos);
                    calibrationLeftArrow.setY(yPos);
                }
            });

            //add pauseButton to layoutView
            constraintLayout.addView(calibrationLeftArrow);
            Log.i(MY_TAG, "Made button at (" + calibrationLeftArrow.getX() + "," + calibrationLeftArrow.getY() + ")");
            //set TouchListener
            calibrationLeftArrow.setOnTouchListener(new MyTouchListener(false, this));
        }
    }

    private void makeCalibrationDownArrow() {
        if (calibrationDownArrow == null) {
            calibrationDownArrow = new ImageView(constraintLayout.getContext());
            calibrationDownArrow.setId(R.id.calibrationDownArrow);
            calibrationDownArrow.setZ(ZPositions.Button.ordinal());

            calibrationDownArrow.setImageResource(R.drawable.calibration_arrow_down);
            calibrationDownArrow.setLayoutParams(new ConstraintLayout.LayoutParams(K_calibrationArrowSize, K_calibrationArrowSize));
            //hide until calibrate button pressed
            calibrationDownArrow.setVisibility(View.INVISIBLE);




            //
            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth()/2f  - K_calibrationArrowSize/2f;
                    float yPos = table.getY() + table.getMeasuredHeight()/1.187f- K_calibrationArrowSize/2f;

                    calibrationDownArrow.setX(xPos);
                    calibrationDownArrow.setY(yPos);
                }
            });

            //add pauseButton to layoutView
            constraintLayout.addView(calibrationDownArrow);
            Log.i(MY_TAG, "Made button at (" + calibrationDownArrow.getX() + "," + calibrationDownArrow.getY() + ")");
            //set TouchListener
            calibrationDownArrow.setOnTouchListener(new MyTouchListener(false, this));
        }
    }

    private void makeCalibrationRightArrow() {
        if (calibrationRightArrow == null) {
            calibrationRightArrow = new ImageView(constraintLayout.getContext());
            calibrationRightArrow.setId(R.id.calibrationRightArrow);
            calibrationRightArrow.setZ(ZPositions.Button.ordinal());

            calibrationRightArrow.setImageResource(R.drawable.calibration_arrow_right);
            calibrationRightArrow.setLayoutParams(new ConstraintLayout.LayoutParams(K_calibrationArrowSize, K_calibrationArrowSize));
            //hide until calibrate button pressed
            calibrationRightArrow.setVisibility(View.INVISIBLE);

            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth()-table.getMeasuredWidth()*0.37f  - K_calibrationArrowSize/2f;
                    float yPos = table.getY() + table.getMeasuredHeight()/1.35f - K_calibrationArrowSize/2f;

                    calibrationRightArrow.setX(xPos);
                    calibrationRightArrow.setY(yPos);
                }
            });

            //add pauseButton to layoutView
            constraintLayout.addView(calibrationRightArrow);
            Log.i(MY_TAG, "Made button at (" + calibrationRightArrow.getX() + "," + calibrationRightArrow.getY() + ")");
            //set TouchListener
            calibrationRightArrow.setOnTouchListener(new MyTouchListener(false, this));
        }
    }

    private Integer K_calibrationCircleSize = 150;

    private void makeCalibrationCircle() {
        if (calibrationCircle == null) {
            calibrationCircle = new ImageView(constraintLayout.getContext());
            calibrationCircle.setId(R.id.calibrationCircle);
            calibrationCircle.setZ(ZPositions.Button.ordinal());

            calibrationCircle.setImageResource(R.drawable.calibration_circle);
            calibrationCircle.setLayoutParams(new ConstraintLayout.LayoutParams(K_calibrationCircleSize, K_calibrationCircleSize));
            //hide until calibrate button pressed
            calibrationCircle.setVisibility(View.INVISIBLE);

            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth()/2f  - K_calibrationCircleSize/2f;
                    float yPos = table.getY() + table.getMeasuredHeight()/1.35f - K_calibrationCircleSize/2f;

                    calibrationCircle.setX(xPos);
                    calibrationCircle.setY(yPos);
                }
            });

            //add pauseButton to layoutView
            constraintLayout.addView(calibrationCircle);
            Log.i(MY_TAG, "Made button at (" + calibrationCircle.getX() + "," + calibrationCircle.getY() + ")");
            //set TouchListener
            calibrationCircle.setOnTouchListener(new MyTouchListener(false, this));
        }
    }

    private void makeCalibrationConfirm() {
        if (calibrationConfirm == null) {
            calibrationConfirm = new ImageView(constraintLayout.getContext());
            calibrationConfirm.setId(R.id.calibrationConfirm);
            calibrationConfirm.setZ(ZPositions.Button.ordinal());

            calibrationConfirm.setImageResource(R.drawable.calibration_circle_confirm);
            calibrationConfirm.setLayoutParams(new ConstraintLayout.LayoutParams(K_calibrationCircleSize, K_calibrationCircleSize));
            //hide until calibrate button pressed
            calibrationConfirm.setVisibility(View.INVISIBLE);

            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    float xPos = table.getX() + table.getMeasuredWidth()/2f  - K_calibrationCircleSize/2f;
                    float yPos = table.getY() + table.getMeasuredHeight()/1.35f - K_calibrationCircleSize/2f;

                    calibrationConfirm.setX(xPos);
                    calibrationConfirm.setY(yPos);
                }
            });

            //add pauseButton to layoutView
            constraintLayout.addView(calibrationConfirm);
            Log.i(MY_TAG, "Made button at (" + calibrationConfirm.getX() + "," + calibrationConfirm.getY() + ")");
            //set TouchListener
            calibrationConfirm.setOnTouchListener(new MyTouchListener(false, this));
        }
    }


    private void addCircleBehindSpinner(DoubleSpinner doubleSpinner) {
        int doubleSpinnerWidth = doubleSpinner.getWidth();

        doubleSpinner.circleBehind = new ImageView(constraintLayout.getContext());
        doubleSpinner.circleBehind.setImageResource(R.drawable.spinner_back_circle);

        doubleSpinner.circleBehind.setTag("circle_behindSpinner");
        doubleSpinner.circleBehind.setId(R.id.circleBehindSpinner);

        int diameter = (int) (doubleSpinner.getScaledWidth() * 1.1f);
        doubleSpinner.circleBehind.setLayoutParams(new ConstraintLayout.LayoutParams(diameter, diameter));
        Log.i(MY_TAG, "Radius of circle behind spinner = " + diameter);

        //Position --> called after
        float x = doubleSpinner.getX() + doubleSpinnerWidth / 2f - diameter / 2f;
        float y = doubleSpinner.getY() + doubleSpinnerWidth / 2f - diameter / 2f;
        doubleSpinner.circleBehind.setX(x);
        doubleSpinner.circleBehind.setY(y);
        doubleSpinner.circleBehind.setVisibility(View.VISIBLE);

        doubleSpinner.circleBehind.setZ(ZPositions.BehindSpinner.ordinal());

        constraintLayout.addView(doubleSpinner.circleBehind);

    }


    private PointXY getControllerPosition(PointXY cursorLocation, PointXY centreCircle) {
//        paintPoint(centreCircle);
        PointXY pointOnCircle = new PointXY();
        PointXY thirdTriPoint = new PointXY(cursorLocation.getX(), centreCircle.getY());

        ///LARGE Triangle to cursorLocation --> centreCircle ---> BottomRightPoint
        double sizeOfOppLargeTri = (cursorLocation.getY() - thirdTriPoint.getY());
        double sizeOfAdjLargeTri = (thirdTriPoint.getX() - centreCircle.getX());
//        Log.i(MY_TAG,"sizeOfOppLargeTri: " + sizeOfOppLargeTri + ",  sizeOfAdjLargeTri: " + sizeOfAdjLargeTri);

        double angle = Math.atan(sizeOfOppLargeTri / sizeOfAdjLargeTri);
        if (thirdTriPoint.getX() < centreCircle.getX()) { angle += Math.PI; }
        else if (angle < 0) { angle += 2 * Math.PI; }

        double radiusOfCircle = radiusOfOrbitCircle; //change this to change orbit circle
        ///LARGE Triangle to orbitCircle --> centreCircle ---> diff BottomRightPoint
        double sizeOfAdjSmallTri = radiusOfCircle * Math.cos(angle);
        double sizeOfOppSmallTri = radiusOfCircle * Math.sin(angle);
//        Log.i(MY_TAG,"sizeOfAdjSmallTri: " + sizeOfAdjSmallTri + ",  sizeOfOppSmallTri: " + sizeOfOppSmallTri);

        pointOnCircle.setX(centreCircle.getX() + (float) sizeOfAdjSmallTri);
        pointOnCircle.setY(centreCircle.getY() + (float) sizeOfOppSmallTri);

        pointOnCircle.setX(pointOnCircle.getX() - K_SpinnerControllerSize / 2f);
        pointOnCircle.setY(pointOnCircle.getY() - K_SpinnerControllerSize / 2f);

        return pointOnCircle;
    }

    private void setControllerPosition(DoubleSpinner doubleSpinner, PointXY pointOnOrbitCircle) {
        doubleSpinner.spinnerController.setX(pointOnOrbitCircle.getX());
        doubleSpinner.spinnerController.setY(pointOnOrbitCircle.getY());

        doubleSpinner.spinnerControllerTouchArea.setX(pointOnOrbitCircle.getX() + K_SpinnerControllerSize / 2f - K_SpinnerControllerTouchAreaSize / 2f);
        doubleSpinner.spinnerControllerTouchArea.setY(pointOnOrbitCircle.getY() + K_SpinnerControllerSize / 2f - K_SpinnerControllerTouchAreaSize / 2f);

        saveSpinnerPosToSelectedBall(doubleSpinner);
    }

    private void saveSpinnerPosToSelectedBall(DoubleSpinner doubleSpinner) {
        try {
            Ball selectedBall = getBallCurrentlySelected();
            if (selectedBall != null) {
                if(doubleSpinner == topSpinner) {
                    selectedBall.setSpinHandlePosTopWheel(new PointXY(doubleSpinner.spinnerController.getX(), doubleSpinner.spinnerController.getY()));
                    selectedBall.setTopTextVal(doubleSpinner.textView.getText().toString());
                }
                else {
                    selectedBall.setSpinHandlePosBottomWheel(new PointXY(doubleSpinner.spinnerController.getX(), doubleSpinner.spinnerController.getY()));
                    selectedBall.setBtmTextVal(doubleSpinner.textView.getText().toString());
                }
            }
        }
        catch (NullPointerException n_err) {
        }
    }

    private float radiusOfOrbitCircle;
    private float radiusOfBackingCircle;
    private float K_SpinnerControllerSize;
    private float K_SpinnerControllerTouchAreaSize;

    private void addSpinnerController(DoubleSpinner doubleSpinner) {
        doubleSpinner.spinnerController = new ImageView(constraintLayout.getContext());
        doubleSpinner.spinnerController.setImageResource(R.drawable.spinner_controller);

        doubleSpinner.spinnerControllerTouchArea = new ImageView(constraintLayout.getContext());
        doubleSpinner.spinnerControllerTouchArea.setImageResource(R.drawable.spinner_controller);
//        doubleSpinner.spinnerController.setTag(doubleSpinner.);

        Log.i(MY_TAG, "doubleSpinner.circleBehind.getWidth() = " + doubleSpinner.circleBehind.getWidth());
        radiusOfBackingCircle = doubleSpinner.circleBehind.getWidth();
        K_SpinnerControllerSize = (int) (radiusOfBackingCircle * 0.15); //Change size of K_SpinnerControllerSize
        K_SpinnerControllerTouchAreaSize = (int) (radiusOfBackingCircle * 0.5);

        doubleSpinner.spinnerController.setLayoutParams(new ConstraintLayout.LayoutParams((int) K_SpinnerControllerSize, (int) K_SpinnerControllerSize));
        doubleSpinner.spinnerControllerTouchArea.setLayoutParams(new ConstraintLayout.LayoutParams((int) K_SpinnerControllerTouchAreaSize, (int) K_SpinnerControllerTouchAreaSize));
        Log.i(MY_TAG, "Size of spinController = " + K_SpinnerControllerSize);


        radiusOfOrbitCircle = radiusOfBackingCircle * 0.8f - K_SpinnerControllerSize/4f;
        PointXY circleCentre = new PointXY(doubleSpinner.circleBehind.getX() + radiusOfBackingCircle / 2f, doubleSpinner.circleBehind.getY() + radiusOfBackingCircle / 2f);

        //set controller to align with K_StartAngleDegress
        PointXY startFakeTouchPoint = new PointXY(circleCentre.getX() + 900, circleCentre.getY());

        doubleSpinner.centreCircle = circleCentre;
        PointXY pointOnOrbitCircle = getControllerPosition(startFakeTouchPoint, doubleSpinner.centreCircle);


        setControllerPosition(doubleSpinner, pointOnOrbitCircle);

//        doubleSpinner.spinnerController.setX(doubleSpinner.getX() + radiusOfOrbitCircle + xOffSet);
//        doubleSpinner.spinnerController.setY(doubleSpinner.getY() + radiusOfOrbitCircle);
        doubleSpinner.spinnerController.setVisibility(View.VISIBLE);
        doubleSpinner.spinnerController.setZ(ZPositions.AboveSpinner.ordinal());
        doubleSpinner.spinnerController.setId(R.id.spinnerController);
        doubleSpinner.spinnerController.setTag(spinnerTag + doubleSpinner.getId() + "controllerVisual");

        doubleSpinner.spinnerControllerTouchArea.setVisibility(View.VISIBLE);
        doubleSpinner.spinnerControllerTouchArea.setZ(ZPositions.AboveSpinner.ordinal());
        doubleSpinner.spinnerControllerTouchArea.setId(R.id.spinnerController);
        doubleSpinner.spinnerControllerTouchArea.setTag(spinnerTag + doubleSpinner.getId());

        doubleSpinner.spinnerControllerTouchArea.setAlpha(0f); //hide it
        constraintLayout.addView(doubleSpinner.spinnerController);
        constraintLayout.addView(doubleSpinner.spinnerControllerTouchArea);
        doubleSpinner.spinnerControllerTouchArea.setOnTouchListener(new MyTouchListener(false, this));
    }


    private void makeDoubleSpinner(boolean isTopSpinner) {
        float progScale = 0.4f;

        int clockID = isTopSpinner ? R.id.progClockwiseTop : R.id.progClockwiseBtm;
        int anticlockID = isTopSpinner ? R.id.progAnticlockwiseTop : R.id.progAnticlockwiseBtm;
        int ID = isTopSpinner ? 1 : 2;


        final CircleProgressBar progClockwise = (CircleProgressBar) findViewById(clockID);
        final CircleProgressBar progAnticlockwise = (CircleProgressBar) findViewById(anticlockID);
        DoubleSpinner doubleSpinner = new DoubleSpinner(progClockwise, progAnticlockwise, progScale, ID);

        progClockwise.setZ(ZPositions.Spinner.ordinal());
        progAnticlockwise.setZ(ZPositions.Spinner.ordinal());
        progClockwise.setColor(getResources().getColor(R.color.spinnerInnerOrange_COL));
        progAnticlockwise.setColor(getResources().getColor(R.color.spinnerInnerCyan_COL));

        TextView textView = new TextView(context);
        textView.setZ(ZPositions.AboveSpinner.ordinal());
        textView.setTextSize(progScale * 85);
        textView.setVisibility(View.INVISIBLE);
        textView.setTextColor(getResources().getColor(R.color.primaryWhite_COL));
        doubleSpinner.textView = textView;

        constraintLayout.post(new Runnable() {
            @Override
            public void run() {
                int progCWidth = progClockwise.getMeasuredWidth();
                int progCHeight = progClockwise.getMeasuredHeight();

                textView.setX(progClockwise.getX() + progCWidth / 2f);
                textView.setY(progClockwise.getY() + progCHeight / 2f);
//                spinnerTextTop.setVisibility(View.VISIBLE);

                float yGap = doubleSpinner.getScaledWidth() * 0.5f;

                float YHeight = isTopSpinner ? yGap / 2f : doubleSpinner.getScaledWidth() + yGap * 2f;
                doubleSpinner.setY(YHeight);
            }
        });

        textView.post(new Runnable() {
            @Override
            public void run() {
                adjustSpinnerTextPosition(textView, doubleSpinner);
            }
        });
        constraintLayout.addView(textView);

        if (isTopSpinner) {
            topSpinner = doubleSpinner;
        } else {
            btmSpinner = doubleSpinner;
        }
        adjustSpinnerTextPosition(textView,doubleSpinner);

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //
    ///////////////////  HELPER METHODS FOR CREATION  ///////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




    private int getRingButtonIDFromButton(ImageView buttonToRing) throws NullPointerException {
        switch (buttonToRing.getId()) {
            case R.id.recordButton:
                return R.id.ring_rec;
            case R.id.stopButton:
                return R.id.ring_stop;
            case R.id.ball:
                return R.id.ring_ball;
//            case R.id.saveButton:
//                return R.id.ring_save;
            case R.id.binButton:
                return R.id.ring_bin;
            default:
                break;
        }

        throw new NullPointerException();
    }

    private String getRingButtonTagFromButton(ImageView buttonToRing) throws NullPointerException {
        switch (buttonToRing.getId()) {
            case R.id.recordButton:
                return "Ring_Record";
            case R.id.stopButton:
                return "Ring_Stop";
            case R.id.ball:
                return "Ring_Ball";
//            case R.id.saveButton:
//                return "Ring_Save";
            case R.id.binButton:
                return "Ring_Bin";
            default:
                break;
        }

        throw new NullPointerException();
    }


    private void adjustSpinnerText(TextView textView, DoubleSpinner doubleSpinner, String text) {
        doubleSpinner.textView.setText(text);
        adjustSpinnerTextPosition(textView,doubleSpinner);
    }

    private void adjustSpinnerTextPosition(TextView textView, DoubleSpinner doubleSpinner) {
        int progCWidth = doubleSpinner.getProgWidth();
        int progCHeight = doubleSpinner.getProgHeight();
        float progClockwiseX = doubleSpinner.getProgX();
        float progClockwiseY = doubleSpinner.getProgY();

        int textViewWidth = textView.getMeasuredWidth();
        int textViewHeight = textView.getMeasuredHeight();

        textView.setX(progClockwiseX + progCWidth / 2f - textViewWidth * 0.465f); //0.465f means Text sits exactly down middle for all digits (e.g. when 1 digit = 0, when 2 digits 10-99 and 3 digits (100))
        textView.setY(progClockwiseY + progCHeight / 2f - textViewHeight / 2f);
        textView.setVisibility(View.VISIBLE);
    }


    private void paintPoint(PointXY pointXY) {
        ImageView pointMarker = new ImageView(constraintLayout.getContext());
        pointMarker.setImageResource(R.drawable.generic_box);

        pointMarker.setZ(ZPositions.AboveSpinner.ordinal());
        int boxSize = K_ballSize;
        pointMarker.setLayoutParams(new ConstraintLayout.LayoutParams(boxSize, boxSize));


        pointMarker.setX(pointXY.getX() - pointMarker.getWidth() / 2f);
        pointMarker.setY(pointXY.getY() - pointMarker.getHeight() / 2f);
        pointMarker.setVisibility(View.INVISIBLE);


        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                pointMarker.setX(pointXY.getX() - pointMarker.getWidth() / 2f);
                pointMarker.setY(pointXY.getY() - pointMarker.getHeight() / 2f);
                pointMarker.setVisibility(View.VISIBLE);

            }
        });

        constraintLayout.addView(pointMarker);

    }


    private float getAngle(PointXY cursorLocation, PointXY centreCircle) {
        PointXY thirdTriPoint = new PointXY(cursorLocation.getX(), centreCircle.getY());
        double sizeOfOppLargeTri = (cursorLocation.getY() - thirdTriPoint.getY());
        double sizeOfAdjLargeTri = (thirdTriPoint.getX() - centreCircle.getX());

        double angle = Math.atan(sizeOfOppLargeTri / sizeOfAdjLargeTri);

        if (thirdTriPoint.getX() < centreCircle.getX()) {
            angle += Math.PI;
        } else if (angle < 0) {
            angle += 2 * Math.PI;
        }

        double angleForProgressBars = angle * 2f;
        Log.i(MY_EMPTY_TAG, "angleForProgressBars = " + Math.toDegrees(angleForProgressBars));


//        Log.i(MY_TAG,"angle: " + Math.toDegrees(angle));
        return (float) angle;
    }

    private PointXY getPointOnOrbitCircleFromDegrees(float radius, float degrees, PointXY centreCircle) {
        PointXY cursorLocation = new PointXY(centreCircle.getX() + radius * (float) Math.cos(degrees), centreCircle.getY() + radius * (float) Math.sin(degrees));
        return cursorLocation;
    }

    private float getAngleReverse(float radius, float degrees, PointXY centreCircle) {
        PointXY cursorLocation = new PointXY(centreCircle.getX() + radius * (float) Math.cos(degrees), centreCircle.getY() + radius * (float) Math.sin(degrees));

        PointXY thirdTriPoint = new PointXY(cursorLocation.getX(), centreCircle.getY());
        double sizeOfOppLargeTri = (cursorLocation.getY() - thirdTriPoint.getY());
        double sizeOfAdjLargeTri = (thirdTriPoint.getX() - centreCircle.getX());

        double angle = Math.atan(sizeOfOppLargeTri / sizeOfAdjLargeTri);

        if (thirdTriPoint.getX() < centreCircle.getX()) {
            angle += Math.PI;
        } else if (angle < 0) {
            angle += 2 * Math.PI;
        }

        double angleForProgressBars = angle * 2f;
        Log.i(MY_EMPTY_TAG, "angleForProgressBarsREVERSE = " + Math.toDegrees(angleForProgressBars));


//        Log.i(MY_TAG,"angle: " + Math.toDegrees(angle));
        return (float) angle;
    }

    private void logPoint(PointXY pointXY, String varName) {
        Log.i(MY_TAG, varName + ": (" + pointXY.getX() + "," + pointXY.getY() + ")");

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //
    ///////////////////  TOUCH HANDLING ///////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void touchesBegan(View srcView) {
        Log.i(MY_TAG, "touchesBegan: " + srcView.getTag());

        switch (srcView.getId()) {
            case R.id.sendToRobotButton:
                sTRButtonTouched();
                break;
            case R.id.confirmButton:
                confirmButtonTouched();
                break;
            case R.id.returnButton:
                returnButtonTouched();
                break;
            case R.id.pauseButton:
                pauseButtonTouched();
                break;
            case R.id.recordButton:
                //recordButtonTouched();
                break;
            case R.id.stopButton:
                //stopButtonTouched();
                break;
            case R.id.calibrationUpArrow:
                calibrationUpArrowTouched();
                break;
            case R.id.calibrationLeftArrow:
                calibrationLeftArrowTouched();
                break;
            case R.id.calibrationDownArrow:
                calibrationDownArrowTouched();
                break;
            case R.id.calibrationRightArrow:
                calibrationRightArrowTouched();
                break;
            case R.id.calibrationCircle:
                calibrationCircleTouched();
                break;
            case R.id.calibrationConfirm:
                calibrationConfirmed();
                break;
            case R.id.spinnerController:
                spinnerControllerTouchBegan(srcView);
                break;
            case R.id.ring_rec:
                recordButtonTouched();
                break;
            case R.id.ring_stop:
                Log.i(MY_TAG, "stop ring");
                stopButtonTouched();
                break;
            case R.id.ring_bin:
                binButtonTouched();
                break;
            case R.id.ring_ball:
//                Ball ringBall = balls.get(balls.size()-1); // make sure always get new ball in ring
//                MotionEvent motionEvent = new MotionEvent() //https://stackoverflow.com/questions/5867059/android-how-to-create-a-motionevent
//                ringBall.getBallImageView().onTouchEvent();
                break;
            case R.id.ring_save:
                saveButtonTouched();
                break;
            case R.id.ball:
                ballTouched(srcView.getTag());
                break;
            case R.id.TableActiveAreaView:
                tableActiveAreaTouched();
                break;
            case R.id.TableView:
                tableTouched();
                break;
            default:
                break;
        }
    }


    @Override
    public void touchesEnded(View srcView, float x, float y) {
        Log.i(MY_TAG, "touchesEnded: " + srcView.getTag() + "  x: " + x + " y: " + y + "\n");

        switch (srcView.getId()) {
            case R.id.spinnerController:
                spinnerControllerTouchEnded(srcView, x, y);
                break;
            default:
                break;
        }
    }


    @Override
    public void touchesMoved(View srcView, float rawX, float rawY) {

        switch (srcView.getId()) {
            case R.id.spinnerController:
//                Log.i(MY_TAG,"touchesMoved: " + srcView.getTag() + "  x: " + rawX + " y: " + rawY + "\n");
                spinnerControllerTouchMoved(srcView, rawX, rawY);
                break;
            default:
                break;
        }

    }

    //Called through TouchInterface from MyDragListener when ball has been dragged
    @Override
    public void hasDragged(View srcView, float dragX, float dragY, ViewGroup newSrcOwner) {
//        Log.i(MY_TAG, "hasDragged:");
        String tag = srcView.getTag().toString();
        tag = tag.toLowerCase();

        try {
            if (tag.startsWith("ball")) {
                Log.i(MY_TAG, "hasDragged: a ball");

                float newX = tableActiveArea.getX() + dragX - K_ballSize/2f;// - getTableMargin();
                float newY = tableActiveArea.getY() + dragY - K_ballSize/2f; //tableActiveArea.getMeasuredHeight();// - getTableMargin();
//                Log.i(MY_TAG, "tableActiveArea = " + "(" + tableActiveArea.getX() + "," + tableActiveArea.getY() + ")");
//                Log.i(MY_TAG, "hasDragged: ball RELATIVE = " + "(" + dragX + "," + dragY + ") ABSOLUTE = " + "(" + newX + ", " + newY + ") ");
                //Get ball dragged by searching tag
                Ball ballDragged = getBallFromTag(srcView.getTag());
                float oldX = ballDragged.getBallImageView().getX();
                float oldY = ballDragged.getBallImageView().getY();
                Log.i(MY_TAG, "hasDragged: ball drag from" + "(" + oldX + "," + oldY + ") to " + "(" + newX + ", " + newY + ") " +
                        "ID: " + srcView.getId() + " TAG:" + srcView.getTag());

                ///Validate new position
                //Check no overlap with other balls
                boolean newBallOnExistingBall = checkIfAllBallPositionsAreValid(ballDragged, newX, newY);
                //No need to check anything else as guaranteed to be in tableActiveArea
                boolean validDropPosition = (newBallOnExistingBall == false);
                Log.i(MY_TAG, "validDropPosition: " + validDropPosition + " -->  newBallOnExistingBall: " + newBallOnExistingBall);


                //VALID so Move ball to new location
                if (validDropPosition) {
                    ballDragged.setPosition(newX, newY);
                    ballDropWasValid(ballDragged);
                } else { //INVALID - keep ball at old location
                    Log.i(MY_TAG, "Invalid Drop: ball still at " + "(" + ballDragged.getX() + "," + ballDragged.getY() + ")");
                }
            } else {
                Log.i(MY_TAG, "hasDragged: non-ball dragged");

            }
        } catch (NullPointerException err) {
            Log.i(MY_TAG, "No ball with tag: " + srcView.getTag());
        }
    }


    private String tagOfSelectedBall = null;

    @Override
    public boolean allowDrag(View srcView) {
        if (currentState == State.Recording) {
            Log.i(MY_TAG, "Can't drag while recording");
            return false;
        }
        //disallow drag if not ball
        if (srcView.getId() == R.id.ball) {
            Ball ball = getBallFromTag(srcView.getTag());
            if (tagOfSelectedBall == null) {
                return true;
            }
            if (ball.isActive() == false) {
                //must be ball icon - user adding ball - allow drag
                return true;
            } else if (srcView.getTag() == tagOfSelectedBall) {
                //TODO: Do we allow drag of selected ball?
                ball.getBallImageView().setVisibility(View.INVISIBLE);
                if (isBallCurrentlySelected(ball)) {
                    selectedBallCircle.setColorFilter(getResources().getColor(R.color.ballSelectedFaded_COL));
                }
                return true;
            } else { //not selected ball
                return false;
            }
        }
        return false;
    }

    private final String spinnerTag = "spinnerController_";

    private void spinnerControllerTouchBegan(View view) {
        Log.i(MY_TAG, "spinnerControllerTouchBegan");
        String tag = (String) view.getTag();

        switch (tag) {
            case (spinnerTag + "1"): //top Spinner Controller
//                Log.i(MY_TAG, "Top Spinner Controller");
                break;
            case (spinnerTag + "2"): //bottom Spinner Controller
//                Log.i(MY_TAG, "Bottom Spinner Controller");
                break;
            default:
                break;
        }

    }

    private void spinnerControllerTouchMoved(View view, float rawX, float rawY) {
        Log.i(MY_TAG, "spinnerControllerTouchMoved");
        String tag = (String) view.getTag();
        PointXY touchPoint = new PointXY(rawX, rawY);

        boolean isTopSpinner = false;
        switch (tag) {
            case (spinnerTag + "1"): //top Spinner Controller
                isTopSpinner = true;
                break;
            case (spinnerTag + "2"): //bottom Spinner Controller
                isTopSpinner = false;
                break;
            default:
                break;
        }
        DoubleSpinner doubleSpinner = isTopSpinner ? topSpinner : btmSpinner;

        adjustSpinnerControllerAndText(doubleSpinner, touchPoint);
        adjustSelectedBallSpin();
    }

    private void adjustSpinnerControllerAndText(DoubleSpinner doubleSpinner, PointXY touchPoint) {
        PointXY pointOnOrbitCircle = getNewSpinnerControllerPosition(doubleSpinner,touchPoint);

        Log.i(MY_TAG,"pointOnOrbitCircle = " + pointOnOrbitCircle.getX() + " ," + pointOnOrbitCircle.getY());

        String textStr = "" + (int) doubleSpinner.getSpinnerMappedValue();
        adjustSpinnerText(doubleSpinner.textView,doubleSpinner,textStr);

        //Change spinner controller position and backend progress
        doubleSpinner.setProgress(doubleSpinner.culmAngleDegrees);

        setControllerPosition(doubleSpinner, pointOnOrbitCircle);
    }


    private void adjustSelectedBallSpin() {
        try {
            Ball selectedBall = getBallCurrentlySelected(); //TODO: CAN THROW NULL
            if (selectedBall != null) {
                selectedBall.setTopWheel(topSpinner.getSpinnerMappedValue());
                selectedBall.setBottomWheel(btmSpinner.getSpinnerMappedValue());
            }
        }
        catch (NullPointerException n_err) {

        }
    }

    //Given a spin/degrees, should work out spinner handle position and text
    private void changeSpinnerToPreviousBallPosition(DoubleSpinner doubleSpinner, float deg, PointXY previousSpinControllerPos, String textVal) {
        Log.i(MY_EMPTY_TAG, " Attempting to change spinner to degrees: " + deg);
        Log.i(MY_TAG, " Attempting to change spinner to degrees: " + deg);

        doubleSpinner.culmAngleDegrees = deg;
        doubleSpinner.prevAngleDegrees = deg;

        String textStr = (textVal != null) ? "" + textVal :  "" + doubleSpinner.getSpinnerMappedValue();
        adjustSpinnerText(doubleSpinner.textView,doubleSpinner,textStr);
        //Change spinner controller position and backend progress
        doubleSpinner.setProgress(doubleSpinner.culmAngleDegrees);

        setControllerPosition(doubleSpinner, previousSpinControllerPos);
        //adjustSpinnerControllerAndText(doubleSpinner,previousSpinControllerPos);


//        PointXY point = getSpinnerControllerPositionFromDegreesValue(doubleSpinner,deg);
//        paintPoint(point);




//        getAngleReverse(radiusOfOrbitCircle,deg,doubleSpinner.centreCircle);
//
//        PointXY pointOnOrbitCircle = getPointOnOrbitCircleFromDegrees(radiusOfOrbitCircle,deg,doubleSpinner.centreCircle);
    }

    private void changeSpinnersForSelectedBall(Ball ball) {
        int topSpin = ball.getTopWheel();
        int bottomSpin = ball.getBottomWheel();

        float topDegrees = topSpinner.getReverseSpinnerMappedValue(topSpin);
        float btmDegrees = btmSpinner.getReverseSpinnerMappedValue(bottomSpin);

        if (ball.getSpinHandlePosTopWheel() != null) {
            changeSpinnerToPreviousBallPosition(topSpinner,topDegrees, ball.getSpinHandlePosTopWheel(), ball.getTopTextVal());
        }
        if (ball.getSpinHandlePosBottomWheel() != null) {
            changeSpinnerToPreviousBallPosition(btmSpinner,btmDegrees, ball.getSpinHandlePosBottomWheel(), ball.getBtmTextVal());
        }
    }


    //Used for working out where controller should be for balls with a spin already
    private PointXY getSpinnerControllerPositionFromDegreesValue(DoubleSpinner doubleSpinner, float deg) {
        ///Get angle from 0 - 720

        //40 = 0', 100 = 360', 160 = 720'

        int degrees = (int) HelperFunctions.map(deg, 40, 160, 0, 720);
        //𝑥 =5cos𝜃,and 𝑦 =5sin𝜃.
        float xPos = doubleSpinner.centreCircle.getX() +  (float) radiusOfOrbitCircle * (float) Math.cos(degrees);
        float yPos = doubleSpinner.centreCircle.getY() + (float) radiusOfOrbitCircle * (float) Math.sin(degrees);

        //convert angle into x,y pos
        PointXY pointOnOrbitCircle = new PointXY(xPos,yPos);
        //put fake point into getNewSpinnerControllerPosition   TODO: relying on this is bad - prevAngle
        //PointXY pointOnOrbitCircle = getControllerPosition(fakeTouchPoint, doubleSpinner.centreCircle);


        //paintPoint(pointOnOrbitCircle);
//        paintPoint(pointOnOrbitCircle);
        //paintPoint(doubleSpinner.centreCircle);

        return pointOnOrbitCircle;
    }

    private PointXY getNewSpinnerControllerPosition(DoubleSpinner doubleSpinner, PointXY touchPoint) {
        PointXY startFakeTouchPoint = new PointXY(doubleSpinner.centreCircle.getX() + 200, doubleSpinner.centreCircle.getY());

        PointXY pointOnOrbitCircle = getControllerPosition(touchPoint, doubleSpinner.centreCircle);


        float angleDeg = (float) Math.toDegrees(getAngle(touchPoint, doubleSpinner.centreCircle));
        float diffInAnglesDeg = angleDeg - doubleSpinner.prevAngleDegrees;
        //Log.i(MY_TAG, "culmAngleDegrees = " + doubleSpinner.culmAngleDegrees + " += ("+ diffInAnglesDeg + ") angleDeg " + angleDeg + "- prevAngleDegrees" + doubleSpinner.prevAngleDegrees + "= " + (doubleSpinner.culmAngleDegrees+diffInAnglesDeg));

        if (Math.abs(Math.round(diffInAnglesDeg / 360f)) == 1) {
            //Log.i(MY_TAG, "add 360 to angleDeg then work out diff (relative diff) -------------------------------");

            //add 360 to angleDeg then work out diff (relative diff)
            angleDeg += 360;
            diffInAnglesDeg = angleDeg - doubleSpinner.prevAngleDegrees;
        }


        if (diffInAnglesDeg > 300) {
            //trying to go beyond (STOP at max -ve spin val)
            //move spinner back
            Log.i(MY_TAG, "Stop going max -ve spin val -------------------------------");

            pointOnOrbitCircle = getControllerPosition(startFakeTouchPoint, doubleSpinner.centreCircle);
            angleDeg = (float) Math.toDegrees(getAngle(pointOnOrbitCircle, doubleSpinner.centreCircle));
            diffInAnglesDeg = 0;
            doubleSpinner.culmAngleDegrees = 0;
            doubleSpinner.prevAngleDegrees = doubleSpinner.culmAngleDegrees;
        }
        else if (diffInAnglesDeg < -300 && doubleSpinner.culmAngleDegrees < 700) {
            Log.i(MY_TAG, " going through middle (allowed) -------------------------------");
            //going through middle (allowed)
            doubleSpinner.culmAngleDegrees += diffInAnglesDeg;
            doubleSpinner.prevAngleDegrees = angleDeg;
        }
        else if (diffInAnglesDeg < -300 && doubleSpinner.culmAngleDegrees > 700) {
            Log.i(MY_TAG, "Stop going max +ve spin val -------------------------------");
            //trying to go beyond (STOP at max +ve spin val)
            //move spinner back
            pointOnOrbitCircle = getControllerPosition(startFakeTouchPoint, doubleSpinner.centreCircle);
            angleDeg = (float) Math.toDegrees(getAngle(pointOnOrbitCircle, doubleSpinner.centreCircle));
            diffInAnglesDeg = 0;
            doubleSpinner.culmAngleDegrees = 720;
            doubleSpinner.prevAngleDegrees = doubleSpinner.culmAngleDegrees;
        }
        else { //normal - just adjust value
            doubleSpinner.culmAngleDegrees += diffInAnglesDeg;
            doubleSpinner.prevAngleDegrees = angleDeg;

            if (diffInAnglesDeg == 0) {
                Log.i(MY_EMPTY_TAG, "Zero diff");
            }
        }

        Log.i(MY_TAG, "culmAngleDegrees = " + doubleSpinner.culmAngleDegrees + " += ("+ diffInAnglesDeg + ") angleDeg " + angleDeg + "- prevAngleDegrees" + doubleSpinner.prevAngleDegrees + "= " + (doubleSpinner.culmAngleDegrees+diffInAnglesDeg));
        Log.i(MY_EMPTY_TAG, "culmAngleDegrees = " + doubleSpinner.culmAngleDegrees + " += ("+ diffInAnglesDeg + ") angleDeg " + angleDeg + "- prevAngleDegrees" + doubleSpinner.prevAngleDegrees + "= " + (doubleSpinner.culmAngleDegrees+diffInAnglesDeg));
        ///By this stage, culmAngleDegress will be correct
        return pointOnOrbitCircle;
    }

    private void spinnerControllerTouchEnded(View view, float rawX, float rawY) {
        Log.i(MY_TAG, "spinnerControllerTouchEnded");
        String tag = (String) view.getTag();

        PointXY touchPoint = new PointXY(rawX, rawY);
        PointXY circleCentre = new PointXY();

        switch (tag) {
            case (spinnerTag + "1"): //top Spinner Controller
                circleCentre = new PointXY(topSpinner.getX(), topSpinner.getY());
                break;
            case (spinnerTag + "2"): //bottom Spinner Controller
                circleCentre = new PointXY(btmSpinner.getX(), btmSpinner.getY());

                break;
            default:
                break;
        }
    }

    private void tableTouched() {
        Log.i(MY_TAG, "TableTouched");
        deselectAllBalls();
    }

    private void tableActiveAreaTouched() {
        Log.i(MY_TAG, "tableActiveAreaTouched");
        deselectAllBalls();
    }


    Routine currentRoutine = new Routine(-10);

    private void ballTouched(Object tag) {
//        Log.i(MY_TAG,"Ball touched: " + tag.toString());
        //getBall
        Ball ball = getBallFromTag(tag);

        //Return if ball is ball icon (user adding ball)
        if (ball.isActive() == false) {
            return;
        }


        if (currentState == State.Recording) {
           recordBall(ball);
        }
        else {
            //Do nothing if already selected
            if (isBallCurrentlySelected(ball)) {
                //Show circle
                selectedBallCircle.setColorFilter(getResources().getColor(R.color.ballSelected_COL));
                return;
            }


            if (tagOfSelectedBall != null) {
                //remove old circle
                deselectAllBalls();
            }
            selectBall(ball);

            Log.i(MY_TAG, "Selected ball is: " + tagOfSelectedBall);
        }


    }

    private void selectBall(Ball ball) {
        tagOfSelectedBall = ball.getBallImageTag().toString();

        ball.getBallImageView().setVisibility(View.VISIBLE);

        changeSpinnersForSelectedBall(ball);
        showSpinners();
        showBinIcon();
        makeCircleBehindSelectedBall(ball);
    }


    final Handler handler = new Handler();
    ///This flashes a circle behind ball to show recording
    private void selectBallRecord(Ball ball) {
        tagOfSelectedBall = ball.getBallImageTag().toString();
        makeCircleBehindSelectedBall(ball);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                deselectBallRecord();
            }
        }, 400);
    }

    //Actually keep track of all details and make a routine
    private void recordBall(Ball ball) {
        Log.i(MY_TAG, "Recording ball with tag: " + ball.getBallImageTag());
        Log.i(TAG_FIRE, "Recording ball with tag: " + ball.getBallImageTag());
        //Show we are recording
        selectBallRecord(ball);

        if (currentRoutine.containsBall(ball)) {
            //Duplicate ball as already in routine
            Ball dupBall = new Ball(ball);

            try {
                //get ball of same ID that we made last
                Ball lastDuplicatedBallWithID = currentRoutine.getBallWithMaxDupIdFromID(ball.getId());

                //Make copy of this ball
                dupBall = new Ball(lastDuplicatedBallWithID);
            }
            catch (NullPointerException err) {
                //Do nothing as  // Ball dupBall = new Ball(ball); will work
            }
            //Set timer for dupBall ONLY
            dupBall.setTimeDelay(getTimerSeconds());
            Log.i(MY_TAG,"NEW DUP BALL TIMER DELAY = " + dupBall.getTimeDelay());

            //Add dup ball to routine (not first time)
            currentRoutine.addBall(dupBall);
        }
        else {
            //NEW BALL
            //Set timer
            ball.setTimeDelay(getTimerSeconds());
            Log.i(MY_TAG,"NEW NORMAL BALL TIMER DELAY = " + ball.getTimeDelay());

            //Add ball to routine (first time)
            currentRoutine.addBall(ball);
        }




        printBall(ball,true,true);
    }




    private void deselectAllBalls() {
        removeCurrentSelectedCircle();
        hideSpinners();
        hideBinIcon();
    }

    private void deselectBallRecord() {
        removeCurrentSelectedCircle();
    }

    private void showBinIcon() {
        binButton.setVisibility(View.VISIBLE);
        binRing.setVisibility(View.VISIBLE);
    }

    private void hideBinIcon() {
        binButton.setVisibility(View.INVISIBLE);
        binRing.setVisibility(View.INVISIBLE);
    }

    private void showSTRButton() {
        sendToRobotButton.setVisibility(View.VISIBLE);
    }

    private void hideSTRButton() {
        sendToRobotButton.setVisibility(View.INVISIBLE);
    }

    private void showConfirmButton() {
        confirmButton.setVisibility(View.VISIBLE);
    }

    private void hideConfirmButton() {
        confirmButton.setVisibility((View.INVISIBLE));
    }

    private void showReturnButton() {
        returnButton.setVisibility(View.VISIBLE);
    }

    private void hideReturnButton() {
        returnButton.setVisibility(View.INVISIBLE);
    }

    private void showPauseButton() {
        pauseButton.setVisibility(View.VISIBLE);
    }

    private void hidePauseButton() {
        pauseButton.setVisibility(View.INVISIBLE);
    }

    private void showRecordButton() {
        recordButton.setVisibility(View.VISIBLE);
        recordRing.setVisibility(View.VISIBLE);
    }

    private void hideRecordButton() {
        recordButton.setVisibility(View.INVISIBLE);
        recordRing.setVisibility(View.INVISIBLE);
    }

    private void showStopButton() {
        stopButton.setVisibility(View.VISIBLE);
        stopRing.setVisibility(View.VISIBLE);
    }

    private void hideStopButton() {
        stopButton.setVisibility(View.INVISIBLE);
        stopRing.setVisibility(View.INVISIBLE);
    }

    private void showCalibrationAll() {
        calibrationUpArrow.setVisibility(View.VISIBLE);
        calibrationLeftArrow.setVisibility(View.VISIBLE);
        calibrationDownArrow.setVisibility(View.VISIBLE);
        calibrationRightArrow.setVisibility(View.VISIBLE);
        calibrationCircle.setVisibility(View.VISIBLE);
    }


    private void hideCalibrationAll() {
        calibrationUpArrow.setVisibility(View.INVISIBLE);
        calibrationLeftArrow.setVisibility(View.INVISIBLE);
        calibrationDownArrow.setVisibility(View.INVISIBLE);
        calibrationRightArrow.setVisibility(View.INVISIBLE);
        calibrationCircle.setVisibility((View.INVISIBLE));
        calibrationConfirm.setVisibility(View.INVISIBLE);
    }

    private void showCalibrationConfirm() {
        calibrationConfirm.setVisibility(View.VISIBLE);
    }

    private void hideCalibrationConfirm() {
        calibrationConfirm.setVisibility(View.INVISIBLE);
    }

    private void hideCalibrationCircle() {
        calibrationCircle.setVisibility((View.INVISIBLE));
    }

    private void showCalibrationCircle() {
        calibrationCircle.setVisibility((View.VISIBLE));
    }



    private void showSpinners() {
        topSpinner.showSpinner();
        btmSpinner.showSpinner();

    }


    private void hideSpinners() {
        topSpinner.hideSpinner();
        btmSpinner.hideSpinner();
    }

    //Does ball have a circle behind it (i.e selected)
    private boolean isBallCurrentlySelected(Ball ball) {
        return ball.getBallImageTag().toString() == tagOfSelectedBall;

    }

    private Ball getBallCurrentlySelected() throws NullPointerException {
        if (tagOfSelectedBall == null) {
            throw new NullPointerException();
        }

        try {
            return getBallFromTag(tagOfSelectedBall);
        } catch (NullPointerException err) {
            throw err;
        }
    }

    private void hideAllBalls() {
        for(Ball ball: balls) {
            ball.getBallImageView().setVisibility(View.INVISIBLE);
        }

    }

    private void showAllBalls() {
        for(Ball ball: balls) {
            ball.getBallImageView().setVisibility(View.VISIBLE);
        }
    }

    private void recordButtonTouched() {
        Log.i(MY_TAG, "recordButtonTouched");
        if (currentState!=State.Creating){
            CharSequence text = "Cannot currently record routine";
            Log.i(MY_TAG,text + "STATE = " + currentState.toString());
            showToastShort(text);
            return;
        }
        if (totalBallsActive == 0) {
            Log.i(MY_TAG, "No balls active, refusing to record");
            return;
        }
        startRecord();
        //toggleRecordStopPlayButton();
    }

    private void startRecord() {
        if (currentState == State.Recording) {
            return;
        }
        Log.i(MY_TAG, "startRecord");
        CHANGE_STATE(State.Recording);
        currentRoutine = new Routine(-10);

        CharSequence text = "Recording started";
        showToastShort(text);

        startTimer();
        deselectAllBalls();
    }

    private void stopRecord() {
        if (currentState != State.Recording) {
            return;
        }
        Log.i(MY_TAG, "stopRecord");
        ///Go to next state
        CHANGE_STATE(State.PlayOrSave);

        Context context = getApplicationContext();
        CharSequence text = "Recording stopped";
        showToastShort(text);
        stopTimer();
        showSTRButton();
        showConfirmButton();
        showReturnButton();
    }

    private void stopButtonTouched() {
        Log.i(MY_TAG, "stopButtonTouched");
        if(currentState == State.Creating) {
            spitfireController.stop();
        }
        else if (currentState == State.Recording) {
            stopRecord();
            CHANGE_STATE(State.PlayOrSave);
        }
    }

    private void binButtonTouched() {
        Log.i(MY_TAG, "binButtonTouched");

        if (tagOfSelectedBall != null) {
            try {
                Ball selBall = getBallCurrentlySelected();
                deleteBall(selBall);
                deselectAllBalls();
            } catch (NullPointerException err) {
                Log.i(MY_TAG, "ERROR tagOfSelectedBall (" + tagOfSelectedBall + ") Not found in ball set)");
            }
        }
    }

    private void sTRButtonTouched() {
        Log.i(MY_TAG, "sendToRobotButtonTouched");

        CHANGE_STATE(State.Playing);
        hideSTRButton();
        hideConfirmButton();
        hideReturnButton();
        showPauseButton();

        if(spitfireController.HAS_CALIBRATION_VALUES() == false) {
            spitfireController.SET_CALIBRATION(calibration);
        }
        spitfireController.playRoutine(currentRoutine);
    }

    ///Need to save current routine
    private void confirmButtonTouched() {
        Log.i(MY_TAG, "confirmButtonTouched - need to save routine");

        CHANGE_STATE(State.Creating);
        hideConfirmButton();
        hideSTRButton();
        hideReturnButton();

        //Set routine to save and now navigate to Routine Menu
        spitfireController.setRoutine(currentRoutine);
        //Navigate there

        ArrayList<Ball> balls = new ArrayList<>();
        balls = currentRoutine.getBalls();
        for (Ball ball : balls){
            ball.setBallImageView(null);
            ball.setBallHitBoxImageView(null);
        }

        Intent addRoutineIntent = new Intent(MainActivity.this, AddRoutineActivity.class);
        addRoutineIntent.putExtra("routine", currentRoutine);
        startActivity(addRoutineIntent);
    }

    private void returnButtonTouched() {
        Log.i(MY_TAG, "returnButtonTouched");
        CHANGE_STATE(State.Creating);

        hideReturnButton();
        hideSTRButton();
        hideConfirmButton();
    }

    private void pauseButtonTouched() {
        Log.i(MY_TAG, "pauseButtonTouched");
        CHANGE_STATE(State.PlayOrSave);

        hidePauseButton();
        showSTRButton();
        showConfirmButton();
        showReturnButton();

        spitfireController.stop();
    }


    ///UP/DOWN is first value (Dist)   LEFT/RIGHT is second (DIR)
    private List<Integer> calibration = new ArrayList<>(Arrays.asList(0,0));
    private int upDownCaliValue = 1;
    private int LRCaliValue = 2;

    private void calibrationUpArrowTouched() {
        Log.i(MY_TAG, "calibration up arrow touched");
        if(calibrationConfirm.getVisibility()==View.VISIBLE) {
            hideCalibrationConfirm();
            showCalibrationCircle();
        }
        testBallImageView.setY(testBallImageView.getY() - 25f);
        calibration.set(0, calibration.get(0) + upDownCaliValue);
        updateSpitfireCalibration();
    }


    private void calibrationDownArrowTouched() {
        Log.i(MY_TAG, "calibration down arrow touched");
        if(calibrationConfirm.getVisibility()==View.VISIBLE) {
            hideCalibrationConfirm();
            showCalibrationCircle();
        }
        testBallImageView.setY(testBallImageView.getY() + 25f);
        calibration.set(0, calibration.get(0) - upDownCaliValue);
        updateSpitfireCalibration();
    }


    private void calibrationLeftArrowTouched() {
        Log.i(MY_TAG, "calibration left arrow touched");
        if(calibrationConfirm.getVisibility()==View.VISIBLE) {
            hideCalibrationConfirm();
            showCalibrationCircle();
        }
        testBallImageView.setX(testBallImageView.getX() - 25f);
        calibration.set(1, calibration.get(1) - LRCaliValue);
        updateSpitfireCalibration();
    }


    private void calibrationRightArrowTouched() {
        Log.i(MY_TAG, "calibration right arrow touched");
        if(calibrationConfirm.getVisibility()==View.VISIBLE) {
            hideCalibrationConfirm();
            showCalibrationCircle();
        }
        testBallImageView.setX(testBallImageView.getX() + 25f);
        calibration.set(1, calibration.get(1) + LRCaliValue);
        updateSpitfireCalibration();
    }

    //first touch - should fire
    private void calibrationCircleTouched() {
        Log.i(MY_TAG, "calibrationCircleTouched once");
        CharSequence text = "Once ball hits target: \nTap target again to exit calibration.";
        showToastLong(text);

        hideCalibrationCircle();
        showCalibrationConfirm();

        Routine caliRoutine = new Routine(-900);
        //Shoots at 131, 90, 100, 88, 0, 1200
        String caliStr = "131,90,100,88,0,1200";
        Ball caliBall = new Ball(131, 90, 4, calibrationCircle.getX() + calibrationCircle.getWidth()/2f, calibrationCircle.getY() + calibrationCircle.getHeight()/2f,-99,true);
        caliRoutine.addBall(caliBall);

        HelperFunctions.assertion(spitfireController.hasSetScreenConsts,"Need to set ScreenConsts");
        updateSpitfireCalibration();
        caliRoutine.setPhysicsCalculator(spitfireController.getPhysicsCalculator());
        String printString = caliRoutine.toString(true,true);
        Log.i(MY_TAG,"============================caliBall = " + printString);

        spitfireController.playRoutine(caliRoutine);
    }


    private void updateSpitfireCalibration() {
        spitfireController.SET_CALIBRATION(calibration);
        Log.i(MY_TAG,"calibration = ( " + calibration.get(0) + ", " + calibration.get(1) + " )");
    }

    private void calibrationConfirmed() {
        CHANGE_STATE(State.Creating);
        Log.i(MY_TAG, "calibration confirmed");
        hideCalibrationAll();
        testBallImageView.setVisibility(View.INVISIBLE);
        testBallImageView=null;
        showAllBalls();
        showRecordButton();
        showStopButton();

        //Shoots at 131, 90, 100, 88, 0, 1200
        updateSpitfireCalibration();
        spitfireController.stop();
    }

    private void saveButtonTouched() {
        Log.i(MY_TAG, "saveButtonTouched");
    }

    private void startTimer() {
        timer.resetTimer();
    }

    private void stopTimer() {
        timer.stopTimer();
    }

    private double getTimerSeconds() {
        return timer.elapsedTimeSeconds();
    }

    private void displayRoutine(Routine routine){
        Log.i(MY_EMPTY_TAG, "Loaded routine");


        ArrayList<Ball> myballs = routine.getBalls();
        ArrayList<Integer> createdballs = new ArrayList<>();

        for (Ball ball : myballs){
            int id = ball.getId();
            Log.i(MY_EMPTY_TAG, "BALL ID FOUND:" + id);
            if (createdballs.contains(id)) { continue; }

            createdballs.add(id);   //keep track of which balls are created
            makeNewBall();          //create imageviews for the balls

            Ball newBall = makeBallWithXY(ball.getX(),ball.getY(),id);
            Log.i(MY_EMPTY_TAG, "ball.getX()  " + (ball.getX()) );
            Log.i(MY_EMPTY_TAG, "ball.getY()  " + (ball.getY()) );

            newBall.setTimeDelay(ball.getTimeDelay());
            newBall.setTopWheel(ball.getTopWheel());
            newBall.setBottomWheel(ball.getBottomWheel());

            newBall.setBallActive();
            newBall.setTopTextVal(ball.getTopTextVal());
            newBall.setBtmTextVal(ball.getBtmTextVal());
            newBall.setSpinHandlePosTopWheel(ball.getSpinHandlePosTopWheel());
            newBall.setSpinHandlePosBottomWheel(ball.getSpinHandlePosBottomWheel());
            newBall.setDupID(ball.getDupID());

            ball = newBall;
        }

        showAllBalls();
    }

    private void showToastShort(CharSequence text) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    private void showToastLong(CharSequence text) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
        toast.show();
        toast.show();
    }
}