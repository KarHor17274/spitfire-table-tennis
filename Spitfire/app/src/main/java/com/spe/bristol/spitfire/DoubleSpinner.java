package com.spe.bristol.spitfire;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.security.acl.LastOwnerException;


public class DoubleSpinner {

    private CircleProgressBar progClockwise, progAnticlockwise;
    private float scaleFactor;
    public ImageView circleBehind, spinnerController, spinnerControllerTouchArea;
    public TextView textView;
    private int id;

    public float culmAngleDegrees = 0;
    public float prevAngleDegrees = 0;
    public PointXY centreCircle;


    private String MY_TAG = "DoubleSpinner";

    public DoubleSpinner(CircleProgressBar progClockwise, CircleProgressBar progAnticlockwise, float progScale, int id) {
        this.progClockwise = progClockwise;
        this.progAnticlockwise = progAnticlockwise;
        this.progClockwise.isClockwise = true;
        scaleFactor = progScale;
        this.id = id;
        setupCircleProgBars(progScale);
    }


    //40 - 100 - 160
    ///Turns a value from 0 - 360 - 720  to actual spin robot value (0-360 for -ve spins, 360-720 +ve)
    public int getSpinnerMappedValue() {
        //culmAngleDegrees is the value from 0 - 720
        int mappedValue = (int) culmAngleDegrees;

        int minValue = 40;
        int midValue = 100;
        int maxValue = 160;

//        int lowerRangeValue = midValue-minValue;
//        int upperRangeValue = maxValue-midValue;

        //HelperFunctions.assertion(rangeValue == maxValue-midValue, "Uneven ranges are allowed but need two range variable");
        mappedValue = (int) HelperFunctions.map(culmAngleDegrees,0, 720, minValue, maxValue);

//        //changing display value from 0 to 360 to  40 to 100
//        if(culmAngleDegrees >= 0 && culmAngleDegrees <= 360){
//            mappedValue = minValue + (int)(culmAngleDegrees / (360/lowerRangeValue));
//        }
//        else if(culmAngleDegrees > 360 && culmAngleDegrees <= 720) {
//            //-360 then change display value from 0 to 360  to   100 to 160
//            mappedValue = (int) (midValue + (culmAngleDegrees - 360) /  (360/upperRangeValue) );
//        }
        return mappedValue;
    }


    ///Turns a value from 40 -100 - 160 to 0 - 360 - 720
    public int getReverseSpinnerMappedValue(int input) {
        int minValue = 40;
        int maxValue = 160;
        int mappedValue = (int) HelperFunctions.map(input,minValue, maxValue, 0, 720);
        return mappedValue;
    }



    public int getId() {
        return id;
    }

    public void hideSpinner() {
        hideViewIfNotNull(progClockwise);
        hideViewIfNotNull(progAnticlockwise);
        hideViewIfNotNull(circleBehind);
        hideViewIfNotNull(textView);
        hideViewIfNotNull(spinnerController);
        hideViewIfNotNull(spinnerControllerTouchArea);

    }

    private void hideViewIfNotNull(View view) {
        if(view != null) {
            view.setVisibility(View.INVISIBLE);
        }
    }

    private void showViewIfNotNull(View view) {
        if(view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }


    public void showSpinner() {
        showViewIfNotNull(progAnticlockwise);
        showViewIfNotNull(progClockwise);
        showViewIfNotNull(circleBehind);
        showViewIfNotNull(spinnerController);
        showViewIfNotNull(textView);
        showViewIfNotNull(spinnerControllerTouchArea);
    }


    private void setProgClockwiseValue(int value) {
        progClockwise.setProgress((float) value);
    }

    private void setProgAntiClockwiseValue(int value) {
        progAnticlockwise.setProgress((float) value);
    }

    public int getProgHeight() {
        return progClockwise.getMeasuredHeight();
    }
    public int getProgWidth() {
        return progClockwise.getMeasuredHeight();
    }

    public float getProgY() {
        return progClockwise.getY();
    }
    public float getProgX() {
        return progClockwise.getX();
    }


    ///Takes value from 0 to 720
    public void setProgress(float value) {
        int val = (int) value;

        if (val >= 0 && val < 360) {
            int perc = (int) HelperFunctions.map(value,360,0,0,100);//((value-360) / 360 * 100);
//            int perc = (int) (value / 360 * 100);
            progAnticlockwise.setVisibility(View.INVISIBLE);
            progClockwise.setVisibility(View.VISIBLE);
            setProgAntiClockwiseValue(0);
            setProgClockwiseValue(perc);

        }
        else if (val >= 360 && val < 720) { //+ve
            int perc = (int) HelperFunctions.map(value,360,720,0,100);//((value-360) / 360 * 100);
            progAnticlockwise.setVisibility(View.VISIBLE);
            progClockwise.setVisibility(View.INVISIBLE);
            setProgAntiClockwiseValue(perc);
            setProgClockwiseValue(0);

        }
    }

    //    public void setCircleBehind(ImageView circleBehind) {
//        this.circleBehind = circleBehind;
//    }
//
//    public void setSpinnerController(ImageView spinnerController) {
//        this.spinnerController = spinnerController;
//    }

    private void setupCircleProgBars(float progScale) {
        progClockwise.setStrokeWidth(progScale * 30);
        progClockwise.setScaleX(progScale);
        progClockwise.setScaleY(progScale);
        progClockwise.setY(200);


        progAnticlockwise.setStrokeWidth(20f);
        progAnticlockwise.setScaleX(progScale);
        progAnticlockwise.setScaleY(progScale);
        progAnticlockwise.setY(200);

        progAnticlockwise.setVisibility(View.INVISIBLE);
    }

    public void setX(float x) {
        progAnticlockwise.setX(x);
        progClockwise.setX(x);
    }

    public void setY(float x) {
        progAnticlockwise.setY(x);
        progClockwise.setY(x);
    }

    public void setXY(float x, float y) {
        setX(x);
        setY(y);
    }

//    public int getWidthOfBackingCircle() {
//        return circleBehind.getWidth();
//    }
//
//    public int getMeasuredWidthOfBackingCircle() {
//        return circleBehind.getMeasuredWidth();
//    }


    public int getWidth() {
        return progClockwise.getWidth();
    }
    public int getMeasuredWidth() {
        return progClockwise.getMeasuredWidth();
    }

    public int getScaledWidth() {
        return (int) (scaleFactor * progClockwise.getWidth());
    }
    public int getScaledMeasuredWidth() {
        return (int) (scaleFactor * progClockwise.getMeasuredWidth());
    }

    public float getX() {
        return progClockwise.getX();
    }

    public float getY() {
        return progClockwise.getY();
    }
}




///*
//    <!--
//      ProgressBar 1 + Seek Bar (top spinner)
//      -->
//        <ProgressBar
//            android:id="@+id/topSpinner1"
//            style="?android:progressBarStyleHorizontal"
//            android:layout_width="150dp"
//            android:layout_height="150dp"
//            android:layout_marginStart="8dp"
//            android:layout_marginTop="8dp"
//            android:layout_marginEnd="8dp"
//            android:progress="0"
//            android:progressDrawable="@drawable/ring_spinner"
//            app:layout_constraintEnd_toEndOf="parent"
//            app:layout_constraintStart_toStartOf="parent"
//            app:layout_constraintTop_toTopOf="parent" />
//
//        <ProgressBar
//            android:id="@+id/topSpinner2"
//            style="?android:progressBarStyleHorizontal"
//            android:layout_width="150dp"
//            android:layout_height="150dp"
//            android:layout_marginStart="8dp"
//            android:layout_marginTop="8dp"
//            android:layout_marginEnd="8dp"
//            android:scaleX="-1"
//            android:rotation="180"
//            android:progress="0"
//            android:progressDrawable="@drawable/ring_spinner"
//            app:layout_constraintEnd_toEndOf="parent"
//            app:layout_constraintStart_toStartOf="parent"
//            app:layout_constraintTop_toTopOf="parent" />
//
//
//        <SeekBar
//            android:id="@+id/topSpinnerSeekBar"
//            android:layout_width="fill_parent"
//            android:layout_height="wrap_content"
//            android:layout_marginStart="16dp"
//            android:layout_marginTop="116dp"
//            android:progress="50"
//            app:layout_constraintStart_toStartOf="parent"
//            app:layout_constraintTop_toTopOf="parent" />
//
//        <TextView
//            android:id="@+id/topSpinnerText"
//            android:layout_width="match_parent"
//            android:layout_height="wrap_content"
//            android:layout_marginTop="56dp"
//            android:gravity="center"
//            android:text="@string/SpinText"
//            app:layout_constraintEnd_toEndOf="parent"
//            app:layout_constraintTop_toTopOf="@id/VerticalLineView" />
//
//        <!--
// */
//public class DoubleSpinner extends ViewGroup {
//
//    private SeekBar seekBar;
//    private TextView text;
//    private ProgressBar progressClockwise, progressAnticlockwise;
////    private ViewGroup.LayoutParams params;
//
////    private View spinnerView;
////    private Color colorClockwise, colorAnticlockwise;
////    int zPos_text, zPos_spinner;
//
//    private Activity view;
//    private static String MY_TAG = "DoubleSpinner";
//    private float sizeOfProgCircle;
//    private float sizeOfTextBox;
//    private int leftPadding, topPadding;
//    private float totalWidth, totalHeight;
////    private View getView() { return spinnerView; }
//
//    public DoubleSpinner(Context context, Activity activity, float sizeOfProgCircle, int leftPadding, int topPadding, float totalWidth, float totalHeight) {
//        super(context);
//
//        this.sizeOfProgCircle = sizeOfProgCircle;
//        this.totalHeight = totalHeight - topPadding;
//        this.totalWidth = totalWidth - leftPadding;
//
//        this.view = activity;
//        this.leftPadding = leftPadding;
//        this.topPadding = topPadding;
//
//
//        sizeOfTextBox = 0.33f * sizeOfProgCircle;
//        Log.i(MY_TAG, "Padding = left = " + leftPadding + ", top= " + topPadding);
//        measure(MeasureSpec.EXACTLY,MeasureSpec.EXACTLY);
//        //params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//
//        makeTopSeekBarAndText(context);
//        makeProgressBars(context);
//        positionText();
//
//        setColorTop(R.color.my_green_light, true);
//        setBackgroundColor(Color.parseColor("#33b5d6e1"));
//    }
//
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
//        int mode = MeasureSpec.getMode(widthMeasureSpec); // mode == View.MesaureSpec.EXACTLY
//        int sizeW = MeasureSpec.getSize(widthMeasureSpec); // size == 400
//        int sizeH = MeasureSpec.getSize(heightMeasureSpec); //
//
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
////        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
////                                    getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
////        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
//
//        for(int i=0; i<getChildCount(); i++) {
//            View v = getChildAt(i);
//            int wspec = MeasureSpec.makeMeasureSpec(sizeW, mode);
//            int hspec = MeasureSpec.makeMeasureSpec(sizeH, mode);
//
//            if (v == progressClockwise || v == progressAnticlockwise) {
//                continue;
//            }
//            else if (v == seekBar) {
//                continue;
//            }
//            else if (v == text) {
//                int width = (int) sizeOfTextBox;
//                int height = (int) sizeOfTextBox;
//                wspec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST);
//                hspec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST);
//            }
//            else {
//                continue;
////                int width = 100;
////                int height = 100;
////                wspec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST);
////                hspec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST);
//            }
//            v.measure(wspec, hspec);
//        }
//
//    }
//
//    Rect rectForReuse = new Rect();
//    @Override
//    protected void onLayout(boolean changed, int l, int t, int r, int b) {
////        int itemWidth = (r - l);
////
////        for (int i = 0; i < this.getChildCount(); i++) {
////            View v = getChildAt(i);
////            Log.i(MY_TAG, "onLayout --> Child:" + v.getTag() + "  Parent size = left: " + l + " top:" + t + " right: " + r + " bottom:" + b);
//////            Log.i(MY_TAG, "getWidth()/2 = " + getWidth()/2 + " r = " + (r));
////
////            //specify X and Y coord, then make rect as big as needs
////            if (v == progressClockwise || v == progressAnticlockwise) {
////                Log.i(MY_TAG, "skip layout of progbars");
////                continue;
////            }
////            else if (v == seekBar) {
////                continue;
//////                v.layout(0, 0, getWidth(), b - t);
////            }
////            else if (v == text) {
//////                v.layout(itemWidth * i, 0, (i + 1) * itemWidth, b - t);
////            }
////            else {
//////                v.layout(itemWidth * i, 0, (i + 1) * itemWidth, b - t);
////            }
////        }
//    }
//
//
//
//
//
//    private void setColorTop(int colorId, boolean forClockwiseSpinner) {
//        ColorFilter filter = new LightingColorFilter(Color.parseColor("#FF000000"), ResourcesCompat.getColor(view.getResources(),colorId,null));
//
//        Drawable progressCircle;
//
//        if (forClockwiseSpinner) {
//            progressCircle  = progressClockwise.getProgressDrawable();
//        }
//        else {
//            progressCircle  = progressAnticlockwise.getProgressDrawable();
//        }
//
//        try {
//            progressCircle.setColorFilter(filter);
//        } catch (NullPointerException err) {
//            //TODO: Handle this
//        }
//    }
//
//
//    public void setProgressBarWidth(int width) {
//        progressClockwise.getLayoutParams().width = width;
//        progressAnticlockwise.getLayoutParams().width = width;
//    }
//
//    public void setProgressBarHeight(int height) {
//        progressClockwise.getLayoutParams().height = height;
//        progressAnticlockwise.getLayoutParams().height = height;
//    }
//
//    private void measureProgressBars() {
//        progressClockwise.measure(MeasureSpec.AT_MOST,MeasureSpec.AT_MOST);
//        progressAnticlockwise.measure(MeasureSpec.AT_MOST,MeasureSpec.AT_MOST);
//    }
//
//    private void layoutProgressBars() {
//        int left =  (int) (totalWidth/2f - sizeOfProgCircle/2f);
//        int top = (int) ((totalHeight * 0.4f)*0.15f - sizeOfProgCircle/2f + sizeOfProgCircle/2f);
//        int right = left + (int)  sizeOfProgCircle;
//        int bottom = top + (int) sizeOfProgCircle;
//        //TODO: Work out why Total height is too big
//
//        progressClockwise.layout(left,top,right, bottom);
//        progressAnticlockwise.layout(left,top,right, bottom);
////
////        progressClockwise.layout(0,0,(int) sizeOfProgCircle,(int) sizeOfProgCircle);
////        progressAnticlockwise.layout(0,0,(int) sizeOfProgCircle,(int) sizeOfProgCircle);
////        text.layout(left,top,left + 20,top + 20);
//    }
//
//
//    public void scaleProgBars(float size) {
//        float scale = 1.0f;
//        progressAnticlockwise.setScaleX(-scale);
//        progressAnticlockwise.setScaleY(scale);
//        progressClockwise.setScaleY(scale);
//        progressClockwise.setScaleX(scale);
//
//        sizeOfProgCircle *= size;
//        measureProgressBars();
//        layoutProgressBars();
//    }
//
//
//
//
//
//    //makes two progressBars (one clockwise and one anticlockwise)
//    private void makeProgressBars(Context context) {
//        /*
//        style="?android:progressBarStyleHorizontal"
//            android:layout_width="150dp"
//            android:layout_height="150dp"
//            app:layout_constraintEnd_toEndOf="parent"
//            app:layout_constraintStart_toStartOf="parent"
//            app:layout_constraintTop_toTopOf="parent"
//         */
//        progressClockwise = new ProgressBar(context,null, android.R.attr.progressBarStyleHorizontal); //view.findViewById(topSpinnerID1);
//        progressClockwise.setIndeterminate(false);
//        progressClockwise.setProgress(100);
//
//        Drawable reverseCircle = ResourcesCompat.getDrawable(view.getResources(), R.drawable.ring_spinner, null);
//        progressClockwise.setProgressDrawable(reverseCircle);
//
//
//
//        progressAnticlockwise = new ProgressBar(context,null, android.R.attr.progressBarStyleHorizontal); //view.findViewById(topSpinnerID2);
//        progressAnticlockwise.setIndeterminate(false);
//        progressAnticlockwise.setProgress(100);
//        progressAnticlockwise.setScaleX(-1);
//        progressAnticlockwise.setRotation(180);
//
//        Drawable forwardCircle = ResourcesCompat.getDrawable(view.getResources(), R.drawable.ring_spinner, null);
//        progressAnticlockwise.setProgressDrawable(forwardCircle);
//
//
//        progressClockwise.setTag("progressClockwise");
//        progressAnticlockwise.setTag("progressAnticlockwise");
//
//        setProgBarPos(0,0);
//
//        measureProgressBars();
//        layoutProgressBars();
//
////        ImageView ballImgView = new ImageView(context);
////        ballImgView.setImageResource(R.drawable.ball);
////        ballImgView.setX(0);
////        ballImgView.setY(0);
////        addView(ballImgView);
////        ballImgView.measure(MeasureSpec.AT_MOST,MeasureSpec.AT_MOST);
////        ballImgView.layout(0,0,300,300);
//
//        addView(progressClockwise);
//        addView(progressAnticlockwise);
//
////        setProgressBarHeight(150);
////        setProgressBarWidth(150);
//        text.setGravity(Gravity.CENTER_HORIZONTAL);
//
////        text.setGravity(Gravity.CENTER);
//        text.setTextColor(Color.rgb(0,0,0));
//    }
//
//    private void setProgBarPos(float x, float y) {
//        progressAnticlockwise.setX(x);
//        progressAnticlockwise.setY(y);
//        progressClockwise.setX(x);
//        progressClockwise.setY(y);
//    }
//
//    private void setSeekBarPos(float x, float y) {
//        seekBar.setX(x);
//        seekBar.setY(y);
//    }
//
//    private void setTextPos(float x, float y) {
//        text.setX(x);
//        text.setY(y);
//    }
//
//
//    private void positionText() {
////        progressClockwise.post(new Runnable() {
////            @Override
////            public void run() {
//                float progWidthHalf = progressClockwise.getWidth()/2f;
//                float progHeightHalf = progressClockwise.getHeight()/2f;
//
//                float textWidthHalf = text.getWidth() * 0.5f;
//                float textHeightHalf = text.getHeight() * 0.5f;
//
//                float textXOff= text.getWidth() * 0.15f;
//                float textYOff = text.getHeight() * 0.1f;
//
//                Log.i(MY_TAG,"progWidthHalf = " + progWidthHalf);
//                Log.i(MY_TAG,"textWidthHalf = " + textWidthHalf + " textHeightHalf =" + textHeightHalf);
//                setTextPos(progressClockwise.getX() + progWidthHalf - textWidthHalf + textXOff,progressClockwise.getY() + progHeightHalf - textHeightHalf + textYOff);
////            }
////        });
//    }
//
//
//    private void makeTopSeekBarAndText(Context context) {
//        //SeekBar
//        seekBar = new SeekBar(context);//view.findViewById(seekBarId);
//        text = new TextView(context); //view.findViewById(textId);
//        text.setTextSize(17);
//
//        String topText = seekBar.getProgress() + "";
//        text.setText(topText);
//
////        text.setLayoutParams(params);
////        seekBar.setLayoutParams(params);
//        seekBar.setTag("seekBar");
//        text.setTag("text");
//
//        setSeekBarPos(0,0);
//
//        text.measure(MeasureSpec.AT_MOST,MeasureSpec.AT_MOST);
//        seekBar.measure(MeasureSpec.AT_MOST,MeasureSpec.AT_MOST);
//
//        seekBar.layout(0,0,1000,100);
//        text.layout(0,0,(int) (sizeOfTextBox),(int) sizeOfTextBox);
//        addView(seekBar);
//        addView(text);
//
//        seekBar.setOnSeekBarChangeListener(
//                new SeekBar.OnSeekBarChangeListener() {
//                    int spinVal;
//
//                    @Override
//                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                        spinVal = progress;
//                        String message = "" + spinVal;
//                        text.setText(message);
//
//                        if (spinVal >= 50) { //top 50% update top Spinner
//                            progressClockwise.setVisibility(View.VISIBLE);
//                            progressClockwise.setProgress(convertSeekValToProgressVal(spinVal, true));
//                            progressAnticlockwise.setVisibility(View.INVISIBLE);
//                        } else {
//                            progressAnticlockwise.setVisibility(View.VISIBLE);
//                            progressAnticlockwise.setProgress(convertSeekValToProgressVal(spinVal, false));
//                            progressClockwise.setVisibility(View.INVISIBLE);
//                        }
//                    }
//
//                    @Override
//                    public void onStartTrackingTouch(SeekBar seekBar) {
//
//                    }
//
//                    @Override
//                    public void onStopTrackingTouch(SeekBar seekBar) {
//                        String message = "" + spinVal;
//                        text.setText(message);
//                    }
//                }
//        );
//    }
//
//
//    //maps values from Seekbar to progressBar --> 50% of seekBar is 0% of progress bar
//    private int convertSeekValToProgressVal(int seekVal, boolean isTopBar) {
//        //0 is -100%
//        // 50 is 0
//        //100 is 100%
//        int retVal;
//        boolean over50 = seekVal >= 50;
//        retVal = over50 ? (seekVal-50)*2 : (100 - seekVal*2);
//
//
//        //return 0 if asking for other bar
//        if (isTopBar && over50 == false) { return 0; } //TopSpinner where 50 = 0 & 100 = 100
//        else if (isTopBar == false && over50) { return 0; } //BottomSpinner where 0 = 100 &  50 = 0 (anticlockwise)
//
//        return retVal;
//    }
//
//    //Tests the conversion from the 0-100 scale of a normal seekBar to the double ended scale used to increase the two seekBars
//    private void testSeekBarConversion() {
//        for (int i = 0; i < 101; i++) {
//            Log.i(MY_TAG,"testSeekBarConversion, topBar: " + i + " --> " + convertSeekValToProgressVal(i, true));
//        }
//        for (int i = 0; i < 101; i++) {
//            Log.i(MY_TAG,"testSeekBarConversion, bottomBar: " + i + " --> " + convertSeekValToProgressVal(i, false));
//        }
//    }
//
//
//}
