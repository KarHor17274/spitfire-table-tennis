package com.spe.bristol.spitfire.RoutineHelper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spe.bristol.spitfire.R;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(String name,String colour, View view, int i);
    }

    private static final String TAG = "MyAdapter";
    private ArrayList<Integer> mRoutineIDs;
    private ArrayList<String> mRoutineNames;
    private ArrayList<String> mRoutineColours;
    private Context mContext;
    private final OnItemClickListener listener;

    public MyAdapter(ArrayList<Integer> mRoutineIDs,ArrayList<String> mRoutineNames,ArrayList<String> mRoutineColours, Context mContext,OnItemClickListener listener) {
        this.mRoutineIDs = mRoutineIDs;
        this.mRoutineNames = mRoutineNames;
        this.mContext = mContext;
        this.listener = listener;
        this.mRoutineColours = mRoutineColours;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView routineNum;
        public TextView routineName;
        public RelativeLayout parentLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            routineNum = itemView.findViewById(R.id.routine_number);
            routineName = itemView.findViewById(R.id.routine_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
        public void bind(String name,String colour,int i, final OnItemClickListener listener){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(name,colour, v, i);
                }
            });
        }
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);
        MyViewHolder holder =new MyViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.routineName.setText(mRoutineNames.get(position));
        myViewHolder.routineNum.setText(Integer.toString(mRoutineIDs.get(position)));
        //myViewHolder.parentLayout.setBackgroundResource(R.drawable.routineboxorange);
        Log.i(TAG,"colour: " + mRoutineColours.get(position));
        setBackgroundColour(myViewHolder,mRoutineColours.get(position));
        myViewHolder.bind(mRoutineNames.get(position),mRoutineColours.get(position),position,listener);
    }

    @Override
    public int getItemCount() {
        return mRoutineNames.size();
    }

    private void setBackgroundColour(MyViewHolder myViewHolder, String colour){
        Log.i(TAG,"colour: " + colour);
        switch (colour) {
            case "blue":
                Log.i(TAG,"if blue");
                myViewHolder.parentLayout.setBackgroundResource(R.drawable.routine_box);
                break;
            case "green":
                Log.i(TAG,"if green");
                myViewHolder.parentLayout.setBackgroundResource(R.drawable.routineboxgreen);
                break;
            case "brown":
                Log.i(TAG,"if brown");
                myViewHolder.parentLayout.setBackgroundResource(R.drawable.routineboxbrown);
                break;
            case "orange":
                Log.i(TAG,"if orange");
                myViewHolder.parentLayout.setBackgroundResource(R.drawable.routineboxorange);
                break;
        }
    }
}