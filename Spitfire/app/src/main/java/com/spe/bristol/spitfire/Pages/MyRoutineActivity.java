package com.spe.bristol.spitfire.Pages;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.spe.bristol.spitfire.R;
import com.spe.bristol.spitfire.RoutineHelper.Routine;
import com.spe.bristol.spitfire.RoutineHelper.DatabaseHelper;
import com.spe.bristol.spitfire.RoutineHelper.EditRoutineActivity;
import com.spe.bristol.spitfire.RoutineHelper.MyAdapter;
import com.spe.bristol.spitfire.SpitfireCommunication.SpitfireController;

import java.util.ArrayList;
import java.util.Objects;

public class MyRoutineActivity extends AppCompatActivity {

    public static final String TAG = "MyRoutineActivity";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private DrawerLayout mDrawerLayout;
    private android.support.v7.widget.Toolbar toolbar;
    private ArrayList<Integer> mRoutineIDs = new ArrayList<>();
    private ArrayList<String> mRoutineNames = new ArrayList<>();
    private ArrayList<String> mRoutineColours = new ArrayList<>();
    private SpitfireController spitfireController;
    private Routine routineToSave;
    DatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_routine);
        mDatabaseHelper = new DatabaseHelper(this);
        initRoutineData();
        initRecyclerView();

        setupDrawerLayout();
        makeActionBarToolbar();

        //Instantiate class to pass routines on to save in MyRoutineActivity
        spitfireController = (SpitfireController) getApplication();

        boolean savingRoutineNow = spitfireController.isRoutineToSave();
        if (savingRoutineNow) {
            routineToSave = spitfireController.getRoutine();
            //Log.i(TAG,"\n ---- routineToSave BELOW -----" + routineToSave.toString(true,false));
            //Log.i(TAG," ---- routineToSave ENDED -----");
        }

        setContentView(mDrawerLayout);
    }

    private void setupDrawerLayout() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        Log.i(TAG, ""+mDrawerLayout);
        mDrawerLayout.setBackgroundColor(Color.argb(255,50,50,50));

        //Nav View (side menu)
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(false);
                    // close drawer when item is tapped
                    mDrawerLayout.closeDrawers();

                    // Add code here to update the UI based on the item selected
                    // For example, swap UI fragments here

                    int id = menuItem.getItemId();
                    switch (id) {
                        case R.id.connect_page:
                            Intent r = new Intent(MyRoutineActivity.this,ConnectActivity.class);
                            startActivity(r);
                            break;
                        case R.id.main_page:
                            Intent i = new Intent(MyRoutineActivity.this, MainActivity.class);
                            startActivity(i);
                            break;
                        case R.id.about_page:
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setData(Uri.parse("http://www.spitfiretabletennis.com"));
                            startActivity(intent);
                            break;
                        case R.id.routines_page:
                            break;
                        case R.id.setting_page:
                            Intent s = new Intent(MyRoutineActivity.this, SettingsActivity.class);
                            startActivity(s);
                            break;
                        case R.id.help_page:
                            Intent f = new Intent(MyRoutineActivity.this, HelpActivity.class);
                            startActivity(f);
                            break;
                    }
                    return true;
                });

        ///Setup DrawerLayout
        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Respond when the drawer is opened
                    }
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Respond when the drawer is closed
                    }
                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void makeActionBarToolbar() {
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        Log.i(TAG,"toolBar value is " + toolbar);

        setSupportActionBar(toolbar);

        //Action Bar
        ActionBar actionbar = getSupportActionBar();
        try{
            Objects.requireNonNull(actionbar).setDisplayHomeAsUpEnabled(true);
        }catch (NullPointerException e){
            Log.e(TAG,e.getMessage());
        }
        Objects.requireNonNull(actionbar).setHomeAsUpIndicator(R.drawable.ic_menu_main);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setDisplayShowHomeEnabled(false);
        initRecyclerView();
        //RecyclerView mRecyclerView = findViewById(R.id.my_recycler_view);
        //populateListView();
    }

    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initRoutineData() {
        //access all data in database and transform into arraylist of each variable
        Cursor data = mDatabaseHelper.getData();
        ArrayList<Integer> routineIDs = new ArrayList<>();
        ArrayList<String> routineNames = new ArrayList<>();
        ArrayList<String> routineColours = new ArrayList<>();
        while(data.moveToNext()){
            routineIDs.add(data.getInt(0));
            routineNames.add(data.getString(1));
            routineColours.add(data.getString(2));
        }

        mRoutineIDs = routineIDs;
        mRoutineNames = routineNames;
        mRoutineColours = routineColours;
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.my_recycler_view);

        mAdapter = new MyAdapter(mRoutineIDs,mRoutineNames,mRoutineColours, this, new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String name,String colour, View view, int i) {
                //clicking on a item in the list will take you to the edit page
                Log.d(TAG, "onItemClick: You Clicked on " + name);
                Cursor data = mDatabaseHelper.getItemID(name);
                int itemID = -1;
                while(data.moveToNext()){
                    itemID = data.getInt(0);
                }
                if(itemID > -1){
                    Log.d(TAG, "onItemCLick: The ID is: " + itemID);
                    Intent editScreenIntent = new Intent(MyRoutineActivity.this, EditRoutineActivity.class);
                    editScreenIntent.putExtra("id", itemID);
                    editScreenIntent.putExtra("name", name);
                    editScreenIntent.putExtra("colour", colour);
                    startActivity(editScreenIntent);
                }
                else{
                    toastMessage("No ID associated with that name");
                }
            }
        });

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}