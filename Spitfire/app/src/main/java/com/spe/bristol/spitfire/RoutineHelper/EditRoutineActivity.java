package com.spe.bristol.spitfire.RoutineHelper;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spe.bristol.spitfire.Pages.MainActivity;
import com.spe.bristol.spitfire.Pages.MyRoutineActivity;
import com.spe.bristol.spitfire.R;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class EditRoutineActivity extends AppCompatActivity {

    private static final String TAG = "EditRoutineActivity";

    DatabaseHelper mDatabaseHelper;
    private Button btnUpdate, btnLoad, btnDelete, btnBlue, btnGreen,btnBrown, btnOrange;
    private EditText ETName, ETColour;
    private LinearLayout routineNameLinLayout;
    private TextView TVID;

    private String selectedName;
    private int selectedID;
    private String selectedcolour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_routine);

        ETName = (EditText) findViewById(R.id.editText_Name);
        btnOrange = (Button) findViewById(R.id.orangebutton2);
        TVID = (TextView) findViewById(R.id.IDtextView);
        routineNameLinLayout = (LinearLayout) findViewById(R.id.linlayouteditscreen);

        makeDeleteButton();
        makeUpdateButton();
        makeLoadButton();
        makeBlueButton();
        makeGreenButton();
        makeBrownButton();
        makeOrangeButton();

        mDatabaseHelper = new DatabaseHelper(this);

        Intent receivedIntent = getIntent();
        selectedID = receivedIntent.getIntExtra("id", -1);
        selectedName = receivedIntent.getStringExtra("name");

        selectedcolour = receivedIntent.getStringExtra("colour");
        Log.i(TAG, "colour: " + selectedcolour);

        TVID.setText(Integer.toString(selectedID));
        ETName.setText(selectedName);
        displaySelectedColour();
    }

    public void displaySelectedColour(){
        switch (selectedcolour) {
            case "blue":
                btnBlue.setBackground(getDrawable(R.drawable.bluecircleselected));
                routineNameLinLayout.setBackground(getDrawable(R.drawable.routine_box));
                break;
            case "green":
                btnGreen.setBackground(getDrawable(R.drawable.greencircleselected));
                routineNameLinLayout.setBackground(getDrawable(R.drawable.routineboxgreen));
                break;
            case "brown":
                btnBrown.setBackground(getDrawable(R.drawable.browncircleselected));
                routineNameLinLayout.setBackground(getDrawable(R.drawable.routineboxbrown));
                break;
            case "orange":
                btnOrange.setBackground(getDrawable(R.drawable.orangecircleselected));
                routineNameLinLayout.setBackground(getDrawable(R.drawable.routineboxorange));
                break;
        }
    }

    public void unselectColour(){
        switch (selectedcolour) {
            case "blue":
                btnBlue.setBackground(getDrawable(R.drawable.bluecircle));
                break;
            case "green":
                btnGreen.setBackground(getDrawable(R.drawable.greencircle));
                break;
            case "brown":
                btnBrown.setBackground(getDrawable(R.drawable.browncircle));
                break;
            case "orange":
                btnOrange.setBackground(getDrawable(R.drawable.orangecircle));
                break;
        }
    }

    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    private void makeDeleteButton(){
        btnDelete = (Button) findViewById(R.id.button_Delete);
        btnDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                mDatabaseHelper.deleteName(selectedID,selectedName);
                ETName.setText("");
                toastMessage("removed from database");

                //change to myRoutine activity
                Intent routinesScreenIntent = new Intent(EditRoutineActivity.this, MyRoutineActivity.class);
                startActivity(routinesScreenIntent);
            }
        });
    }

    private void makeUpdateButton(){
        btnUpdate = (Button) findViewById(R.id.button_Update);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = ETName.getText().toString();
                if(!item.equals("")){
                    mDatabaseHelper.updateNameandColour(item,selectedID,selectedName,selectedcolour);
                    Intent routinesScreenIntent = new Intent(EditRoutineActivity.this, MyRoutineActivity.class);
                    startActivity(routinesScreenIntent);
                }else{
                    toastMessage("Name cannot be blank");
                }
            }
        });
    }

    private void makeLoadButton(){
        btnLoad = (Button) findViewById(R.id.button_Load);

        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Cursor data = mDatabaseHelper.getRoutine(selectedID);
                byte[] routineAsBytes = new byte[]{};
                Routine routine = null;
                while(data.moveToNext()) {
                    routineAsBytes = data.getBlob(0);
                }
                try {
                    //get routine from database using serialisation
                    ByteArrayInputStream baip = new ByteArrayInputStream(routineAsBytes);
                    ObjectInputStream ois = new ObjectInputStream(baip);
                    routine = (Routine) ois.readObject();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                Intent mainActivityIntent = new Intent(EditRoutineActivity.this, MainActivity.class);
                mainActivityIntent.putExtra("routine", routine);
                startActivity(mainActivityIntent);
            }
        });
    }

    private void makeBlueButton(){
        btnBlue = (Button) findViewById(R.id.bluebutton2);
        btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"blue button pressed");
                unselectColour();
                selectedcolour = "blue";
                displaySelectedColour();
            }
        });
    }

    private void makeGreenButton(){
        btnGreen = (Button) findViewById(R.id.greenbutton2);
        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"green button pressed");
                unselectColour();
                selectedcolour = "green";
                displaySelectedColour();
            }
        });
    }

    private void makeBrownButton(){
        btnBrown = (Button) findViewById(R.id.brownbutton2);
        btnBrown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"brown button pressed");
                unselectColour();
                selectedcolour = "brown";
                displaySelectedColour();
            }
        });
    }

    private  void makeOrangeButton(){
        btnOrange = (Button) findViewById(R.id.orangebutton2);
        btnOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"Orange button pressed");
                unselectColour();
                selectedcolour = "orange";
                displaySelectedColour();
            }
        });
    }
}
