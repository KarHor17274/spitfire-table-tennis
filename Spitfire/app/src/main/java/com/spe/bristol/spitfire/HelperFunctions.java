package com.spe.bristol.spitfire;

public abstract class HelperFunctions {

    public static void assertion(boolean bool, String message) {
        if (!bool) throw new AssertionError("ASSERTION MESSAGE: " + message);

    }

    public static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }

    public static String padLeft(String inputString, int length, char withChar) {
        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length - inputString.length()) {
            sb.append(withChar);
        }
        sb.append(inputString);

        return sb.toString();
    }

    ///Mapping the range of (lower1 to upper1) to the range of (lower2 to upper2)
    //Returns value of inputNum mapped to second range
    public static double map(double inputNum, double lower1, double upper1, double lower2, double upper2){
        return (inputNum == lower1) ? lower2 : (inputNum - lower1) * (upper2 - lower2) / (upper1 - lower1) + lower2;
    }
}


