package com.spe.bristol.spitfire.Pages;

import android.os.SystemClock;
import android.util.Log;

public class MyTimer {
    private String MY_TAG = "myTimer";
    private long startTime ;
    private long endTime ;
    private boolean relativeMode = false;
    MyTimer(boolean relativeMode) {
        this.relativeMode = relativeMode;
        startTimer();
    }

    private void startTimer() {
        setStartTime();
        Log.i(MY_TAG, "startTimer " + startTime);
    }

    public void stopTimer() {
        setEndTime();
        Log.i(MY_TAG, "endTimer " + endTime);
    }

    private void setEndTime() {
        endTime = SystemClock.elapsedRealtime();
    }

    private void setStartTime() {
        startTime = SystemClock.elapsedRealtime();
    }

    public double elapsedTimeSeconds() {

        double elapsedSeconds = elapsedTimeMilliseconds() / 1000.0;
        return elapsedSeconds;
    }

    public long elapsedTimeMilliseconds() {
        setEndTime();
        long elapsedMilliSeconds = endTime - startTime;
        if(relativeMode) {
            setStartTime();
        }
        return  elapsedMilliSeconds;
    }

    public void resetTimer() {
        startTimer();
    }
}
