package com.spe.bristol.spitfire;

public enum State {
    Creating, Recording, PlayOrSave, Calibrating, Playing;

    @Override
    public String toString() {
        switch (this) {
            case Creating: return "Creating";
            case Recording: return "Recording";
            case PlayOrSave: return "PlayOrSave";
            case Calibrating: return "Calibrating";
            case Playing:return "Playing";
            default:return "UNKNOWN STATE";
        }
    }
}
