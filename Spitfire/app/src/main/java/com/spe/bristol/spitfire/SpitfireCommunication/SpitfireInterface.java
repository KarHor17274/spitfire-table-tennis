package com.spe.bristol.spitfire.SpitfireCommunication;

import com.spe.bristol.spitfire.RoutineHelper.Routine;

public interface SpitfireInterface {

    void playRoutine(Routine routine);
    void stop();
    void connect();

}
