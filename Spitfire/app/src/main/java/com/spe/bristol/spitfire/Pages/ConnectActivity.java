package com.spe.bristol.spitfire.Pages;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.spe.bristol.spitfire.R;
import com.spe.bristol.spitfire.SpitfireCommunication.SpitfireController;

import java.util.Objects;

public class ConnectActivity extends AppCompatActivity {
    public static final String MY_TAG = "ConnectActMessage:";
    private DrawerLayout mDrawerLayout;
    private Button connectButton;
    SpitfireController spitfireController;

    //this variable keeps track of the 3 connectionStatus,st
    //0 =  connectionStatus : paired but not connected
    //1 =  connectionStatus : paired and connected
    //2 =  connectionStatus : paired but disconnected
    public static int connectionStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getApplicationContext().setTheme(R.style.customisedTheme);
        Log.i(MY_TAG,"onCreate");

        //Set the UI of the activity by linking to res/layout/activity_connect.xml
        setContentView(R.layout.activity_connect);

        setupDrawerLayout();

        //Replacing original ActionBar with Toolbar for compatibility of different android sdk
        makeActionBarToolbar();

        //Variable to hold the "connect button" in the activity_connect.xml UI
        connectButton = findViewById(R.id.connect_button);

        spitfireController = new SpitfireController(this,this);

        IntentFilter btIntentConnected = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        registerReceiver(BTReceiver,btIntentConnected);
        IntentFilter btIntentDisconnected = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(BTReceiver,btIntentDisconnected);

        //Click listener to respond to the click
        connectButton.setOnClickListener(v -> {
            Log.d(MY_TAG, "onClick : Enabling/Disabling bluetooth = " );
            onConnectButtonPressed();
        });

        setContentView(mDrawerLayout);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(MY_TAG,"onStart");
        Log.i(MY_TAG,"checkConnected is " + connectionStatus);
        setConnectButton();
    }

    private void setConnectButton(){
        if(connectionStatus == 1){
            connectButton.setTextColor(Color.GREEN);
            connectButton.setText(getString(R.string.connection_status_1));
        }
        else if(connectionStatus == 2){
            connectButton.setTextColor(Color.RED);
            connectButton.setText(getString(R.string.connection_status_2));
        }
    }

    //The BroadcastReceiver that listens for bluetooth broadcasts
    public final BroadcastReceiver BTReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                //Do something if connected
                connectionStatus = 1;
                setConnectButton();
                Toast.makeText(context, "Bluetooth Connected", Toast.LENGTH_SHORT).show();
                goToPageOfClass(MainActivity.class);

                Log.d(MY_TAG, "BTReceiver: connected");
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                //Do something if disconnected
                connectionStatus = 2;
                setConnectButton();
                Toast.makeText(context, "Bluetooth Disconnected", Toast.LENGTH_SHORT).show();
                Log.d(MY_TAG, "BTReceiver: disconnected");
            }
        }
    };


    private void setupDrawerLayout() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        Log.i(MY_TAG, "" + mDrawerLayout);
        mDrawerLayout.setBackgroundColor(Color.argb(255,50,50,50));

        //Nav View (side menu)
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(false);
                    // close drawer when item is tapped
                    mDrawerLayout.closeDrawers();

                    // Add code here to update the UI based on the item selected
                    // For example, swap UI fragments here
                    int id = menuItem.getItemId();
                    switch (id) {
                        case R.id.connect_page:
                            break;
                        case R.id.main_page:
                            goToPageOfClass(MainActivity.class);
                            break;
                        case R.id.about_page:
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setData(Uri.parse("http://www.spitfiretabletennis.com"));
                            startActivity(intent);
                            break;
                        case R.id.routines_page:
                            goToPageOfClass(MyRoutineActivity.class);
                            break;
                        case R.id.setting_page:
                            goToPageOfClass(SettingsActivity.class);
                            break;
                        case R.id.help_page:
                           goToPageOfClass(HelpActivity.class);
                    }
                    return true;
                });

        ///Setup DrawerLayout
        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Respond when the drawer is opened
                    }
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Respond when the drawer is closed
                    }
                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void makeActionBarToolbar() {
        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        Log.i(MY_TAG,"toolBar value is " + toolbar);

        setSupportActionBar(toolbar);

        //Action Bar
        ActionBar actionbar = getSupportActionBar();
        try{
            Objects.requireNonNull(actionbar).setDisplayHomeAsUpEnabled(true);
        }catch (NullPointerException e){
            Log.e(MY_TAG,e.getMessage());
        }
        Objects.requireNonNull(actionbar).setHomeAsUpIndicator(R.drawable.ic_menu_connect);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setDisplayShowHomeEnabled(false);
    }

    //activity switching mechanism
    private void goToPageOfClass(Class cls) {
        Intent r = new Intent(ConnectActivity.this, cls);
        startActivity(r);
    }

    //function induced upon clicking the connect button
    protected void onConnectButtonPressed(){
        //instantiating SpitfireController to call connect() function
        if(isEmulator()){
            goToPageOfClass(MainActivity.class);
        }
        else {
            Log.d(MY_TAG, "connect() function called");
            if(connectionStatus == 0 || connectionStatus == 2){
                spitfireController.connect();
                connectButton.setText(getString(R.string.connection_status_connecting));
            }
        }
    }

    //returns true if runs on emulator,returns false if runs on real device
    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }
}
