package com.spe.bristol.spitfire.SpitfireCommunication;

import android.util.Log;

import com.spe.bristol.spitfire.Ball;
import com.spe.bristol.spitfire.HelperFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PhysicsCalculator {
    private Integer K_ACTIVE_TABLE_AREA_X = null;
    private Integer K_ACTIVE_TABLE_AREA_Y = null;
    private Integer K_ACTIVE_TABLE_AREA_HEIGHT = null;
    private Integer K_ACTIVE_TABLE_AREA_WIDTH = null;
    private Integer K_SPITFIRE_X = null;
    private Integer K_SPITFIRE_Y = null;

    public static final String MY_TAG = "Physics";


    public void SET_SCREEN_CONSTS(Integer k_ACTIVE_TABLE_AREA_HEIGHT, Integer k_ACTIVE_TABLE_AREA_WIDTH, Integer k_ACTIVE_TABLE_AREA_X, Integer k_ACTIVE_TABLE_AREA_Y, Integer k_SPITFIRE_X, Integer k_SPITFIRE_Y) {
        Log.i(MY_TAG, k_ACTIVE_TABLE_AREA_WIDTH + "  SET_SCREEN_CONSTS SET_SCREEN_CONSTS SET_SCREEN_CONSTS SET_SCREEN_CONSTS SET_SCREEN_CONSTS SET_SCREEN_CONSTS SET_SCREEN_CONSTS SET_SCREEN_CONSTS SET_SCREEN_CONSTS");

        K_ACTIVE_TABLE_AREA_WIDTH = k_ACTIVE_TABLE_AREA_WIDTH;

        K_ACTIVE_TABLE_AREA_HEIGHT = k_ACTIVE_TABLE_AREA_HEIGHT;



        K_ACTIVE_TABLE_AREA_X = k_ACTIVE_TABLE_AREA_X;
        K_ACTIVE_TABLE_AREA_Y = k_ACTIVE_TABLE_AREA_Y;

        K_SPITFIRE_X = k_SPITFIRE_X;
        K_SPITFIRE_Y = k_SPITFIRE_Y;
    }

    public void SET_CALIBRATION(List<Integer> calibration) {
        this.calibration = calibration;
    }

    public boolean HAS_CALIBRATION_VALUES() {
        return calibration.get(0) != 0 || calibration.get(1) != 0;
    }

        private List<Integer> calibration = new ArrayList<>(Arrays.asList(0,0));

    //calibration = (direction,distance)

//    //result in form (top wheel,bottom wheel,direction,distance,randomizer,delay)
//    public ArrayList<ArrayList<Integer>> calculateArray(Routine routine){
//        //Casts doubles to Integers at end (loses precision)
//
//        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
//        for (int i = 0; i < routine.getLength();i++){
//            Ball ball = routine.getBalls().get(i);
//            ArrayList<Integer> x = new ArrayList<>(Arrays.asList(0,0,0,0,0,0));
//            List<Integer> wheelSpeeds = limitWheelSpeeds(ball.getTopWheel(),ball.getBottomWheel());
//            x.set(0,wheelSpeeds.get(0));
//            x.set(1,wheelSpeeds.get(1));
//            x.set(2,(int) calculateDirection(ball));
//            x.set(3,calculateDistance(ball));
//            x.set(4, 0);
//            x.set(5, ball.getTimeDelay());
//            result.set(i,x);
//        }
//
//        return result;
//    }

    public List<Integer> getCalibration() {
        return calibration;
    }


    ////SAFE RANGES FOR SPITFIRE ROBOT
    private static final int max_dist = 125;
    private static final int min_dist = 80;//75;
    private static final int min_direction = 80;
    private static final int max_direction = 120;
    ///Returns safe distance within range
    public double getSafeDistance(Ball ball) {
        double dist = calculateDistance(ball);

        if (dist > max_dist) {
            return max_dist;
        }
        else if (dist < min_dist) {
            return min_dist;
        }
        return dist + calibration.get(0);
        ///UP/DOWN is first value (Dist)   LEFT/RIGHT is second (DIR)
    }


    ///Returns safe distance within range
    public double getSafeDirection(Ball ball) {
        ///UP/DOWN is first value (Dist)   LEFT/RIGHT is second (DIR)
        double direction = calculateDirection(ball) ;

        if (direction > max_direction) {
            return max_direction;
        }
        else if (direction < min_direction) {
            return min_direction;
        }
        return direction + calibration.get(1);
    }

//    private static final int middleTable = 520;
//    private double calcDirection_custom() {
//        double tanangle = java.lang.Math.atan( (ball.getX()-225) / (ball.getY()+170) );
//        double angle = (double) Math.round( tanangle * (180/Math.PI));
//
//        //-12,12 is range of app angles; 75,130 is robot range.
//        double result = map(angle,-12,12,70,130) + calibration.get(0);
//        return result;
//    }


    private double getAngleDegreesOA(double opp,double adj) {
        double tanTheta = opp/adj;
        double angleR = Math.atan(tanTheta);
        double angleDeg = angleR * (180/Math.PI);
        return angleDeg;
    }


    private Integer minPossAngleDeg = null, maxPossAngleDeg = null;

    private void calculateMinMaxAngles() {
        if (K_ACTIVE_TABLE_AREA_WIDTH == 0) { return; }
        double opp_min = (K_ACTIVE_TABLE_AREA_X - K_SPITFIRE_X);
        double adj_min = (K_ACTIVE_TABLE_AREA_Y - K_SPITFIRE_Y);

        double opp_max = (K_ACTIVE_TABLE_AREA_X + K_ACTIVE_TABLE_AREA_WIDTH - K_SPITFIRE_X);
        double adj_max = (K_ACTIVE_TABLE_AREA_Y - K_SPITFIRE_Y);
        minPossAngleDeg = (int) getAngleDegreesOA(opp_min,adj_min) + 9;
        maxPossAngleDeg = (int) getAngleDegreesOA(opp_max,adj_max) - 9;
        Log.i(MY_TAG,"K_ACTIVE_TABLE_AREA_X: " + K_ACTIVE_TABLE_AREA_X + "- K_SPITFIRE_X: " + K_SPITFIRE_X);
        Log.i(MY_TAG,"K_ACTIVE_TABLE_AREA_WIDTH: " + K_ACTIVE_TABLE_AREA_WIDTH + "& K_ACTIVE_TABLE_AREA_Y: " + K_ACTIVE_TABLE_AREA_Y);
        Log.i(MY_TAG,"opp_min: " + opp_min + " adj_min: " + adj_min);
        Log.i(MY_TAG,"opp_max: " + opp_max + " adj_max: " + adj_max);

        Log.i(MY_TAG,"minPossAngleDeg: " + minPossAngleDeg + " maxPossAngleDeg: " + maxPossAngleDeg);
        //HelperFunctions.assertion(maxPossAngleDeg != 0 && maxPossAngleDeg != minPossAngleDeg, "Error calculating min max");
    }

    private double calculateDirection(Ball ball) {
        HelperFunctions.assertion(K_SPITFIRE_X != null && K_SPITFIRE_Y != null,"Need to call .SET_SCREEN_CONSTS() before using PhysicsCalculator");
        if(minPossAngleDeg == null && maxPossAngleDeg == null) { calculateMinMaxAngles(); }

        Log.i(MY_TAG,"Spitfire X: " + K_SPITFIRE_X + " Y: " + K_SPITFIRE_Y);
        Log.i(MY_TAG,"Ball X: " + ball.getX() + " Y: " + ball.getY());

        double opp = (ball.getX() - K_SPITFIRE_X);
        double adj = (ball.getY() - K_SPITFIRE_Y);

        double angleDeg = getAngleDegreesOA(opp,adj);
        Log.i(MY_TAG,"angleDeg  "  + angleDeg);
        Log.i(MY_TAG,"minMax Angle  "  + minPossAngleDeg + " to " + maxPossAngleDeg);

        double result = map(angleDeg,minPossAngleDeg,maxPossAngleDeg,60,140); //-12,12 is range of AS3 app angles; 75,130 is robot range.
        Log.i(MY_TAG,"ROBOT angleVal  "  + result + "\n");

        return result;
    }


    ///Mapping the range of (lower1 to upper1) to the range of (lower2 to upper2)
    //Returns value of inputNum mapped to second range
    private double map(double inputNum, double lower1, double upper1, double lower2, double upper2){
        return (inputNum == lower1) ? lower2 : (inputNum - lower1) * (upper2 - lower2) / (upper1 - lower1) + lower2;
    }


    private int capValue(int val, int min, int max) {
        if(val < min) {
            return min;
        }
        else if (val > max) {
            return max;
        }
        return val;
    }

    //for topspin/no-spin: wheels spin forwards and top wheel speed > bottom wheel speed
    private final int topSpinNet = 85;
    private final int topSpinLine = 81;

    //for chop: top wheel speed is zero
    private final int chopNet = 85;
    private final int chopLine = 79;

    //for loop: bottom wheel spins backwards*
    private final int loopNet = 70;
    private final int loopLine = 66;

    //for heavy chop: top wheel spins backwards*
    private final int heavyChopNet = 19;
    private final int heavyChopLine = 14;

    public boolean isLockedValues = false;

    private Integer calculateDistance(Ball ball){
        HelperFunctions.assertion(K_ACTIVE_TABLE_AREA_HEIGHT != null && K_ACTIVE_TABLE_AREA_HEIGHT != null,"Need to call .SET_SCREEN_CONSTS() before using PhysicsCalculator");
        HelperFunctions.assertion(K_ACTIVE_TABLE_AREA_X != null && K_ACTIVE_TABLE_AREA_Y != null,"Need to call .SET_SCREEN_CONSTS() before using PhysicsCalculator");
        //Load correct pixel range to be mapped
        final int lowerPixelRange = K_ACTIVE_TABLE_AREA_Y; //550;///This is the pixel amounts that worked on AS3 legacy code
        final int upperPixelRange = K_ACTIVE_TABLE_AREA_Y + K_ACTIVE_TABLE_AREA_HEIGHT; //700;

        double result;
        double distance;
        int bw = ball.getBottomWheel();
        int tw = ball.getTopWheel();

        int minValue = 40;//148;
        int zeroValue = 90;//148;
        int maxValue = 160;//400;

        boolean isTopSpin = (bw >= zeroValue) && (tw >= bw);
        boolean isChop = (tw > zeroValue) && (tw < bw);
        boolean isHeavyChop = tw < zeroValue;


        double y = (double) ball.getY();
        if (isTopSpin) {
            Log.i(MY_TAG,"isTopSpin");

            if(isLockedValues == false) {
                tw = capValue(tw, 116,160);
                bw = capValue(bw, 90,107);
            }

            distance = map(tw,zeroValue,maxValue,0,70) / map(bw,zeroValue,maxValue,8,78);
            result = map(y, lowerPixelRange,upperPixelRange,topSpinNet,topSpinLine) + distance; //+ calibration.get(1);
        }
        else if (isChop) {
            Log.i(MY_TAG,"isChop");
            float lowerRange = zeroValue + 0.44f *(maxValue-zeroValue);

            if(isLockedValues == false) {
                tw = capValue(tw, 90, 90);
                bw = capValue(bw, 118, 139);
            }

            distance = (0.5f) * Math.pow( map(bw,lowerRange,maxValue,95,1000), 0.5f) ;
            result = map(y, lowerPixelRange,upperPixelRange,chopNet,chopLine) + distance; // calibration.get(1);
        }
        else if (isHeavyChop) {
            Log.i(MY_TAG,"isHeavyChop");

            if(isLockedValues == false) {
                tw = capValue(tw, 80, 84);
                bw = capValue(bw, 138, 160);
            }

            float upperTWChopRange = minValue + 0.32f *(maxValue-minValue);
            float lowerBWChopRange = minValue + (186f/350f) *(maxValue-minValue);
            //50 min, 112 max
            //            distance = map(tw,50,112,38,44) + map(bw,186, maxValue,19,32);
            distance = map(tw,minValue,upperTWChopRange,38,44) + map(bw,lowerBWChopRange, maxValue,19,32);
            result = map(y,lowerPixelRange,upperPixelRange,heavyChopNet,heavyChopLine) + distance ; //calibration.get(1);
        }
        else { //Must be loop shot
            Log.i(MY_TAG,"isLoop");
            float upperBWLoopRange = minValue + 0.32f *(maxValue-minValue);

            if(isLockedValues == false) {
                // tw = capValue(tw, 140,160);
                bw = capValue(bw, 70, 84);
            }

            //top wheel speed has to stay high enough to overcome the bottom wheel reverse speed and
            //the ball still travel far enough to get over the net. My lines 359-361 kind of worked in my app.
            // But try, instead, mapping 140to160 to 84to70, so as you drag the bottom reverse wheelspeed up, the
            // app 'snaps' the top wheelspeed up with it.
            tw = (int) map(bw, 70, 84, 140,160);

            distance = map(tw,zeroValue,maxValue,0,14) + map(bw,minValue,upperBWLoopRange,0,7);
            result = map(y,lowerPixelRange,upperPixelRange,loopNet,loopLine) + distance; //calibration.get(1);
        }
        return (int) Math.round(result);
    }

}
