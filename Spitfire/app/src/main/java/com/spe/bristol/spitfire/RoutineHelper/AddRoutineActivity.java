package com.spe.bristol.spitfire.RoutineHelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.spe.bristol.spitfire.HelperFunctions;
import com.spe.bristol.spitfire.Pages.MyRoutineActivity;
import com.spe.bristol.spitfire.R;

public class AddRoutineActivity extends AppCompatActivity {

    private static final String TAG = "AddRoutineActivity";

    DatabaseHelper mDatabaseHelper;
    private Button btnSave, btnBlue, btnGreen,btnBrown, btnOrange;
    private EditText ETID, ETName;
    private String selectedColour = "blue";
    private Routine currentRoutine;
    private LinearLayout routineNameLinLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_routine);

        //finding view on activity
        ETName = (EditText) findViewById(R.id.editText_Name);
        routineNameLinLayout = (LinearLayout) findViewById(R.id.linearLayout2);

        //recieve the routine that is being saved
        Intent receivedIntent = getIntent();
        currentRoutine = (Routine) receivedIntent.getSerializableExtra("routine");

        //Instantiate a database helper
        mDatabaseHelper = new DatabaseHelper(this);

        //Setting up the buttons on for the activity
        makeSaveButton();
        makeBlueButton();
        makeGreenButton();
        makeBrownButton();
        makeOrangeButton();
    }

    public void unselectColour(){
        //function to make colour button appear unselected
        switch (selectedColour) {
            case "blue":
                btnBlue.setBackground(getDrawable(R.drawable.bluecircle));
                break;
            case "green":
                btnGreen.setBackground(getDrawable(R.drawable.greencircle));
                break;
            case "brown":
                btnBrown.setBackground(getDrawable(R.drawable.browncircle));
                break;
            case "orange":
                btnOrange.setBackground(getDrawable(R.drawable.orangecircle));
                break;
        }
    }

    public void AddData(String newEntry) {
        //function to save the to the database and display if it successful to user
        boolean insertData = mDatabaseHelper.addData(newEntry,selectedColour,currentRoutine);

        if (insertData) {
            toastMessage("Routine Saved");
        } else {
            toastMessage("Something went wrong");
        }
    }

    private void toastMessage(String message){
        //displays a toast message
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    private  void makeSaveButton(){
        //finds the save button view and sets on click listener
        btnSave = (Button) findViewById(R.id.button_Save);
        HelperFunctions.assertion(btnSave != null,"button is null");
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newEntry = ETName.getText().toString();
                if (ETName.length() != 0) {
                    AddData(newEntry);
                    ETName.setText("");
                    Intent intent = new Intent(AddRoutineActivity.this, MyRoutineActivity.class);
                    startActivity(intent);
                } else {
                    toastMessage("Name of Routine cannot be blank");
                }
            }
        });
    }

    private void makeBlueButton(){
        //setup blue colour button
        btnBlue = (Button) findViewById(R.id.bluebutton);
        btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"blue button pressed");
                unselectColour();
                selectedColour = "blue";
                btnBlue.setBackground(getDrawable(R.drawable.bluecircleselected));
                routineNameLinLayout.setBackground(getDrawable(R.drawable.routine_box));
            }
        });
    }

    private  void makeGreenButton(){
        //setup green colour button
        btnGreen = (Button) findViewById(R.id.greenbutton);
        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"green button pressed");
                unselectColour();
                selectedColour = "green";
                btnGreen.setBackground(getDrawable(R.drawable.greencircleselected));
                routineNameLinLayout.setBackground(getDrawable(R.drawable.routineboxgreen));
            }
        });
    }

    private void makeBrownButton(){
        //setup brown colour button
        btnBrown = (Button) findViewById(R.id.brownbutton);
        btnBrown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"brown button pressed");
                unselectColour();
                selectedColour = "brown";
                btnBrown.setBackground(getDrawable(R.drawable.browncircleselected));
                routineNameLinLayout.setBackground(getDrawable(R.drawable.routineboxbrown));
            }
        });
    }

    private void makeOrangeButton(){
        //setup orange colour button
        btnOrange = (Button) findViewById(R.id.orangebutton);
        btnOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"Orange button pressed");
                unselectColour();
                selectedColour = "orange";
                btnOrange.setBackground(getDrawable(R.drawable.orangecircleselected));
                routineNameLinLayout.setBackground(getDrawable(R.drawable.routineboxorange));
            }
        });
    }
}
