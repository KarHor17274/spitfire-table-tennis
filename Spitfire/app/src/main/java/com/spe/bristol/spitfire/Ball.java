package com.spe.bristol.spitfire;

import android.widget.ImageView;

import java.io.Serializable;

public class Ball implements Serializable {

    private ImageView ballImageView;
    private ImageView ballHitBoxImageView;
    private int topWheel = 120;
    private int bottomWheel = 90; //DEFAULT VALS
    private int timeDelay = 600;
    private float x;
    private float y;
    private int id;
    private int dupID = 0;
    private boolean isActive; //active balls will be fired by machine
    private PointXY spinHandlePosTopWheel, spinHandlePosBottomWheel;
    private String topTextVal, btmTextVal;

    public Ball(ImageView ballView,ImageView ballHitBoxImageView, int ID) {
        this.ballImageView = ballView;
        this.ballHitBoxImageView = ballHitBoxImageView;
        setId(ID);
    }

    public Ball(int topWheel, int bottomWheel, int timeDelay, float x, float y, int id, boolean isActive) {
        this.topWheel = topWheel;
        this.bottomWheel = bottomWheel;
        this.timeDelay = timeDelay;
        this.x = x;
        this.y = y;
        this.id = id;
        this.isActive = isActive;
    }

    public Ball(Ball ball) {

        ///COPY ALL ATTRIBUTES
        this.x = ball.getX();
        this.y = ball.getY();
        this.topWheel = ball.getTopWheel();
        this.bottomWheel = ball.getBottomWheel();
        this.timeDelay = ball.getTimeDelay();
        this.id = ball.getId();
        this.ballImageView = ball.getBallImageView();


        ///COPY ALL ATTRIBUTES EXCEPT dupID

        //dupID being 0 means the ball is the first, original ball
        if (ball.dupID == 0) {
            this.dupID = 1;
        }
        else {
            this.dupID = ball.dupID + 1;
        }
    }

    public void setDupID(int dupID) {
        this.dupID = dupID;
    }

    public String getTopTextVal() {
        return topTextVal;
    }

    public void setTopTextVal(String topTextVal) {
        this.topTextVal = topTextVal;
    }

    public String getBtmTextVal() {
        return btmTextVal;
    }

    public void setBtmTextVal(String btmTextVal) {
        this.btmTextVal = btmTextVal;
    }

    public PointXY getSpinHandlePosTopWheel() {
        return spinHandlePosTopWheel;
    }

    public PointXY getSpinHandlePosBottomWheel() {
        return spinHandlePosBottomWheel;
    }

    public void setSpinHandlePosTopWheel(PointXY spinHandlePosTopWheel) {
        this.spinHandlePosTopWheel = spinHandlePosTopWheel;
    }

    public void setSpinHandlePosBottomWheel(PointXY spinHandlePosBottomWheel) {
        this.spinHandlePosBottomWheel = spinHandlePosBottomWheel;
    }

    public Object getBallImageTag() {
        return ballHitBoxImageView.getTag();
    }

    public int getTopWheel() {
        return topWheel;
    }

    public void setTopWheel(int topWheel) {
        this.topWheel = topWheel;
    }

    public int getBottomWheel() {
        return bottomWheel;
    }

    public void setBottomWheel(int bottomWheel) {
        this.bottomWheel = bottomWheel;
    }

    public int getTimeDelay() {
        return Math.max(timeDelay * 600, 600);
    }

    public void setTimeDelay(double timeDelay) {
        this.timeDelay = (int) timeDelay;
    }

    public void setBallImageView(ImageView imageView){
        this.ballImageView = imageView;
    }

    public void setBallHitBoxImageView(ImageView imageView){
        this.ballHitBoxImageView = imageView;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
        ballImageView.setX(x);
        ballHitBoxImageView.setX(x);
    }

    public float getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        ballImageView.setY(y);
        ballHitBoxImageView.setY(y);

    }

    public int getId() {
        return id;
    }

    public boolean isDuplicate() {
        return (this.dupID != 0);
    }

    public int getDupID() {
        return dupID;
    }

    private void setId(int id) {
        this.id = id;
    }

    public void setBallView(ImageView imageView) {
        this.ballImageView = imageView;
    }

    public ImageView getBallImageView() {
        return ballImageView;
    }

    public ImageView getBallHitBoxImageView() {
        return ballHitBoxImageView;
    }

    /* Returns whether ball is active (e.g. on table) or inactive (still in ball box) */
    public boolean isActive() {
        return isActive;
    }

    public void setBallActive() {
        isActive = true;
    }

    public void setBallInactive() {
        isActive = false;
    }

    public void setPosition(float x, float y){
        this.x = x;
        this.y = y;
        ballImageView.setX(x);
        ballImageView.setY(y);
        ballHitBoxImageView.setX(x + ballImageView.getMeasuredWidth()/2f - ballHitBoxImageView.getMeasuredWidth()/2f );
        ballHitBoxImageView.setY(y + ballImageView.getMeasuredHeight()/2f - ballHitBoxImageView.getMeasuredHeight()/2f );
    }

}

