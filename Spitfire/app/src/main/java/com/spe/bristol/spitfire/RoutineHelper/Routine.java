package com.spe.bristol.spitfire.RoutineHelper;

import android.util.Log;

import com.spe.bristol.spitfire.Ball;
import com.spe.bristol.spitfire.HelperFunctions;
import com.spe.bristol.spitfire.SpitfireCommunication.PhysicsCalculator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Routine implements Serializable {
    private String name;
    private Date date;
    private String creator;
    private ArrayList<Ball> balls = new ArrayList<>();
    private int id;
    private int length;

    private PhysicsCalculator physicsCalculator;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }


    public Routine(int id) {
        this.id = id;

    }

    public Routine(String name,Date date,String creator,ArrayList<Ball> balls, int id){
        this.name = name;
        this.date = date;
        this.creator = creator;
        this.balls = balls;
        this.id = id;
    }

    public void setPhysicsCalculator(PhysicsCalculator physicsCalculator) {
        this.physicsCalculator = physicsCalculator;
    }

    public void printBalls(String TAG1) {
        for(Ball ball : balls) {
            Log.i(TAG1, Routine.getPrintBallString(ball, physicsCalculator));
        }
    }

    public String getStringFromBall(Ball ball) {
        int distance = (int) physicsCalculator.getSafeDistance(ball);
        int direction = (int)  physicsCalculator.getSafeDirection(ball);
        //TODO Ensure distance is correct
        String ballAsStr = "" + ball.getTopWheel() + "," + ball.getBottomWheel() + "," + direction + "," + distance  + "," +  "0" + "," + ball.getTimeDelay();
        return ballAsStr;
    }

    public String getPrintBallString(Ball ball) {
        return getPaddedStringFromBall(ball,true,true);
    }



    public static String getPrintBallString(Ball ball, PhysicsCalculator physicsCalculator) {
        return getPaddedStringFromBall(ball,true,true, physicsCalculator);
    }

    public String getPaddedStringFromBall(Ball ball, boolean withVarNames, boolean withXY) {
        HelperFunctions.assertion(physicsCalculator != null,"Need to call setPhysicsCalculator() before printing strings");
        int distance = (int) physicsCalculator.getSafeDistance(ball);
        int direction = (int)  physicsCalculator.getSafeDirection(ball);

        int padLength = 6;
        char padChar = ' ';
        String distStr = HelperFunctions.padLeft("" + distance,padLength,padChar);
        String dirStr = HelperFunctions.padLeft("" + direction,padLength,padChar);
        String topStr = HelperFunctions.padLeft("" + ball.getTopWheel(),padLength,padChar);
        String btmStr = HelperFunctions.padLeft("" + ball.getBottomWheel(),padLength,padChar);
        String timStr = HelperFunctions.padLeft("" + ball.getTimeDelay(),padLength,padChar);
        String ranStr = HelperFunctions.padLeft("" + "0",padLength,padChar);

        String idStr = HelperFunctions.padLeft("" + ball.getId(),padLength,padChar);
        String dupIDStr = HelperFunctions.padLeft("" + ball.getDupID(),padLength,padChar);


        String ballAsStr = idStr + "." + dupIDStr + " -> " + topStr + "," + btmStr + "," + dirStr + "," + distStr + "," +  ranStr + "," + timStr;

        if (withVarNames) {
            ballAsStr = " ID:" + idStr + "." + dupIDStr + " ->" + "TOP:" + topStr + " BTM:" + btmStr + " DIR:" + dirStr + " DIS:" + distStr + " RAN:" +  ranStr + " TIM:" + timStr;

            if (withXY) {
                String xStr = HelperFunctions.padLeft("" + ball.getX(),padLength,padChar);
                String yStr = HelperFunctions.padLeft("" + ball.getY(),padLength,padChar);
                ballAsStr += " Xpos:" +  xStr + " Ypos:" + yStr;
            }
        }
        else {
            if (withXY) {
                String xStr = HelperFunctions.padLeft("" + ball.getX(),padLength,padChar);
                String yStr = HelperFunctions.padLeft("" + ball.getY(),padLength,padChar);
                ballAsStr += "," +  xStr + "," + yStr;
            }
        }


        return ballAsStr;
    }

    public static String getPaddedStringFromBall(Ball ball, boolean withVarNames, boolean withXY, PhysicsCalculator physicsCalculator) {
        int distance = (int) physicsCalculator.getSafeDistance(ball);
        int direction = (int)  physicsCalculator.getSafeDirection(ball);

        int padLength = 6;
        char padChar = ' ';
        String distStr = HelperFunctions.padLeft("" + distance,padLength,padChar);
        String dirStr = HelperFunctions.padLeft("" + direction,padLength,padChar);
        String topStr = HelperFunctions.padLeft("" + ball.getTopWheel(),padLength,padChar);
        String btmStr = HelperFunctions.padLeft("" + ball.getBottomWheel(),padLength,padChar);
        String timStr = HelperFunctions.padLeft("" + ball.getTimeDelay(),padLength,padChar);
        String ranStr = HelperFunctions.padLeft("" + "0",padLength,padChar);

        String idStr = HelperFunctions.padLeft("" + ball.getId(),padLength,padChar);
        String dupIDStr = HelperFunctions.padLeft("" + ball.getDupID(),padLength,padChar);


        String ballAsStr = idStr + "." + dupIDStr + " -> " + topStr + "," + btmStr + "," + dirStr + ","+  distStr + "," +  ranStr + "," + timStr;

        if (withVarNames) {
            ballAsStr = " ID:" + idStr + "." + dupIDStr + " ->" + "TOP:" + topStr + " BTM:" + btmStr + " DIR:" + dirStr + " DIS:" + distStr + " RAN:" +  ranStr + " TIM:" + timStr;

            if (withXY) {
                String xStr = HelperFunctions.padLeft("" + ball.getX(),padLength,padChar);
                String yStr = HelperFunctions.padLeft("" + ball.getY(),padLength,padChar);
                ballAsStr += " Xpos:" +  xStr + " Ypos:" + yStr;
            }
        }
        else {
            if (withXY) {
                String xStr = HelperFunctions.padLeft("" + ball.getX(),padLength,padChar);
                String yStr = HelperFunctions.padLeft("" + ball.getY(),padLength,padChar);
                ballAsStr += "," +  xStr + "," + yStr;
            }
        }


        return ballAsStr;
    }


    //Send this string to robot - concats all balls in routine
    public String getStringFromBalls() {
        String ballString = "";

        for(Ball ball : balls) {
            ballString += getStringFromBall(ball); //TODO look at commas see if format correct
        }
        return ballString;
    }

    public ArrayList<String> getStringsFromBalls() {
        ArrayList<String> ballStrings = new ArrayList<>();

        for (int i = 0; i < balls.size(); i++) {
            Ball ball = balls.get(i);

            //Set last ball to min time delay of 1000 = 1s
            if (i == balls.size()-1) {
                if (ball.getTimeDelay() < 1000) {
                    ball.setTimeDelay(2);
                }
            }
            ballStrings.add(getStringFromBall(ball));
        }
        return ballStrings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public ArrayList<Ball> getBalls() {
        return balls;
    }

    public void setBalls(ArrayList<Ball> balls) {
        this.balls = balls;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addBall(Ball ball) {
        balls.add(ball);
    }

    public void removeBall(int ballID) throws NullPointerException {
        try {
            for(Ball b : balls) {
                if(b.getId() == ballID) {
                    balls.remove(b);
                }
            }
        }
        catch (NullPointerException err) {

        }
    }


    public void removeBall(Ball ball) {
        balls.remove(ball);
    }

    public boolean containsBall(Ball ball) {
        for (Ball b : getBalls()) {
            if (b == ball) {
                return true;
            }
        }

        return false;
    }

    public Ball getOriginalBallOfID(int id) throws NullPointerException {
        for (Ball b : getBalls()) {
            if (b.getId() == id) {
                if (b.isDuplicate() == false) {
                    return b;
                }
            }
        }

        throw new NullPointerException();
    }

    public Ball getBallWithMaxDupIdFromID(int id) throws NullPointerException {
        Ball maxDupBall = null;

        for (Ball b : getBalls()) {
            if (b.getId() == id) {
                //Set it if haven't yet
                if (maxDupBall == null) { maxDupBall = b; }

                //Looking for highest dupID
                if (maxDupBall.getDupID() < b.getDupID()) {
                    maxDupBall = b;
                }
            }
        }

        if (maxDupBall == null) { throw new NullPointerException(); }

        return maxDupBall;
    }


    public String toString(boolean withVarNames, boolean withXY) {
        String routineString = "\n";

        for(Ball ball : balls) {
            routineString += getPaddedStringFromBall(ball,withVarNames,withXY);
            routineString += " \n";
        }
        routineString += " \n";

        return routineString;
    }
}
