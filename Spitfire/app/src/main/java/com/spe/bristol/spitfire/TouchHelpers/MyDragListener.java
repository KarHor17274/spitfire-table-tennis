package com.spe.bristol.spitfire.TouchHelpers;

import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.widget.Toast;

import com.spe.bristol.spitfire.HelperFunctions;
import com.spe.bristol.spitfire.Pages.MainActivity;

public class MyDragListener implements View.OnDragListener {
    private Context context = null;
    private TouchInterface dragInterface = null;
    private static final String TAG = "drag:";

    public MyDragListener(Context context, TouchInterface touchInterface) {
        this.context = context;
        this.dragInterface = touchInterface;
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        //Log.i(TAG, "onDrag");

        // Get the drag drop action.
        int dragAction = dragEvent.getAction();

        if (dragAction == dragEvent.ACTION_DRAG_STARTED) {
            Log.i(TAG, "ACTION_DRAG_STARTED");
            // Check whether the dragged view can be placed in this target view or not.
            ClipDescription clipDescription = dragEvent.getClipDescription();

            if (clipDescription.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                // Return true because the target view can accept the dragged object.
                Log.i(TAG, "target view can accept the dragged object");

                return true;
            }
        }
        else if (dragAction == dragEvent.ACTION_DRAG_ENTERED) {
            Log.i(TAG, "ACTION_DRAG_ENTERED");

            // When the being dragged view enter the target view, change the target view background color.
            changeTargetViewBackground(view, Color.GREEN);
            return true;

        }
        else if (dragAction == dragEvent.ACTION_DRAG_EXITED) {
            Log.i(TAG, "ACTION_DRAG_EXITED");
            // When the being dragged view exit target view area, clear the background color.
            resetTargetViewBackground(view);
            return true;
        }
        else if (dragAction == dragEvent.ACTION_DRAG_ENDED) {
            Log.i(TAG, "ACTION_DRAG_ENDED");
            // When the drop ended reset target view background color.
            resetTargetViewBackground(view);

            // Get drag and drop action result.
            boolean result = dragEvent.getResult();

            // result is true means drag and drop action success.
            if (result) {
                Log.i(TAG, "Drag and drop action complete successfully.");
//                Toast.makeText(context, "Drag and drop action complete successfully.", Toast.LENGTH_LONG).show();
            }
            else {
                Log.i(TAG, "Drag and drop action failed");
                //Failed so need to make visible again
                View srcView = (View) dragEvent.getLocalState();
                srcView.setVisibility(View.VISIBLE);
                //Toast.makeText(context, "Drag and drop action failed.", Toast.LENGTH_LONG).show();
            }

            return true;
        }
        else if (dragAction == dragEvent.ACTION_DROP) {
            Log.i(TAG, "ACTION_DROP");
            // When drop action happened.
            // Get clip data in the drag event first.
            ClipData clipData = dragEvent.getClipData();

            // Get drag and drop item count.
            int itemCount = clipData.getItemCount();

            // If item count bigger than 0.
            if (itemCount > 0) {
                // Get clipdata item.
                ClipData.Item item = clipData.getItemAt(0);
                // Get item text.
                String dragDropString = item.getText().toString();

                Log.i(TAG, "Dragged object is a " + dragDropString);

                // Reset target view background color.
                resetTargetViewBackground(view);

                View srcView = (View) dragEvent.getLocalState();
                    // Get dragged view object from drag event object.
                HelperFunctions.assertion(srcView != null, "srcView Cannot be null");
                HelperFunctions.assertion(srcView.getParent() != null, "srcView.parent Cannot be null");
//                Log.i(TAG, "srcView = (tag= " + srcView.getTag() + " ) " + srcView.toString());
                Log.i(TAG, "srcView = (parent= " + srcView.getParent());


                    // Get dragged view's parent view group.
                ViewGroup owner = (ViewGroup) srcView.getParent();

                // OLD --> The drop target view is a LinearLayout object so cast the view object to it's type.
                ConstraintLayout newOwner = (ConstraintLayout) view.getParent();

                boolean newOwnerIsOldOwner = owner == newOwner;

                //Remove & add if owner is not old owner
                if (newOwnerIsOldOwner == false) {
                    if (owner != null) {
                        // Remove source view from original parent view group.
                        owner.removeView(srcView);
                    }
                    // Add the dragged view in the new container.
                    newOwner.addView(srcView);
                }

                ///Get relative x,y
                float xPos = dragEvent.getX();
                float yPos = dragEvent.getY();
                dragInterface.hasDragged(srcView, xPos,yPos, newOwner);

//                Log.i(TAG, "Context: " + (context));
//                Log.i(TAG, "Checking context is MainActivity: " + (context instanceof MainActivity));
//                if(context instanceof MainActivity) {
//                    ((MainActivity) context).ballDropped();
//                    Log.i(TAG, "Context is MainActivity");
//                }
                Log.i(TAG, "Table (active area)= " + view.toString());
                Log.i(TAG, "New Owner = " + newOwner.toString());

                // Make the dragged view object visible.
                srcView.setVisibility(View.VISIBLE);
                // Returns true to make DragEvent.getResult() value to true.
                return true;
            }
        }
        else if (dragAction == dragEvent.ACTION_DRAG_LOCATION) {
            Log.i(TAG, "ACTION_DRAG_LOCATION");
            return true;
        }
        else {
            Log.i(TAG, "Drag and drop unknown action type.");
            //Toast.makeText(context, "Drag and drop unknown action type.", Toast.LENGTH_LONG).show();
        }

        return false;
    }


    /* Reset drag and drop target view's background color. */
    private void resetTargetViewBackground(View view) {
        // Clear color filter.
        view.getBackground().clearColorFilter();

        // Redraw the target view use original color.
        view.invalidate();
    }


    /* Change drag and drop target view's background color. */
    private void changeTargetViewBackground(View view, int color) {
        /* When the being dragged view enter the target view, change the target view background color.
         *
         *  color : The changed to color value.
         *
         *  mode : When to change the color, SRC_IN means source view ( dragged view ) enter target view.
         * */
        view.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);

        // Redraw the target view use new color.
        view.invalidate();
    }
}