package com.spe.bristol.spitfire;

public enum ZPositions {
  Table, TableActiveArea, TableLine, Toolbar, BehindBall, Ball, AboveBall, BehindSpinner, Spinner, AboveSpinner, Button, ButtonRing;
}

