package com.spe.bristol.spitfire.SpitfireCommunication;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import com.spe.bristol.spitfire.Pages.ConnectActivity;
import com.spe.bristol.spitfire.RoutineHelper.Routine;
import com.spe.bristol.spitfire.SpitfireCommunication.Bluetooth.BluetoothService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class SpitfireController extends Application implements SpitfireInterface {

    public SpitfireController() {
    }

    private static final String TAG = "SpitfireController";

    private static BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothDevice btDevice;
    private final BluetoothService bluetoothService = new BluetoothService();

    // variable to hold context,connectActivity of ConnectActivity.java
    private Context context;
    private ConnectActivity connectActivity;

    ///GLOBAL ROUTINE HANDLER METHODS
    private Routine routine_internal;
    public Routine getRoutine() {
        return routine_internal;
    }
    public void setRoutine(Routine routine) {
        this.routine_internal = routine;
    }
    public Boolean isRoutineToSave() {
        return routine_internal != null;
    }


    private PhysicsCalculator physicsCalculator = new PhysicsCalculator();


    //this constructor obtains context,connectActivity of ConnectActivity.java
    public SpitfireController(Context connectActivity_context, ConnectActivity connectActivity_activity) {
        this.connectActivity = connectActivity_activity;
        this.context = connectActivity_context;
    }


    public boolean hasSetScreenConsts = false;
    public void SET_SCREEN_CONSTS(Integer k_ACTIVE_TABLE_AREA_HEIGHT, Integer k_ACTIVE_TABLE_AREA_WIDTH, Integer k_ACTIVE_TABLE_AREA_X, Integer k_ACTIVE_TABLE_AREA_Y, Integer k_SPITFIRE_X, Integer k_SPITFIRE_Y) {
        physicsCalculator.SET_SCREEN_CONSTS(k_ACTIVE_TABLE_AREA_HEIGHT,k_ACTIVE_TABLE_AREA_WIDTH,k_ACTIVE_TABLE_AREA_X,k_ACTIVE_TABLE_AREA_Y, k_SPITFIRE_X, k_SPITFIRE_Y);
        hasSetScreenConsts = true;
    }


    public void SET_CALIBRATION(List<Integer> calibration) {
        physicsCalculator.SET_CALIBRATION(calibration);
    }

    public boolean HAS_CALIBRATION_VALUES() {
        return physicsCalculator.HAS_CALIBRATION_VALUES();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //
    ///////////////////  ROUTINE CODE  ///////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void playRoutine(Routine routine) {
        //Set physics calculator for routine (ensures correct screen widths)
        routine.setPhysicsCalculator(physicsCalculator);

        //send the list of balls' array to the robot through bluetooth
        routine.printBalls(TAG);

        ArrayList<String> ballStrings = routine.getStringsFromBalls();

        String strToSend = "";
        for (String ballString : ballStrings) {
            Log.i(TAG, "Bluetooth write.." + ballString);
            strToSend += ballString + ",";
        }
//        strToSend = HelperFunctions.removeLastChar(strToSend);
        Log.i(TAG, "SENDING ----> " + strToSend);

        bluetoothService.write(strToSend.getBytes());
    }

    public void stop() { //pauseSignal should be 30000 to stop
        String stopSignal = "90,90,100,100,0,30000,";
        String stopSignal2 = "90,90,100,100,0,30000";
        Log.i(TAG, "Bluetooth write..(STOP) = " + stopSignal2);
        if (bluetoothService == null) {
            return;
        }
        bluetoothService.write(stopSignal2.getBytes());
    }

    public PhysicsCalculator getPhysicsCalculator() {
        return physicsCalculator;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //
    ///////////////////  BLUETOOTH  ///////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // Create a BroadcastReceiver for ACTION_FOUND so that it broadcasts to the system when it detects action changes on bluetooth
    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (action.equals(mBluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, mBluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "mBroadcastReceiver1: STATE OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE ON");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING ON");
                        break;
                }
            }
        }
    };

    //Initialise connection to the robot
    public void connect() {
        // Device doesn't support Bluetooth
        if (mBluetoothAdapter == null) {
            Log.i(TAG, "Bluetooth not supported");
            Toast.makeText(context, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
            //closing the application if the bt function is not supported at all
            connectActivity.finish();
        }
        //if bluetooth is not enabled. request user permission
        if (!mBluetoothAdapter.isEnabled()) {
            Log.i(TAG, "Bluetooth is not enabled & request permission");

            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            connectActivity.startActivity(enableBtIntent);

            //intentFilter to detect and broadcast the action_state_change
            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            context.registerReceiver(mBroadcastReceiver1, BTIntent);
        }
        //if the bluetooth is enabled, we proceed to next connectActivity
        if (mBluetoothAdapter.isEnabled()) {
            Log.i(TAG, "Bluetooth is enabled.");
            //intentFilter to detect and broadcast the action_state_change
            IntentFilter btState = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            context.registerReceiver(mBroadcastReceiver1, btState);
            if (ConnectActivity.connectionStatus == 2 || ConnectActivity.connectionStatus == 0) {
                Log.i(TAG, "initialising connecting");
                establishConnection();
            }
            }
        }

    //establishing connection through bluetooth by searching the paired list of devices and match with bluetooth name.
    private void establishConnection() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        Log.i(TAG, "Reading paired devices");
        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                Log.i(TAG, "onReceive: " + device.getName() + ": " + device.getAddress());
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                if (deviceName.equals("HC-06")) {
                    btDevice = device;
                    Log.i(TAG, "condition verified");
                }
            }
        }
        Log.i(TAG, "started bluetoothService");
        bluetoothService.initConnection(btDevice);
    }

}
