package com.spe.bristol.spitfire.TouchHelpers;

import android.view.View;
import android.view.ViewGroup;

public interface TouchInterface {
    void hasDragged(View srcView, float newX, float newY, ViewGroup newSrcOwner);

    void touchesBegan(View srcView);

    void touchesEnded(View srcView, float rawX, float rawY);

    void touchesMoved(View srcView, float rawX, float rawY);

    boolean allowDrag(View srcView);
}
