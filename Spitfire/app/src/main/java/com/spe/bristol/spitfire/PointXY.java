package com.spe.bristol.spitfire;

import java.io.Serializable;

public class PointXY  implements Serializable {
    private float x, y;

    public PointXY(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public PointXY() {

    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }
}
