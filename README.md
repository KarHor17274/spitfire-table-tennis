# README #

Spitfire table tennis robot

===============================

The Table tennis robots spit balls at various spins and speeds to various points on the table, and you try to hit them back. 

This project is to create an android app to control the robot by letting the user select the location, speed and spin of the balls being shot. The is potential to extend the functionally of the app. 